import xml.etree.ElementTree as ET
import os.path
import numpy as np
import math

# Disclaimer: All the following cases are not handled and for the moment need to be avoided or fixed manually:
# Mitsuba materials cannot be called 'Material' or 'None'. 
# In the Mitsuba XML there should only be either an emitter or a bsdf for an OBJ, not both
# (Falcor object-emitter and bsdf are both materials and I didnt implement the merging of the two into one Falcor material)

# TODO : maybe put specular component to (1-diffuse) to ensure energy conservation. Materials are overall too shiny.
# TODO : normal bump maps 
# TODO : handle opacity masks. 
# TODO : use Mitsuba's camera matrix.

# TODO: this script only handles OBJ objects and lights + envmaps. Does not handle Mitsuba preset shapes (sphere, plane etc.)
# Area lights only work if they are defined as an area-emitter ID and that is then associated to an OBJ like a material.


# Paths for where to find the Mitsuba XML and where to save the Falcor pyscene
mitsuba2xml = 'C:/Users/grainer/Desktop/inriaprojects/Falcor/scenes/living-room/scene.xml'
falcorscript = 'C:/Users/grainer/Desktop/inriaprojects/Falcor/scenes/living-room/falcorscene.pyscene'


def extractTransform(tmat):
    t = np.array([tmat[0, 3], tmat[1, 3], tmat[2, 3]]) # squeeze wasn't working somehow -.-
    s = np.array([np.sqrt(tmat[0,0]*tmat[0,0] + tmat[1,0]*tmat[1,0] + tmat[2,0]*tmat[2,0]),
                          np.sqrt(tmat[0,1]*tmat[0,1] + tmat[1,1]*tmat[1,1] + tmat[2,1]*tmat[2,1]),
                          np.sqrt(tmat[0,2]*tmat[0,2] + tmat[1,2]*tmat[1,2] + tmat[2,2]*tmat[2,2]) ])
    r = tmat[0:3, 0:3]
    r[:,0] = r[:,0] / s[0]
    r[:,1] = r[:,1] / s[1]
    r[:,2] = r[:,2] / s[2]

    return s,r,t

def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6

def rotationMatrixToEulerAngles(R):
    assert (isRotationMatrix(R))
    sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])
    singular = sy < 1e-6
    if not singular:
        x = math.atan2(R[2, 1], R[2, 2])
        y = math.atan2(-R[2, 0], sy)
        z = math.atan2(R[1, 0], R[0, 0])
    else:
        x = math.atan2(-R[1, 2], R[1, 1])
        y = math.atan2(-R[2, 0], sy)
        z = 0

    # Convert to degrees:
    x = x / np.pi * 180.0
    y = y / np.pi * 180.0
    z = z / np.pi * 180.0
    return np.array([x, y, z])

def writeBSDFtopyscene(f, elem, id = None):
    roughness = 1.0
    basecol = "1,1,1"
    basecol_texture = False
    specularcol = "0,0,0"

    if 'id' in elem.attrib.keys():
        bsdf_id = elem.attrib['id']
    else:
        if id is None:
            bsdf_id = 'mat' + str(np.random.randint(9999))
        else:
            bsdf_id = id

    f.write(bsdf_id + " = Material('" + bsdf_id + "')" + '\n')

    if elem.attrib['type'] == "twosided":
        elem = elem[0]
        f.write(bsdf_id + ".doubleSided = bool(" + str(True) + ")" + '\n')

    if elem.attrib['type'] == 'diffuse':
        for child_elem in elem:
            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "reflectance":
                basecol = child_elem.attrib['value']

            if child_elem.tag == "texture" and child_elem.attrib['name'] == "reflectance":
                basecol_texture = True
                for child2 in child_elem:
                    if child2.tag == "string" and child2.attrib['name'] == "filename":
                        f.write(
                            bsdf_id + ".loadTexture(MaterialTextureSlot.BaseColor, '" + scenepath + '/' + child2.attrib[
                                'value'] + "')" + '\n')

    if elem.attrib['type'] == "conductor":
        roughness = 0.0
        specularcol = "1,1,1"
        basecol = "0,0,0"  # TODO : is the basecol of a conductor 0 ??
        for child_elem in elem:
            if elem[0].tag == "rgb" and elem[0].attrib['name'] == "specular_reflectance":
                specularcol = elem[0].attrib['value']

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "eta":
                eta = np.matrix(child_elem.attrib['value'])
                eta = np.mean(eta)
                # TODO : handle Mitsuba's complex IORs
                f.write(bsdf_id + ".indexOfRefraction = float(" + str(eta) + ")" + '\n')

    if elem.attrib['type'] == "roughconductor":
        specularcol = "1,1,1"
        basecol = "0,0,0"  # TODO : is the basecol the specular coeff??
        for child_elem in elem:
            if child_elem.tag == "float" and child_elem.attrib['name'] == "alpha":
                roughness = child_elem.attrib['value']

            if elem[0].tag == "rgb" and elem[0].attrib['name'] == "specular_reflectance":
                specularcol = elem[0].attrib['value']

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "eta":
                eta = np.matrix(child_elem.attrib['value'])
                eta = np.mean(eta)
                # TODO : handle Mitsuba's complex IORs
                f.write(bsdf_id + ".indexOfRefraction = float(" + str(eta) + ")" + '\n')

    if elem.attrib['type'] == "plastic":
        specularcol = "0.2,0.2,0.2"
        roughness = 0
        for child_elem in elem:
            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "diffuse_reflectance":
                basecol = child_elem.attrib['value']
                specularcol = [ (1.0 - float(kd)) for kd in basecol.split(',')]
                specularcol = np.array2string(specularcol, separator=',')[1:-1]

            if child_elem.tag == "texture" and child_elem.attrib['name'] == "diffuse_reflectance":
                basecol_texture = True
                for child2 in child_elem:
                    if child2.tag == "string" and child2.attrib['name'] == "filename":
                        f.write(bsdf_id + ".loadTexture(MaterialTextureSlot.BaseColor, '" + scenepath + '/' +
                                child2.attrib['value'] + "')" + '\n')

            if child_elem.tag == "float" and child_elem.attrib['name'] == "int_ior":
                f.write(bsdf_id + ".indexOfRefraction = float(" + child_elem.attrib['value'] + ")" + '\n')

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "specular_reflectance":
                specularcol = child_elem.attrib['value']

    if elem.attrib['type'] == "roughplastic":
        specularcol = "0.2,0.2,0.2"
        for child_elem in elem:
            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "diffuse_reflectance":
                basecol = child_elem.attrib['value'] 
                specularcol = np.array([ 1.0 - float(kd) for kd in basecol.split(',')]) 
                specularcol = np.array2string(specularcol, separator=',')[1:-1]

            if child_elem.tag == "texture" and child_elem.attrib['name'] == "diffuse_reflectance":
                basecol_texture = True
                for child2 in child_elem:
                    if child2.tag == "string" and child2.attrib['name'] == "filename":
                        f.write(
                            bsdf_id + ".loadTexture(MaterialTextureSlot.BaseColor, '" + scenepath + '/' + child2.attrib[
                                'value'] + "')" + '\n')

            if child_elem.tag == "float" and child_elem.attrib['name'] == "int_ior":
                f.write(bsdf_id + ".indexOfRefraction = float(" + child_elem.attrib['value'] + ")" + '\n')

            if child_elem.tag == "float" and child_elem.attrib['name'] == "alpha":
                roughness = child_elem.attrib['value']

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "specular_reflectance":
                specularcol = child_elem.attrib['value']

    if elem.attrib['type'] == "dielectric":
        specularcol = "1,1,1"
        speculartrans = "1"
        roughness = 0
        for child_elem in elem:
            if child_elem.tag == "float" and child_elem.attrib['name'] == "int_ior":
                f.write(bsdf_id + ".indexOfRefraction = float(" + child_elem.attrib['value'] + ")" + '\n')

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "specular_reflectance":
                specularcol = child_elem.attrib['value']

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "specular_transmittance":
                speculartrans = str(np.matrix(child_elem.attrib['value']).item(0))

        f.write(bsdf_id + ".specularTransmission = float(" + speculartrans + ")" + '\n')
        f.write(bsdf_id + ".nestedPriority = 2" + '\n')
        f.write(bsdf_id + ".doubleSided = bool(" + str(True) + ")" + '\n')
        # basecol_texture = True

    if elem.attrib['type'] == "roughdielectric":
        specularcol = "1,1,1"
        speculartrans = "1"
        for child_elem in elem:
            if child_elem.tag == "float" and child_elem.attrib['name'] == "int_ior":
                f.write(bsdf_id + ".indexOfRefraction = float(" + child_elem.attrib['value'] + ")" + '\n')

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "specular_reflectance":
                specularcol = child_elem.attrib['value']

            if child_elem.tag == "rgb" and child_elem.attrib['name'] == "specular_transmittance":
                speculartrans = str(np.matrix(child_elem.attrib['value']).item(0))

            if child_elem.tag == "float" and child_elem.attrib['name'] == "alpha":
                roughness = child_elem.attrib['value']

        f.write(bsdf_id + ".specularTransmission = float(" + speculartrans + ")" + '\n')
        f.write(bsdf_id + ".nestedPriority = 2" + '\n')
        f.write(bsdf_id + ".doubleSided = bool(" + str(True) + ")" + '\n')
        # basecol_texture = True # seems like transparency needs a basecol (1)

    if not basecol_texture:
        f.write(bsdf_id + ".baseColor = float4(" + basecol + ",0)" + '\n')

    # The model uses glossiness instead of roughness. Also Falcor's "linear roughness" seems to refer to the square root of Mitsuba's roughness:
    roughness = math.sqrt(np.matrix(roughness).item(0))
    f.write(bsdf_id + ".specularParams = float4(" + specularcol + "," + str(1.0 - roughness) + ")" + '\n')
    return bsdf_id

def writeEmittertopyscene(f, elem, id = None):
    f.write('\n')
    # TODO : do local light sources too
    if elem.attrib['type'] == "envmap":
        bsdf_id = id
        envmap_path = scenepath
        for child_elem in elem:
            if child_elem.tag == "string" and child_elem.attrib['name'] == "filename":
                envmap_path = envmap_path + '/' + child_elem.attrib['value']
                # TODO : also retrieve other parameters (for example multiplier of envmap) envMap.intensity = 1
                f.write("envMap = EnvMap('" + envmap_path + "')" + '\n')
                f.write("sceneBuilder.envMap = envMap" + '\n')

    if elem.attrib['type'] == "area":
        intensity = np.ones(3)

        if 'id' in elem.attrib.keys():
            bsdf_id = elem.attrib['id']
        else:
            bsdf_id = id

        for child_elem in elem:
            if child_elem.attrib['name'] == "radiance":
                if child_elem.tag == "rgb":
                    intensity = child_elem.attrib['value']

                if child_elem.tag == "spectrum":
                    li = child_elem.attrib['value']
                    intensity = li + "," + li + "," + li

        f.write(bsdf_id + " = Material('" + bsdf_id + "')" + '\n')
        f.write(bsdf_id + ".baseColor = float4(0,0,0,0)" + '\n')
        f.write(bsdf_id + ".emissiveColor = float3(" + str(intensity) + ")" + '\n')
        f.write(bsdf_id + ".emissiveFactor = float(1.0)" + '\n')

    f.write('\n')
    return bsdf_id


tree = ET.parse(mitsuba2xml)
root = tree.getroot()
scenepath = os.path.dirname(mitsuba2xml)

mesh_ids = []
mesh_mat_ids = []
transforms = []
rectangleCounter = 0

with open(falcorscript, 'w') as f:

    # # TODO : verify scene builder flags... nevermind they're read-only
    # f.write("sceneBuilder.flags.UseSpecGlossMaterials = True" + '\n')
    # f.write("sceneBuilder.flags.UseMetalRoughMaterials = False" + '\n')

    f.write("DefaultMat = Material('DefaultMat')" + '\n')
    
    for elem in root:

        if elem.tag == "sensor":
            f.write('\n')
            f.write("camera = Camera('Camera')" + '\n')
            for child_elem in elem:
                if child_elem.tag == "transform" and child_elem.attrib['name'] == "to_world":
                    if child_elem[0].tag == "matrix":
                        # TODO convert matrix to camera vectors
                        print("Cant handle camera matrix, not implemented yet")

                    if child_elem[0].tag == "lookat":
                        f.write("camera.position = float3(" + child_elem[0].attrib['origin'] + ")" + '\n')
                        f.write("camera.target = float3(" + child_elem[0].attrib['target'] + ")" + '\n')
                        f.write("camera.up = float3(" + child_elem[0].attrib['up'] + ")" + '\n')

                if child_elem.tag == "float" and child_elem.attrib['name'] == "fov":
                    # TODO extract meaningful focal length from FOV
                    f.write("camera.focalLength = 35" + '\n')

            f.write("sceneBuilder.addCamera(camera)" + '\n')
            f.write('\n')

        if elem.tag == "emitter":
            writeEmittertopyscene(f, elem)

        # TODO : not handling shape groups atm
        if elem.tag == "shape":
            objpath = scenepath
            obj_tranf = False
            bsdf_id = False
            is_obj = True

            if elem.attrib['type'] == "rectangle": #Falcor rectange goes from -.5 to .5 with normal Y. Mitsuba rectangle goes from -1 to 1 with normal Z
                mesh_ids.append("quad_" + str(rectangleCounter))
                rectangleCounter = rectangleCounter + 1
                f.write(mesh_ids[-1] + " = TriangleMesh.createQuad()" + '\n')
                is_obj = False

            for child_elem in elem:
                if child_elem.tag == "string" and child_elem.attrib['name'] == "filename":
                    objpath = objpath + '/' + child_elem.attrib['value']
                    mesh_ids.append(os.path.splitext(os.path.basename(objpath))[0] + "_object" + str(len(mesh_ids)))
                    f.write(mesh_ids[-1] + " = TriangleMesh.createFromFile('" + objpath + "')" + '\n')

                if child_elem.tag == "transform" and child_elem.attrib['name'] == "to_world":
                    obj_tranf = True
                    for child_elem2 in child_elem:
                        s = "1,1,1"
                        t = "0,0,0"
                        reuler = "0,0,0"

                        if child_elem2.tag=="matrix":
                            tmat = np.matrix(child_elem[0].attrib['value']).reshape(4,4).astype(np.float32)
                            # transforms.append(child_elem[0].attrib['value'].replace(" ", ","))

                            s, r, t = extractTransform(tmat)
                            reuler = rotationMatrixToEulerAngles(r)

                            if elem.attrib['type'] == "rectangle":
                                reuler[0] = reuler[0] + 90
                                s = s * 2

                            s = np.array2string(s, separator=',')[1:-1]
                            reuler = np.array2string(reuler, separator=',')[1:-1]
                            t = np.array2string(t[:], separator=',')[1:-1]

                        if child_elem2.tag == "rotate":
                            ang = np.float32(child_elem2.attrib['angle']) * np.pi / 180.0
                            r = np.zeros((3,3))
                            if 'x' in child_elem2.attrib:
                                r = np.matrix([[1, 0, 0],
                                           [0, math.cos(ang), -math.sin(ang)],
                                           [0, math.sin(ang), math.cos(ang)]])
                            if 'y' in child_elem2.attrib:
                                r =  np.matrix([[math.cos(ang), 0, math.sin(ang)],
                                                  [0, 1, 0],
                                                  [-math.sin(ang), 0, math.cos(ang)]])
                            if 'z' in child_elem2.attrib:
                                r =  np.matrix([[math.cos(ang), -math.sin(ang), 0],
                                                  [math.sin(ang), math.cos(ang), 0],
                                                  [0, 0, 1]])
                            reuler = rotationMatrixToEulerAngles(r)

                            if elem.attrib['type'] == "rectangle":
                                reuler[0] = reuler[0] + 90
                                s = s * 2

                            reuler = np.array2string(reuler, separator=',')[1:-1]

                        if child_elem2.tag == "translate":
                            t = np.zeros(3)
                            if 'x' in child_elem2.attrib:
                                t[0] = child_elem2.attrib['x']
                            if 'y' in child_elem2.attrib:
                                t[1] = child_elem2.attrib['y']
                            if 'z' in child_elem2.attrib:
                                t[2] = child_elem2.attrib['z'] 
                            t = np.array2string(t, separator=',')[1:-1]
                            
                        if child_elem2.tag == "scale":
                            scale = np.zeros(3)
                            if 'x' in child_elem2.attrib:
                                scale[0] = child_elem2.attrib['x']
                            if 'y' in child_elem2.attrib:
                                scale[1] = child_elem2.attrib['y']
                            if 'z' in child_elem2.attrib:
                                scale[2] = child_elem2.attrib['z']
                            s = np.array2string(scale, separator=',')[1:-1]

                        transforms.append([s, reuler, t])

                if child_elem.tag == "ref":
                    mesh_mat_ids.append(child_elem.attrib['id'])
                    bsdf_id = True

                if child_elem.tag == "bsdf":
                    if is_obj:
                        temp_id = os.path.splitext(os.path.basename(objpath))[0] + "_bsdf"
                    else:
                        temp_id = mesh_ids[-1] + "_bsdf"
                    temp_id = writeBSDFtopyscene(f, child_elem, id = temp_id)
                    mesh_mat_ids.append(temp_id)
                    bsdf_id = True

                if child_elem.tag == "emitter":
                    if is_obj:
                        temp_id = os.path.splitext(os.path.basename(objpath))[0] + "_emitter"
                    else:
                        temp_id = mesh_ids[-1] + "_emitter"
                    temp_id = writeEmittertopyscene(f, child_elem, id = temp_id)
                    mesh_mat_ids.append(temp_id)
                    bsdf_id = True

            if bsdf_id == False:
                print("One of the objects does not have a BSDF: ", os.path.splitext(os.path.basename(objpath))[0])
                mesh_mat_ids.append(None)


            if not obj_tranf:
                transforms.append(None)

        if elem.tag == "bsdf": # TODO : not handling thindielectrics, nor normal/opacity maps atm. Also handle textures for roughness and specular
            writeBSDFtopyscene(f, elem)


    # apply materials to meshes
    for i in range(len(mesh_ids)):

        # print(mesh_ids[i])
        # print(mesh_mat_ids[i])

        f.write('\n')
        if mesh_mat_ids[i] is not None:
            f.write("mesh_" + str(i) + " = sceneBuilder.addTriangleMesh(" + mesh_ids[i] + ", " + mesh_mat_ids[i] + ")" + '\n')
        else:
            f.write("mesh_" + str(i) + " = sceneBuilder.addTriangleMesh(" + mesh_ids[i] + ", DefaultMat)" + '\n')
            
        # TODO : Verify the coordinate systems !!!

        if transforms[i] is None:
            f.write("node_" + str(i) + " = sceneBuilder.addNode('node_" + str(i) + "')" + '\n')
        else:
            # create a node for each object to apply the transform
            s,reuler,t = transforms[i]
            f.write("node_"+str(i)+" = sceneBuilder.addNode('node_"+str(i)+
                    "', Transform(scaling=float3("+ s + "), rotationEulerDeg=float3("+ reuler +
                      "),translation=float3("+ t + ")) )" + '\n')

        # combine the node and the mesh
        f.write("sceneBuilder.addMeshInstance(node_" + str(i) + ", mesh_"+str(i)+")" + '\n')

