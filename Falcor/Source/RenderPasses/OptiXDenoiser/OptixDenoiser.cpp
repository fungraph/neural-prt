/***************************************************************************
 # Copyright (c) 2020, NVIDIA CORPORATION.  All rights reserved.
 #
 # NVIDIA CORPORATION and its licensors retain all intellectual property
 # and proprietary rights in and to this software, related documentation
 # and any modifications thereto.  Any use, reproduction, disclosure or
 # distribution of this software and related documentation without an express
 # license agreement from NVIDIA CORPORATION is strictly prohibited.
 **************************************************************************/
#include "OptixDenoiser.h"
#include "CudaUtils.h"

namespace
{
    const char kDesc[] = "Apply the OptiX AI Denoiser";

    const Gui::DropdownList kInputsChoices =
    {
        { (uint32_t)OptixDenoiser::Inputs::Auto, "Automatic" },
        { (uint32_t)OptixDenoiser::Inputs::Color, "Color Only" },
        { (uint32_t)OptixDenoiser::Inputs::ColorAlbedo, "Color + Albedo" },
        { (uint32_t)OptixDenoiser::Inputs::ColorAlbedoNormal, "Color + Albedo + Normal" }
    };

    const Gui::DropdownList kModelChoices =
    {
        { (uint32_t)OptixDenoiser::Model::LDR, "LDR Denoising" },
        { (uint32_t)OptixDenoiser::Model::HDR, "HDR Denoising" }
    };

    const char kColorInput[] = "color";
    const char kAlbedoInput[] = "albedo";
    const char kNormalInput[] = "normal";
    const char kOutput[] = "output";

    const char kEnabled[] = "enabled";
    const char kInputs[] = "inputs";
    const char kModel[] = "model";
    const char kBlend[] = "blend";
    const char kDenoiseAlpha[] = "denoiseAlpha";
    const char kOutputFormat[] = "outputFormat";

    const std::string kConvertTexToBufFile = "RenderPasses/OptixDenoiser/ConvertTexToBuf.cs.slang";
    const std::string kConvertBufToTexFile = "RenderPasses/OptixDenoiser/ConvertBufToTex.ps.slang";

    const Falcor::Resource::BindFlags   kSharedBufferFlags = Resource::BindFlags::ShaderResource | Resource::BindFlags::UnorderedAccess | Resource::BindFlags::RenderTarget | Resource::BindFlags::Shared;
};

static void regOptixDenoiser(pybind11::module& m)
{
    pybind11::class_<OptixDenoiser, RenderPass, OptixDenoiser::SharedPtr> pass(m, "OptixDenoiserPass");
    pass.def_property(kEnabled, &OptixDenoiser::getEnabled, &OptixDenoiser::setEnabled);

    pybind11::enum_<OptixDenoiser::Inputs> inputs(m, "OptixDenoiserInputs");
    inputs.value("Auto", OptixDenoiser::Inputs::Auto);
    inputs.value("Color", OptixDenoiser::Inputs::Color);
    inputs.value("ColorAlbedo", OptixDenoiser::Inputs::ColorAlbedo);
    inputs.value("ColorAlbedoNormal", OptixDenoiser::Inputs::ColorAlbedoNormal);

    pybind11::enum_<OptixDenoiser::Model> model(m, "OptixDenoiserModel");
    model.value("LDR", OptixDenoiser::Model::LDR);
    model.value("HDR", OptixDenoiser::Model::HDR);
}

// Don't remove this. it's required for hot-reload to function properly
extern "C" __declspec(dllexport) const char* getProjDir()
{
    return PROJECT_DIR;
}

extern "C" __declspec(dllexport) void getPasses(Falcor::RenderPassLibrary& lib)
{
    lib.registerClass("OptixDenoiser", kDesc, OptixDenoiser::create);
    ScriptBindings::registerBinding(regOptixDenoiser);
}

OptixDenoiser::OptixDenoiser(const Dictionary& dict)
{
    for (const auto& [key, value] : dict)
    {
        if (key == kEnabled) mEnabled = value;
        else if (key == kInputs) mInputs = value;
        else if (key == kModel) mModel = value;
        else if (key == kBlend) mBlend = value;
        else if (key == kDenoiseAlpha) mDenoiseAlpha = value;
        else if (key == kOutputFormat) mOutputFormat = dict[kOutputFormat];
        else logWarning("Unknown field '" + key + "' in a OptixDenoiser dictionary");
    }

    mpConvertTexToBuf = ComputePass::create(kConvertTexToBufFile, "main");
    mpConvertBufToTex = FullScreenPass::create(kConvertBufToTexFile);
    mpFbo = Fbo::create();
}

OptixDenoiser::SharedPtr OptixDenoiser::create(RenderContext* pRenderContext, const Dictionary& dict)
{
    return SharedPtr(new OptixDenoiser(dict));
}

std::string OptixDenoiser::getDesc() { return kDesc; }

Dictionary OptixDenoiser::getScriptingDictionary()
{
    Dictionary d;
    d[kEnabled] = mEnabled;
    d[kInputs] = mInputs;
    d[kModel] = mModel;
    d[kBlend] = mBlend;
    d[kDenoiseAlpha] = mDenoiseAlpha;
    if (mOutputFormat != ResourceFormat::Unknown) d[kOutputFormat] = mOutputFormat;

    return d;
}

RenderPassReflection OptixDenoiser::reflect(const CompileData& compileData)
{
    // Define the required resources here
    RenderPassReflection r;
    r.addInput(kColorInput, "Color input");
    r.addInput(kAlbedoInput, "Albedo input").flags(RenderPassReflection::Field::Flags::Optional);
    r.addInput(kNormalInput, "Normal input").flags(RenderPassReflection::Field::Flags::Optional);
    auto fmt = mOutputFormat != ResourceFormat::Unknown ? mOutputFormat : ResourceFormat::RGBA32Float;
    r.addOutput(kOutput, "Denoised output").format(fmt);
    return r;
}

void OptixDenoiser::compile(RenderContext* pContext, const CompileData& compileData)
{
    if (!initializeOptix()) { return; }

    // Determine available inputs
    bool hasColorInput = (compileData.connectedResources.getField(kColorInput) != nullptr);
    bool hasAlbedoInput = (compileData.connectedResources.getField(kAlbedoInput) != nullptr);
    bool hasNormalInput = (compileData.connectedResources.getField(kNormalInput) != nullptr);
    if (hasColorInput && hasAlbedoInput && hasNormalInput) mAvailableInputs = Inputs::ColorAlbedoNormal;
    else if (hasColorInput && hasAlbedoInput) mAvailableInputs = Inputs::ColorAlbedo;
    else mAvailableInputs = Inputs::Color;

    // (Re-)allocate temporary buffers when render resolution changes
    uint2 newSize = compileData.defaultTexDims;

    if (newSize != mBufferSize && newSize.x > 0 && newSize.y > 0)
    {
        mBufferSize = newSize;

        // Create staging buffers for transferring memory to and from the Optix denoiser
        for (int i = 0; i < 4; i++)
        {
            auto& stageBuffer = mStageBuffers[i];

            if (stageBuffer.devicePtr) freeSharedDevicePtr((void*)stageBuffer.devicePtr);

            stageBuffer.buffer = Buffer::createTyped<float4>(mBufferSize.x * mBufferSize.y, kSharedBufferFlags);
            stageBuffer.devicePtr = (CUdeviceptr)exportBufferToCudaDevice(stageBuffer.buffer);

            stageBuffer.image.width = mBufferSize.x;
            stageBuffer.image.height = mBufferSize.y;
            stageBuffer.image.rowStrideInBytes = mBufferSize.x * 4 * sizeof(float);
            stageBuffer.image.pixelStrideInBytes = 4 * sizeof(float);
            stageBuffer.image.format = OPTIX_PIXEL_FORMAT_FLOAT4;
            stageBuffer.image.data = stageBuffer.devicePtr;
        }

        mRecreateDenoiser = true;
    }
}

void OptixDenoiser::execute(RenderContext* pContext, const RenderData& data)
{
    if (mEnabled)
    {
        if (mRecreateDenoiser)
        {
            setupDenoiser();
            mRecreateDenoiser = false;
        }

        // Copy input textures to buffers
        convertTexToBuf(pContext, data[kColorInput]->asTexture(), mStageBuffers[0].buffer, mBufferSize);
        if (mDenoiser.inputCount > 1) convertTexToBuf(pContext, data[kAlbedoInput]->asTexture(), mStageBuffers[1].buffer, mBufferSize);
        if (mDenoiser.inputCount > 2) convertTexToBuf(pContext, data[kNormalInput]->asTexture(), mStageBuffers[2].buffer, mBufferSize);

        // TODO: Find a better way to synchronize
        // https://gitlab-master.nvidia.com/nvresearch-gfx/Tools/Falcor/issues/772
        pContext->flush(true);

        // Update denoiser parameters
        mDenoiser.params.denoiseAlpha = mDenoiseAlpha ? 1 : 0;
        mDenoiser.params.hdrIntensity = (CUdeviceptr)0;
        mDenoiser.params.blendFactor = glm::clamp(mBlend, 0.f, 1.f);

        // Run denoiser
        optixDenoiserInvoke(mDenoiser.instance,
            /*stream*/0,
            &mDenoiser.params,
            mDenoiser.stateBuffer.getDevicePtr(), mDenoiser.stateBuffer.getSize(),
            mDenoiser.inputs, mDenoiser.inputCount,
            /*inputOffsetX*/ 0,
            /*inputOffsetY*/ 0,
            &mDenoiser.output,
            mDenoiser.scratchBuffer.getDevicePtr(), mDenoiser.scratchBuffer.getSize());

        // Copy output buffer to texture
        convertBufToTex(pContext, mStageBuffers[3].buffer, data[kOutput]->asTexture(), mBufferSize);
    }
    else
    {
        pContext->blit(data[kColorInput]->asTexture()->getSRV(), data[kOutput]->asTexture()->getRTV());
    }
}

void OptixDenoiser::renderUI(Gui::Widgets& widget)
{
    widget.checkbox("Enabled", mEnabled);

    if (mEnabled)
    {
        if (uint32_t inputs = uint32_t(mInputs); widget.dropdown("Inputs", kInputsChoices, inputs))
        {
            mInputs = Inputs(inputs);
            mRecreateDenoiser = true;
        }
        widget.tooltip("Inputs used for denoising.");

        if (uint32_t model = uint32_t(mModel); widget.dropdown("Model", kModelChoices, model))
        {
            mModel = Model(model);
            mRecreateDenoiser = true;
        }
        widget.tooltip("Selects the denosing model.\nHDR targets dynamic range of [0..10,000] for best results.");

        widget.slider("Blend", mBlend, 0.f, 1.f);
        widget.tooltip("Blend between denoised and original input.");

        widget.checkbox("Denoise Alpha", mDenoiseAlpha);
        widget.tooltip("Enable denoising the alpha channel.");
    }
}

void *OptixDenoiser::exportBufferToCudaDevice(Buffer::SharedPtr &buf)
{
    if (buf == nullptr) return nullptr;

    return getSharedDevicePtr(buf->getSharedApiHandle(), (uint32_t)buf->getSize());
}

bool OptixDenoiser::initializeOptix()
{
    if (!mOptixInitialized) mOptixInitialized = initOptix(mOptixContext) >= 0;

    return mOptixInitialized;
}

void OptixDenoiser::setupDenoiser()
{
    // Destroy the denoiser, if it already exists
    if (mDenoiser.instance)
    {
        optixDenoiserDestroy(mDenoiser.instance);
    }

    // Determine active inputs
    mActiveInputs = mInputs == Inputs::Auto ?
        mAvailableInputs :
        Inputs(glm::min(uint32_t(mInputs), uint32_t(mAvailableInputs)));

    // Set selected inputs in GUI to active inputs if not in auto mode
    if (mInputs != Inputs::Auto) mInputs = mActiveInputs;

    OptixDenoiserInputKind inputKind = OPTIX_DENOISER_INPUT_RGB;
    switch (mActiveInputs)
    {
        case Inputs::Color:             inputKind = OPTIX_DENOISER_INPUT_RGB;               break;
        case Inputs::ColorAlbedo:       inputKind = OPTIX_DENOISER_INPUT_RGB_ALBEDO;        break;
        case Inputs::ColorAlbedoNormal: inputKind = OPTIX_DENOISER_INPUT_RGB_ALBEDO_NORMAL; break;
        default: break;
    }

    // Initialize the denoiser options appropriately
    mDenoiser.options.inputKind = inputKind; // What type of inputs are we sending the denoiser?
    mDenoiser.options.pixelFormat = OPTIX_PIXEL_FORMAT_FLOAT4; // Take noisy color in fp32 format

    // Create the denoiser
    optixDenoiserCreate(mOptixContext, &mDenoiser.options, &mDenoiser.instance);
    optixDenoiserSetModel(mDenoiser.instance, mModel == Model::LDR ? OPTIX_DENOISER_MODEL_KIND_LDR : OPTIX_DENOISER_MODEL_KIND_HDR, nullptr, 0);

    // Find out how much memory is needed
    optixDenoiserComputeMemoryResources(mDenoiser.instance, mBufferSize.x, mBufferSize.y, &mDenoiser.sizes);

    // Allocate/resize some temporary CUDA buffers for internal OptiX processing/state
    mDenoiser.scratchBuffer.resize(mDenoiser.sizes.recommendedScratchSizeInBytes);
    mDenoiser.stateBuffer.resize(mDenoiser.sizes.stateSizeInBytes);

    // Finish setup of the denoiser
    optixDenoiserSetup(mDenoiser.instance, 0, mBufferSize.x, mBufferSize.y,
        mDenoiser.stateBuffer.getDevicePtr(), mDenoiser.stateBuffer.getSize(),
        mDenoiser.scratchBuffer.getDevicePtr(), mDenoiser.scratchBuffer.getSize());

    // Set up our input and output images
    mDenoiser.inputCount = uint32_t(mActiveInputs) - uint32_t(Inputs::Color) + 1;
    mDenoiser.inputs[0] = mStageBuffers[0].image;
    mDenoiser.inputs[1] = mStageBuffers[1].image;
    mDenoiser.inputs[2] = mStageBuffers[2].image;
    mDenoiser.output = mStageBuffers[3].image;
}

void OptixDenoiser::convertTexToBuf(RenderContext* pContext, const Texture::SharedPtr& tex, const Buffer::SharedPtr& buf, const uint2& size)
{
    auto vars = mpConvertTexToBuf->getVars();
    vars["GlobalCB"]["gStride"] = size.x;
    vars["gInTex"] = tex;
    vars["gOutBuf"] = buf;
    mpConvertTexToBuf->execute(pContext, size.x, size.y);
}

void OptixDenoiser::convertBufToTex(RenderContext* pContext, const Buffer::SharedPtr& buf, const Texture::SharedPtr& tex, const uint2& size)
{
    auto vars = mpConvertBufToTex->getVars();
    vars["GlobalCB"]["gStride"] = size.x;
    vars["gInBuf"] = buf;
    mpFbo->attachColorTarget(tex, 0);
    mpConvertBufToTex->execute(pContext, mpFbo);
}
