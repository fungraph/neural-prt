/***************************************************************************
 # Copyright (c) 2020, NVIDIA CORPORATION.  All rights reserved.
 #
 # NVIDIA CORPORATION and its licensors retain all intellectual property
 # and proprietary rights in and to this software, related documentation
 # and any modifications thereto.  Any use, reproduction, disclosure or
 # distribution of this software and related documentation without an express
 # license agreement from NVIDIA CORPORATION is strictly prohibited.
 **************************************************************************/

/* This is a simple encapsulating of the OptiX Denoiser. It is not guaranteed
   to be optimal.  In fact, it is likely it is wildly suboptimal, since the
   goal is to keep this flexible for use in *any* Falcor render graph without
   the user needing to be aware of any requirements for
   Falcor <-> OptiX <-> CUDA resource interop.

   This means there *are* extraneous resource copies that could be optimized
   away for users who know what they are doing.  Quick testing suggests this
   pass adds a ~50% overhead (4-5ms) on top of the OptiX denoiser running at 1080p.

   Using this pass:
     * Connect noisy color image to the "color" pass texture
          - Can be LDR or HDR.  OptiX claims HDR mode expects values in the range
            [0...10000], though my testing shows the HDR model works equivalently
            (to the LDR model) on LDR inputs... so this pass defaults to HDR.
     * (Optionally) connect non-noisy albedo and normals to the "albedo" and
       "normal" pass inputs.  Think:  These come directly from your G-buffer.
     * Denoised results get output to the "output" pass texture
     * Basic UI controls various settings.  Most changes in the UI are costly,
       since I rebuild the denoiser whenever parameters are not obviously mutable
       via the OptiX API.  This might be avoidable with better internal caching.
       There may be resource leaks with frequent UI/parameter changes; resource
       cleanup is not clearly documented for the denoiser.  So user beware.
     * The following parameters can be used in Python / scripting to control
       startup / initial default settings:
          - inputs [OptixDenoiserInputs.Automatic/Color/ColorAlbedo/ColorAlbedoNormal]
          - model [OptixDenoiserModel.LDR/HDR]
          - denoiseAlpha [True/False]:  Should denoising run on alpha channel of input?
          - blend [0...1]:  Blends output between denoised and noisy input image
               (here 0 means fully denoised, which is the default)
     * The pass currently neither sets nor responds to any parameters on the shared
       internal, inter-pass Python dictionary.
 */

#pragma once

#include "Falcor.h"
#include "FalcorExperimental.h"

// Optix headers define a struct OptixDenoiser that clashes with our render pass
// class. We redefine the optix name such that it does not clash with our name.
// This is hacky but as it is confined to this render pass, it should be fine.
#define OptixDenoiser OptixDenoiserInstance
#include "CudaUtils.h"
#undef OptixDenoiser

using namespace Falcor;

class OptixDenoiser : public RenderPass
{
public:
    using SharedPtr = std::shared_ptr<OptixDenoiser>;

    enum class Inputs {
        Auto,
        Color,
        ColorAlbedo,
        ColorAlbedoNormal,
    };

    enum class Model {
        LDR,
        HDR
    };

    static SharedPtr create(RenderContext* pRenderContext, const Dictionary& dict);

    virtual std::string getDesc() override;
    virtual Dictionary getScriptingDictionary() override;
    virtual RenderPassReflection reflect(const CompileData& compileData) override;
    virtual void compile(RenderContext* pContext, const CompileData& compileData) override;
    virtual void execute(RenderContext* pRenderContext, const RenderData& renderData) override;
    virtual void renderUI(Gui::Widgets& widget) override;

    // Scripting functions
    bool getEnabled() const { return mEnabled; }
    void setEnabled(bool enabled) { mEnabled = enabled; }

private:
    OptixDenoiser(const Dictionary& dict);

    void *exportBufferToCudaDevice(Buffer::SharedPtr &buf);

    bool initializeOptix();
    void setupDenoiser();

    void convertTexToBuf(RenderContext* pContext, const Texture::SharedPtr& tex, const Buffer::SharedPtr& buf, const uint2& size);
    void convertBufToTex(RenderContext* pContext, const Buffer::SharedPtr& buf, const Texture::SharedPtr& tex, const uint2& size);

    // Options
    bool                        mEnabled = true;
    Inputs                      mInputs = Inputs::Auto;
    Model                       mModel = Model::HDR;
    float                       mBlend = 0.f;
    bool                        mDenoiseAlpha = true;
    ResourceFormat              mOutputFormat = ResourceFormat::Unknown;

    // Runtime state
    Inputs                      mAvailableInputs = Inputs::Color;
    Inputs                      mActiveInputs = Inputs::Color;
    uint2                       mBufferSize = uint2(0, 0);

    bool                        mRecreateDenoiser = true;

    // Optix denoiser appears to expect images inputs and outputs as flat arrays
    //    (i.e., not CUDA arrays / textures with z-order internal memory layout).
    //    We can either bang on CUDA/OptiX to support that *or* we can convert layout
    //    on the DX size with a pre-/post-pass to convert to a flat array, then share
    //    those flat arrays with OptiX.  While not at all optimal, since we'd need to
    //    do a couple blit()s internally anyways (to avoid exposing the mess of Optix
    //    interop settings outside this render pass) this isn't much slower that a
    //    better-designed sharing of GPU memory between DX and OptiX.

    struct StageBuffer
    {
        Buffer::SharedPtr       buffer;
        CUdeviceptr             devicePtr = (CUdeviceptr)0;
        OptixImage2D            image;
    };

    StageBuffer                 mStageBuffers[4];

    // Optix context
    bool                        mOptixInitialized = false;
    OptixDeviceContext          mOptixContext = nullptr;

    // Denoiser
    struct
    {
        OptixDenoiserInstance   instance = nullptr;
        OptixDenoiserOptions    options;
        OptixDenoiserSizes      sizes;
        OptixDenoiserParams     params;

        CudaBuffer              scratchBuffer;
        CudaBuffer              stateBuffer;

        uint32_t                inputCount;
        OptixImage2D            inputs[3];
        OptixImage2D            output;

    } mDenoiser;

    ComputePass::SharedPtr      mpConvertTexToBuf;
    FullScreenPass::SharedPtr   mpConvertBufToTex;
    Fbo::SharedPtr              mpFbo;
};
