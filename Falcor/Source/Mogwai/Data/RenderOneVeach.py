def render_graph_PathTracerGraph():
    g = RenderGraph("PathTracerGraph")
    loadRenderPassLibrary("GBuffer.dll")
    loadRenderPassLibrary("ToneMapper.dll")
    loadRenderPassLibrary("MegakernelPathTracer.dll")
    loadRenderPassLibrary("OptixDenoiser.dll")
    g.addPass(createPass("OptixDenoiser", {}), "Denoiser")
    ToneMappingPass = createPass("ToneMapper", {'autoExposure': False, 'exposureCompensation': 0.0})
    g.addPass(ToneMappingPass, "ToneMappingPass")
    GBufferRT = createPass("GBufferRT", {'forceCullMode': False, 'cull': CullMode.CullBack, 'samplePattern': SamplePattern.Stratified, 'sampleCount': 16})
    g.addPass(GBufferRT, "GBufferRT")
# ref x #num frames
#    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=1024, maxBounces=7)})
    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=16, maxBounces=7)})
    g.addPass(MegakernelPathTracer, "MegakernelPathTracer")
    g.addEdge("GBufferRT.vbuffer", "MegakernelPathTracer.vbuffer")      # Required by ray footprint.
    g.addEdge("GBufferRT.posW", "MegakernelPathTracer.posW")
    g.addEdge("GBufferRT.normW", "MegakernelPathTracer.normalW")
    g.addEdge("GBufferRT.tangentW", "MegakernelPathTracer.tangentW")
    g.addEdge("GBufferRT.faceNormalW", "MegakernelPathTracer.faceNormalW")
    g.addEdge("GBufferRT.viewW", "MegakernelPathTracer.viewW")
    g.addEdge("GBufferRT.diffuseOpacity", "MegakernelPathTracer.mtlDiffOpacity")
    g.addEdge("GBufferRT.specRough", "MegakernelPathTracer.mtlSpecRough")
    g.addEdge("GBufferRT.emissive", "MegakernelPathTracer.mtlEmissive")
    g.addEdge("GBufferRT.matlExtra", "MegakernelPathTracer.mtlParams")
    g.addEdge("MegakernelPathTracer.color", "ToneMappingPass.src")
    g.addEdge("ToneMappingPass.dst", "Denoiser.color")
    g.addEdge("GBufferRT.diffuseOpacity", "Denoiser.albedo")
    g.addEdge("GBufferRT.normW",  "Denoiser.normal")
    g.markOutput("ToneMappingPass.dst")
    return g

PathTracerGraph = render_graph_PathTracerGraph()
try: m.addGraph(PathTracerGraph)
except NameError: None

# m.loadScene("E:\datasets\Stavros\\veach-ajar-falcor\scene2_falcor_new.pyscene")
m.loadScene("C:\Users\grainer.AD\Desktop\projects\data\mitsuba-falcor-conversion\stavrosscenes\bathroom.pyscene")

m.clock.pause()

renderFrame()
m.frameCapture.baseFilename = f"Veach-"
m.frameCapture.capture()

m.activeGraph.markOutput("Denoiser.output")
renderFrame()
m.frameCapture.baseFilename = f"VeachDen-"
m.frameCapture.capture()

exit()


