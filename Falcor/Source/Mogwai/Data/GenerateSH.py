# run the script with 
# ../Bin/x64/Debug/Mogwai.exe --script=../Source/Mogwai/Data/GenerateSH.py   --width=512 --height=512 --silent 



# # # BEDROOM
#basepath = "C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/data/"
basepath = "F:/gilles/Dropbox/INRIA/projects/nprt/data/"
scenepath = basepath + "assets/scene5-bedroom/scene.pyscene"
envmap_dir = "C:/Users/grainer/Documents/cluster/output/SHbasis"
out_dir = envmap_dir + "/bedroom"
campos = [3.57,	1.5,	2.99]
camtar = [2.87,	1.39,	2.28]


# # # ATELIER
basepath = "F:/gilles/Dropbox/INRIA/projects/nprt/data/"
scenepath = basepath + "assets/scene2-atelier/intermediatescene.pyscene"
envmap_dir = basepath + "scenes/prt/shbasis"
out_dir = basepath + "scenes/prt/atelier"
campos = [95.78	,67.5	,22.58	]
camtar = [95.12	,66.89,	22.15]


focal_len = 24 #
accumulator_frames = 300 # 200# 1# 60# 30
spp = 16# 3# 64

from falcor import *
import csv
import math
import time
import os
import random 
def render_graph_PathTracerGraph():
    g = RenderGraph('PathTracerGraph')
    loadRenderPassLibrary('AccumulatePass.dll')
    loadRenderPassLibrary('MegakernelPathTracer.dll')
    loadRenderPassLibrary('GBuffer.dll')
    loadRenderPassLibrary('ToneMapper.dll') 
    AccumulatePass = createPass('AccumulatePass', {'enableAccumulation': True, 'autoReset': True, 'precisionMode': AccumulatePrecision.Single, 'subFrameCount': 0})
    g.addPass(AccumulatePass, 'AccumulatePass')
    #SAN MIGUEL
    ToneMappingPass = createPass('ToneMapper', {'exposureCompensation': 0.0, 'autoExposure': True, 'filmSpeed': 100.0, 'whiteBalance': False, 'whitePoint': 6500.0, 'operator': ToneMapOp.Aces, 'clamp': True, 'whiteMaxLuminance': 1.0, 'whiteScale': 11.199999809265137, 'fNumber': 1.0, 'shutter': 1.0, 'exposureMode': ExposureMode.AperturePriority})
    g.addPass(ToneMappingPass, 'ToneMappingPass')
    #GBufferRT = createPass('GBufferRT', {'samplePattern': SamplePattern.Center, 'sampleCount': 1, 'disableAlphaTest': False, 'adjustShadingNormals': True, 'forceCullMode': False, 'cull': CullMode.CullBack, 'texLOD': LODMode.RayDifferentials})
    GBufferRT = createPass("GBufferRT", {'forceCullMode': False, 'cull': CullMode.CullBack, 'samplePattern': SamplePattern.Stratified, 'sampleCount': 16})
    g.addPass(GBufferRT, 'GBufferRT')
    MegakernelPathTracer = createPass('MegakernelPathTracer', {'mSharedParams': PathTracerParams(samplesPerPixel=spp, lightSamplesPerVertex=1, maxNonSpecularBounces=8, maxBounces=8, adjustShadingNormals=0, useVBuffer=0, forceAlphaOne=1, useAlphaTest=1, clampSamples=1, useMIS=1, clampThreshold=10.0, useLightsInDielectricVolumes=0, specularRoughnessThreshold=0.25, useBRDFSampling=1, useNestedDielectrics=1, useNEE=1, misHeuristic=1, misPowerExponent=2.0, probabilityAbsorption=0.20000000298023224, useRussianRoulette=0, useFixedSeed=0, useLegacyBSDF=0, disableCaustics=0, rayFootprintMode=0, rayConeMode=2, rayFootprintUseRoughness=0), 'mSelectedSampleGenerator': 1, 'mSelectedEmissiveSampler': EmissiveLightSamplerType.LightBVH, 'mUniformSamplerOptions': EmissiveUniformSamplerOptions(), 'mLightBVHSamplerOptions': LightBVHSamplerOptions(useBoundingCone=True, buildOptions=LightBVHBuilderOptions(splitHeuristicSelection=SplitHeuristic.BinnedSAOH, maxTriangleCountPerLeaf=10, binCount=16, volumeEpsilon=0.0010000000474974513, useLeafCreationCost=True, createLeavesASAP=True, useLightingCones=True, splitAlongLargest=False, useVolumeOverSA=False, allowRefitting=True, usePreintegration=True), useLightingCone=True, disableNodeFlux=False, useUniformTriangleSampling=True, solidAngleBoundMethod=SolidAngleBoundMethod.Sphere)})

    loadRenderPassLibrary("OptixDenoiser.dll")
    g.addPass(createPass("OptixDenoiser", {}), "Denoiser")

    #ToneMappingPass = createPass('ToneMapper', {'exposureCompensation': 0.0, 'autoExposure': True, 'filmSpeed': 100.0, 'whiteBalance': False, 'whitePoint': 6500.0, 'operator': ToneMapOp.Aces, 'clamp': True, 'whiteMaxLuminance': 1.0, 'whiteScale': 11.199999809265137, 'fNumber': 1.0, 'shutter': 1.0, 'exposureMode': ExposureMode.AperturePriority})
    #g.addPass(ToneMappingPass, 'ToneMappingPass') 
    #GBufferRT = createPass('GBufferRT', {'samplePattern': SamplePattern.Center, 'sampleCount': 1, 'disableAlphaTest': False, 'adjustShadingNormals': True, 'forceCullMode': False, 'cull': CullMode.CullBack, 'texLOD': LODMode.UseMip0})
    #g.addPass(GBufferRT, 'GBufferRT')
    #MegakernelPathTracer = createPass('MegakernelPathTracer', {'mSharedParams': PathTracerParams(samplesPerPixel=spp, lightSamplesPerVertex=lightsamples, maxNonSpecularBounces=32, maxBounces=32, adjustShadingNormals=0, useVBuffer=0, forceAlphaOne=1, useAlphaTest=1, clampSamples=1, useMIS=1, clampThreshold=10.0, useLightsInDielectricVolumes=0, specularRoughnessThreshold=0.25, useBRDFSampling=1, useNestedDielectrics=1, useNEE=1, misHeuristic=1, misPowerExponent=2.0, probabilityAbsorption=0.20000000298023224, useRussianRoulette=0, useFixedSeed=0, useLegacyBSDF=0, disableCaustics=0, rayFootprintMode=0, rayConeMode=2, rayFootprintUseRoughness=0), 'mSelectedSampleGenerator': 1, 'mSelectedEmissiveSampler': EmissiveLightSamplerType.LightBVH, 'mUniformSamplerOptions': EmissiveUniformSamplerOptions(), 'mLightBVHSamplerOptions': LightBVHSamplerOptions(useBoundingCone=True, buildOptions=LightBVHBuilderOptions(splitHeuristicSelection=SplitHeuristic.BinnedSAOH, maxTriangleCountPerLeaf=10, binCount=16, volumeEpsilon=0.0010000000474974513, useLeafCreationCost=True, createLeavesASAP=True, useLightingCones=True, splitAlongLargest=False, useVolumeOverSA=False, allowRefitting=True, usePreintegration=True), useLightingCone=True, disableNodeFlux=False, useUniformTriangleSampling=True, solidAngleBoundMethod=SolidAngleBoundMethod.Sphere)})
    g.addPass(MegakernelPathTracer, 'MegakernelPathTracer')
    g.addEdge('GBufferRT.vbuffer', 'MegakernelPathTracer.vbuffer')
    g.addEdge('GBufferRT.posW', 'MegakernelPathTracer.posW')
    g.addEdge('GBufferRT.normW', 'MegakernelPathTracer.normalW')
    g.addEdge('GBufferRT.tangentW', 'MegakernelPathTracer.tangentW')
    g.addEdge('GBufferRT.faceNormalW', 'MegakernelPathTracer.faceNormalW')
    g.addEdge('GBufferRT.viewW', 'MegakernelPathTracer.viewW')
    g.addEdge('GBufferRT.diffuseOpacity', 'MegakernelPathTracer.mtlDiffOpacity')
    g.addEdge('GBufferRT.specRough', 'MegakernelPathTracer.mtlSpecRough')
    g.addEdge('GBufferRT.emissive', 'MegakernelPathTracer.mtlEmissive')
    g.addEdge('GBufferRT.matlExtra', 'MegakernelPathTracer.mtlParams')
    g.addEdge('MegakernelPathTracer.color', 'AccumulatePass.input')
    g.addEdge('AccumulatePass.output', 'ToneMappingPass.src') 
    g.markOutput('ToneMappingPass.dst')
    g.markOutput('AccumulatePass.output')

    g.addEdge("ToneMappingPass.dst", "Denoiser.color")
    g.addEdge("GBufferRT.diffuseOpacity", "Denoiser.albedo")
    g.addEdge("GBufferRT.normW",  "Denoiser.normal")

    return g
m.addGraph(render_graph_PathTracerGraph())
m.frameCapture.outputDir = out_dir

if not os.path.exists(out_dir):
    os.makedirs(out_dir)
# LOAD ENVIRONMENT MAPS
envmap_files = []
for r, d, f in os.walk(envmap_dir):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            envmap_files.append(os.path.join(r, file))

# LOAD SCENE
m.loadScene(scenepath, buildFlags = SceneBuilderFlags.UseSpecGlossMaterials, info = 0.0) 
m.clock.pause()
m.scene.camera.up = float3(0,1,0)
m.scene.camera.focalLength = focal_len # manually matched
# Layers for learning
m.activeGraph.markOutput("GBufferRT.posW")
m.activeGraph.markOutput("GBufferRT.normW")
m.activeGraph.markOutput("GBufferRT.diffuseOpacity")
m.activeGraph.markOutput("GBufferRT.specRough")
m.activeGraph.markOutput("GBufferRT.alpha")
m.activeGraph.markOutput("GBufferRT.specShading")
m.activeGraph.markOutput("GBufferRT.transShading")
m.activeGraph.markOutput("GBufferRT.viewW")
print(">>>>>>>>>>>>> SCENE BOUNDING BOX: ", m.scene.bounds)  #	AABB	World space scene bounds (readonly))  


# IF USING DENOISER :
#renderFrame()
#m.activeGraph.markOutput("Denoiser.output")


m.profiler.enabled = True
#m.profiler.startCapture()

for i in range(len(envmap_files)):

    # change camera position
    m.scene.camera.position = float3(float(campos[0]), float(campos[1]), float(campos[2]))
    m.scene.camera.target = float3(camtar[0], camtar[1], camtar[2])


    # change envmap
    m.scene.setEnvMap(envmap_files[i % len(envmap_files)])


    # render and accumulate
    start = time.time()
    print("Start rendering")
    for acc in range(accumulator_frames):
        renderFrame()
    end = time.time()
    print("Rendering time:  ", end - start)

    #for mkey in m.profiler.events.keys():
    #    print(m.profiler.events[mkey])
    #m.profiler.clearEvents()

    m.frameCapture.baseFilename = f"{i:05d}"
    m.frameCapture.capture()



#capture = m.profiler.endCapture()
#m.profiler.enabled = False

#meanFrameTime = capture["events"]["/onFrameRender/gpuTime"]["stats"]["mean"]
#print("Mean frame time: ", meanFrameTime)




exit()
