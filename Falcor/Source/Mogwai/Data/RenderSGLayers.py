# run the script with
# ../Bin/x64/Debug/Mogwai.exe --script=../Source/Mogwai/Data/GenerateBuddhaTrainingData.py   --width=400 --height=400 --silent

# MODIFY THESE PATHS





# # # # BUDDHA SCENE:
#[[-66.981522, -76.794205, -63.656311], [93.015289, 51.702751, 86.803894]]
scenepath = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene1-buddha/trainscene.pyscene"
out_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/buddha/train3"
cameras_path = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene1-buddha/cameras.xyz"
envmap_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/train"

out_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/buddha/test3"
envmap_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/test"
camtar = [ [0, 20, 0], [3,22,-3] ]  
accumulator_frames = 45# 30
spp = 64# 128
lightsamples = 8
focal_len = 29 #
translate_camera = False


# # # # ATELIER SCENE: 
scenepath = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene2-atelier/intermediatescene.pyscene"
#out_dir = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/training-scenes/trainatelier"
out_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/atelier/highres"
#out_dir = "C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/data/scenes/atelier/train"
#out_dir = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/training-scenes/testatelier"
cameras_path = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene2-atelier/cameras-highres.xyz"
#cameras_path = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene2-atelier/cameras.xyz"
#envmap_dir = "C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/train"
envmap_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/highres"
#envmap_dir = "C:/Users/grainer.AD/Dropbox/INRIA/projects/nprt/data/scenes/indoorenvs/test"
camtar = [ [-60, -10, -25],  [-30, -10, -25],  [0, -10, -25] ]
focal_len = 21 # 

number_output_renders = 5# 200# 000# 100# 10000#    20#00 #10000 # 200#  2500# 100
blender_camera_scale = True #if cameras come from Blender they are in m not mm
accumulator_frames = 30
spp = 16# 64
lightsamples = 8



# # # # SAN MIGUEL SCENE:  TODO : make FOV wider so we see more of the scene (less of the table)
scenepath = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene4-sanmiguel/san-miguel-low-poly.obj" 
out_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/test" 
#out_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/sanmiguel/train" 
cameras_path = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene4-sanmiguel/cams.xyz" 
envmap_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test"
#envmap_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/train"
camtar = [ [16.7, 0.3, 6.5], [16.7, 0.5, 6.5] ]
cam_translation = [ 6.4, -6, 8.4]
translate_camera = True
#camtar = [ [10.3, 6.3, -1.9],  [10.2, 6.4, -1.8],  [10.4, 6.2, -1.99] ]
focal_len = 21 #
blender_camera_scale = False #if cameras come from Blender they are in m not mm. If the scene is an OBJ then no need to scale the camera positions.

number_output_renders = 21# 2500# 200# 000# 100# 10000#    20#00 #10000 # 200#  2500# 100
accumulator_frames = 40
spp = 64
lightsamples = 2



# # # # KITCHEN SCENE:  TODO : make FOV wider so we see more of the scene (less of the table)
scenepath = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene3-kitchen/kitchen2.pyscene" 
out_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/kitchen3/test" 
out_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/kitchen3/train" 
cameras_path = "F:/gilles/Dropbox/INRIA/projects/nprt/data/assets/scene3-kitchen/cams.xyz" 
envmap_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/test"
envmap_dir = "F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/outdoorenvs/train"
camtar = [ [-1.488517,0.9, -2.3], [-1.5, 0, -1.9035365] ]
cam_translation = [ 0,0,4.5]
translate_camera = True 
focal_len = 25 #
blender_camera_scale = False #if cameras come from Blender they are in m not mm. If the scene is an OBJ then no need to scale the camera positions.

number_output_renders = 200# 2500# 200# 000# 100# 10000#    20#00 #10000 # 200#  2500# 100
accumulator_frames = 150
spp = 16
lightsamples = 1




from falcor import *
import csv
import math
import time
import os
import random 
def render_graph_PathTracerGraph():
    g = RenderGraph('PathTracerGraph')
    loadRenderPassLibrary('AccumulatePass.dll')
    loadRenderPassLibrary('MegakernelPathTracer.dll')
    loadRenderPassLibrary('GBuffer.dll')
    loadRenderPassLibrary('ToneMapper.dll') 
    AccumulatePass = createPass('AccumulatePass', {'enableAccumulation': True, 'autoReset': True, 'precisionMode': AccumulatePrecision.Single, 'subFrameCount': 0})
    g.addPass(AccumulatePass, 'AccumulatePass')
    #SAN MIGUEL
    ToneMappingPass = createPass('ToneMapper', {'exposureCompensation': 0.0, 'autoExposure': True, 'filmSpeed': 100.0, 'whiteBalance': False, 'whitePoint': 6500.0, 'operator': ToneMapOp.Aces, 'clamp': True, 'whiteMaxLuminance': 1.0, 'whiteScale': 11.199999809265137, 'fNumber': 1.0, 'shutter': 1.0, 'exposureMode': ExposureMode.AperturePriority})
    g.addPass(ToneMappingPass, 'ToneMappingPass')
    GBufferRT = createPass('GBufferRT', {'samplePattern': SamplePattern.Center, 'sampleCount': 1, 'disableAlphaTest': False, 'adjustShadingNormals': True, 'forceCullMode': False, 'cull': CullMode.CullBack, 'texLOD': LODMode.UseMip0})
    g.addPass(GBufferRT, 'GBufferRT')
    MegakernelPathTracer = createPass('MegakernelPathTracer', {'mSharedParams': PathTracerParams(samplesPerPixel=spp, lightSamplesPerVertex=lightsamples, maxNonSpecularBounces=8, maxBounces=8, adjustShadingNormals=0, useVBuffer=0, forceAlphaOne=1, useAlphaTest=1, clampSamples=1, useMIS=1, clampThreshold=10.0, useLightsInDielectricVolumes=0, specularRoughnessThreshold=0.25, useBRDFSampling=1, useNestedDielectrics=1, useNEE=1, misHeuristic=1, misPowerExponent=2.0, probabilityAbsorption=0.20000000298023224, useRussianRoulette=0, useFixedSeed=0, useLegacyBSDF=0, disableCaustics=0, rayFootprintMode=0, rayConeMode=2, rayFootprintUseRoughness=0), 'mSelectedSampleGenerator': 1, 'mSelectedEmissiveSampler': EmissiveLightSamplerType.LightBVH, 'mUniformSamplerOptions': EmissiveUniformSamplerOptions(), 'mLightBVHSamplerOptions': LightBVHSamplerOptions(useBoundingCone=True, buildOptions=LightBVHBuilderOptions(splitHeuristicSelection=SplitHeuristic.BinnedSAOH, maxTriangleCountPerLeaf=10, binCount=16, volumeEpsilon=0.0010000000474974513, useLeafCreationCost=True, createLeavesASAP=True, useLightingCones=True, splitAlongLargest=False, useVolumeOverSA=False, allowRefitting=True, usePreintegration=True), useLightingCone=True, disableNodeFlux=False, useUniformTriangleSampling=True, solidAngleBoundMethod=SolidAngleBoundMethod.Sphere)})
    
    #ToneMappingPass = createPass('ToneMapper', {'exposureCompensation': 0.0, 'autoExposure': True, 'filmSpeed': 100.0, 'whiteBalance': False, 'whitePoint': 6500.0, 'operator': ToneMapOp.Aces, 'clamp': True, 'whiteMaxLuminance': 1.0, 'whiteScale': 11.199999809265137, 'fNumber': 1.0, 'shutter': 1.0, 'exposureMode': ExposureMode.AperturePriority})
    #g.addPass(ToneMappingPass, 'ToneMappingPass') 
    #GBufferRT = createPass('GBufferRT', {'samplePattern': SamplePattern.Center, 'sampleCount': 1, 'disableAlphaTest': False, 'adjustShadingNormals': True, 'forceCullMode': False, 'cull': CullMode.CullBack, 'texLOD': LODMode.UseMip0})
    #g.addPass(GBufferRT, 'GBufferRT')
    #MegakernelPathTracer = createPass('MegakernelPathTracer', {'mSharedParams': PathTracerParams(samplesPerPixel=spp, lightSamplesPerVertex=lightsamples, maxNonSpecularBounces=32, maxBounces=32, adjustShadingNormals=0, useVBuffer=0, forceAlphaOne=1, useAlphaTest=1, clampSamples=1, useMIS=1, clampThreshold=10.0, useLightsInDielectricVolumes=0, specularRoughnessThreshold=0.25, useBRDFSampling=1, useNestedDielectrics=1, useNEE=1, misHeuristic=1, misPowerExponent=2.0, probabilityAbsorption=0.20000000298023224, useRussianRoulette=0, useFixedSeed=0, useLegacyBSDF=0, disableCaustics=0, rayFootprintMode=0, rayConeMode=2, rayFootprintUseRoughness=0), 'mSelectedSampleGenerator': 1, 'mSelectedEmissiveSampler': EmissiveLightSamplerType.LightBVH, 'mUniformSamplerOptions': EmissiveUniformSamplerOptions(), 'mLightBVHSamplerOptions': LightBVHSamplerOptions(useBoundingCone=True, buildOptions=LightBVHBuilderOptions(splitHeuristicSelection=SplitHeuristic.BinnedSAOH, maxTriangleCountPerLeaf=10, binCount=16, volumeEpsilon=0.0010000000474974513, useLeafCreationCost=True, createLeavesASAP=True, useLightingCones=True, splitAlongLargest=False, useVolumeOverSA=False, allowRefitting=True, usePreintegration=True), useLightingCone=True, disableNodeFlux=False, useUniformTriangleSampling=True, solidAngleBoundMethod=SolidAngleBoundMethod.Sphere)})
    g.addPass(MegakernelPathTracer, 'MegakernelPathTracer')
    g.addEdge('GBufferRT.vbuffer', 'MegakernelPathTracer.vbuffer')
    g.addEdge('GBufferRT.posW', 'MegakernelPathTracer.posW')
    g.addEdge('GBufferRT.normW', 'MegakernelPathTracer.normalW')
    g.addEdge('GBufferRT.tangentW', 'MegakernelPathTracer.tangentW')
    g.addEdge('GBufferRT.faceNormalW', 'MegakernelPathTracer.faceNormalW')
    g.addEdge('GBufferRT.viewW', 'MegakernelPathTracer.viewW')
    g.addEdge('GBufferRT.diffuseOpacity', 'MegakernelPathTracer.mtlDiffOpacity')
    g.addEdge('GBufferRT.specRough', 'MegakernelPathTracer.mtlSpecRough')
    g.addEdge('GBufferRT.emissive', 'MegakernelPathTracer.mtlEmissive')
    g.addEdge('GBufferRT.matlExtra', 'MegakernelPathTracer.mtlParams')
    g.addEdge('MegakernelPathTracer.color', 'AccumulatePass.input')
    g.addEdge('AccumulatePass.output', 'ToneMappingPass.src') 
    g.markOutput('ToneMappingPass.dst')
    g.markOutput('AccumulatePass.output')
    return g
m.addGraph(render_graph_PathTracerGraph())
m.frameCapture.outputDir = out_dir


# READ CAMERA POSITIONS
campos = []
xyz = open(cameras_path, "r")
num_cameras = 0
for line in xyz:
    camx, camy, camz = line.split()
    #print('>>> ', camx, camy, camz)
    camx = float(camx)
    camy = float(camy)
    camz = float(camz)
    if blender_camera_scale:
        camx = 100 * camx
        camy = 100 * camy
        camz = 100 * camz
    if translate_camera:
        camx = camx + cam_translation[0]
        camy = camy + cam_translation[1]
        camz = camz + cam_translation[2]
    campos.append([camx, camy, camz])
    num_cameras = num_cameras +1
xyz.close()
random.shuffle(campos)
print(">>>>>>>>>>>>> Found ", num_cameras, " camera positions.")


# LOAD ENVIRONMENT MAPS
envmap_files = []
for r, d, f in os.walk(envmap_dir):  # r=root, d=directories, f = files
    for file in f:
        if '.exr' in file:
            envmap_files.append(os.path.join(r, file))
print(">>>>>>>>>>>>> Found ", num_cameras, " envmaps.")



# LOAD SCENE
m.loadScene(scenepath, buildFlags = SceneBuilderFlags.UseSpecGlossMaterials, info = 0.0)
#m.loadScene(scenepath, buildFlags = SceneBuilderFlags.UseMetalRoughMaterials, info = 0.0)
m.clock.pause()
m.scene.camera.up = float3(0,1,0)
m.scene.camera.focalLength = focal_len # manually matched
# Layers for learning
m.activeGraph.markOutput("GBufferRT.posW")
m.activeGraph.markOutput("GBufferRT.normW")
m.activeGraph.markOutput("GBufferRT.diffuseOpacity")
m.activeGraph.markOutput("GBufferRT.specRough")
m.activeGraph.markOutput("GBufferRT.alpha")
m.activeGraph.markOutput("GBufferRT.specShading")
m.activeGraph.markOutput("GBufferRT.transShading")
m.activeGraph.markOutput("GBufferRT.viewW")

print(">>>>>>>>>>>>> SCENE BOUNDING BOX: ", m.scene.bounds)  #	AABB	World space scene bounds (readonly)) 
# ATELIER BOUNDING BOX:  [[-146.994873, -59.336391, -170.382034], [146.993851, 85.836891, 113.362228]]

 
for i in range(number_output_renders): #range(len(envmap_files)):

    # change camera position
    m.scene.camera.position = float3(float(campos[i % num_cameras][0]), float(campos[i % num_cameras][1]), float(campos[i % num_cameras][2]))
    m.scene.camera.target = float3(camtar[i% len(camtar)][0], camtar[i% len(camtar)][1], camtar[i% len(camtar)][2])
    
    # change envmap
    m.scene.setEnvMap(envmap_files[i % len(envmap_files)])

    # render and accumulate
    start = time.time()
    print("Start rendering")
    for acc in range(accumulator_frames):
        renderFrame()
    end = time.time()
    print("Rendering time:  ", end - start)

    m.frameCapture.baseFilename = f"{i:05d}"
    m.frameCapture.capture()
exit()

# sometimes the specular shading values are very high, sometimes they are -Inf :( why would they even be negative??


# find the Fresnel implementations
# verify the specular shading buffer and write the transmitted one


# TODO before launching generation:

# launch and immediately try to train a little
# read SH lighting gritty details  



# generate camera positions and orientations in Blender
# test with atelier scene
# check if I can shorten the output filenames
# verify that I output all the buffers that Python outputs

# write the script as: per envmap, render a couple of cameras (say 10?). Load those 10 in an inner loop from the CSV that should contain millions of cam positions


# separate diffuse from specular in python
# check that alpha is correct GGX

# generate transparent layer too
# write the bounding box data to a txt file 


# divide position buffer by bounding box

# why is the diffuse opacity of the buddha 1??

# foreground = norm(normal)>0

# can we change the capture name to make it shorter?

# run script once for train and once for test





# Problems: Falcor does not seem to importance sample the envmap
# 30 secs for 500x500 1600spp image (that can be quite noisy still)
# try using more light samples in the renders and compare


# should we completely scramble the data to train on random pixels?
# should we pre-train the encoder?
