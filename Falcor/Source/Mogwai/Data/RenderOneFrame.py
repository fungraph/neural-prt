def render_graph_PathTracerGraph():
    g = RenderGraph("PathTracerGraph")
    loadRenderPassLibrary("AccumulatePass.dll")
    loadRenderPassLibrary("GBuffer.dll")
    loadRenderPassLibrary("ToneMapper.dll")
    loadRenderPassLibrary("MegakernelPathTracer.dll")
    AccumulatePass = createPass("AccumulatePass", {'enableAccumulation': True})
    loadRenderPassLibrary("OptixDenoiser.dll")
    g.addPass(createPass("OptixDenoiser", {}), "Denoiser")
    g.addPass(AccumulatePass, "AccumulatePass")
    ToneMappingPass = createPass("ToneMapper", {'autoExposure': False, 'exposureCompensation': 0.0})
    g.addPass(ToneMappingPass, "ToneMappingPass")
    GBufferRT = createPass("GBufferRT", {'forceCullMode': False, 'cull': CullMode.CullBack, 'samplePattern': SamplePattern.Stratified, 'sampleCount': 16})
    g.addPass(GBufferRT, "GBufferRT")
# ref x #num frames
#    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=1024, maxBounces=7)})
    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=15, maxBounces=7)})
    g.addPass(MegakernelPathTracer, "MegakernelPathTracer")
    g.addEdge("GBufferRT.vbuffer", "MegakernelPathTracer.vbuffer")      # Required by ray footprint.
    g.addEdge("GBufferRT.posW", "MegakernelPathTracer.posW")
    g.addEdge("GBufferRT.normW", "MegakernelPathTracer.normalW")
    g.addEdge("GBufferRT.tangentW", "MegakernelPathTracer.tangentW")
    g.addEdge("GBufferRT.faceNormalW", "MegakernelPathTracer.faceNormalW")
    g.addEdge("GBufferRT.viewW", "MegakernelPathTracer.viewW")
    g.addEdge("GBufferRT.diffuseOpacity", "MegakernelPathTracer.mtlDiffOpacity")
    g.addEdge("GBufferRT.specRough", "MegakernelPathTracer.mtlSpecRough")
    g.addEdge("GBufferRT.emissive", "MegakernelPathTracer.mtlEmissive")
    g.addEdge("GBufferRT.matlExtra", "MegakernelPathTracer.mtlParams")
    g.addEdge("MegakernelPathTracer.color", "AccumulatePass.input")
    g.addEdge("AccumulatePass.output", "ToneMappingPass.src")
    g.addEdge("ToneMappingPass.dst", "Denoiser.color")
    g.addEdge("GBufferRT.diffuseOpacity", "Denoiser.albedo")
    g.addEdge("GBufferRT.normW",  "Denoiser.normal")
    g.markOutput("ToneMappingPass.dst")
    #g.markOutput("Denoiser.output")
    return g

PathTracerGraph = render_graph_PathTracerGraph()
try: m.addGraph(PathTracerGraph)
except NameError: None

#m.loadScene("E:\datasets\Stavros\\veach-ajar-falcor\scene2_falcor_new.pyscene")
m.loadScene("C:/Users/grainer.AD/Desktop/projects/data/mitsuba-falcor-conversion/stavrosscenes/bathroom.pyscene")

m.clock.pause()

renderFrame()
m.frameCapture.outputDir = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/firstruns"
m.frameCapture.baseFilename = f"Bathroom-"
m.frameCapture.capture()

# TODO : first do the cameras, then look at animations for objects

# NOTES ( Gilles)
# updatePass(name, dict)	Update a render pass with new configuration options in dict.
# markOutput(name)	Mark an output to be selectable in Mogwai and writing files when capturing frames.
# Scene
# animated	bool	Enable/disable scene animations.
# cameras	list(Camera)	List of cameras.   -> Load all cameras and then set active camera at each frame
# getMaterial(name)	Return a material by name.
# Or addViewpoint(position, target, up)	Add a viewpoint to the viewpoint list.
#removeViewpoint()	Remove selected viewpoint.
#selectViewpoint(index)	Select a specific viewpoint and move the camera to it.
#addAnimation(animation)	Add an animation.
#createAnimation(animatable, name, duration)	Create an animation for an animatable object. Returns the new animation or None if one already exists.
#



#for i in range (8):
#    renderFrame()
#    print (i)
#    if i == 7:
#        m.frameCapture.baseFilename = f"Veach-{i}"
#        m.frameCapture.capture()

#m.activeGraph.markOutput("Denoiser.output")
#m.frameCapture.baseFilename = f"MyTestDen-"
#m.frameCapture.capture()

exit()
