import csv
import math
import time

def render_graph_PathTracerGraph():
    g = RenderGraph("PathTracerGraph")
#    loadRenderPassLibrary("AccumulatePass.dll")
    loadRenderPassLibrary("GBuffer.dll")
    loadRenderPassLibrary("ToneMapper.dll")
    loadRenderPassLibrary("MegakernelPathTracer.dll")
#    AccumulatePass = createPass("AccumulatePass", {'enableAccumulation': True})
    loadRenderPassLibrary("OptixDenoiser.dll")
    g.addPass(createPass("OptixDenoiser", {}), "Denoiser")
#    g.addPass(AccumulatePass, "AccumulatePass")
    ToneMappingPass = createPass("ToneMapper", {'autoExposure': False, 'exposureCompensation': 0.0})
    g.addPass(ToneMappingPass, "ToneMappingPass")
    GBufferRT = createPass("GBufferRT", {'forceCullMode': False, 'cull': CullMode.CullBack, 'samplePattern': SamplePattern.Stratified, 'sampleCount': 16})
    g.addPass(GBufferRT, "GBufferRT")
# ref x #num frames
#    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=1024, maxBounces=7)})
    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=15, maxBounces=7)})
    g.addPass(MegakernelPathTracer, "MegakernelPathTracer")
    g.addEdge("GBufferRT.vbuffer", "MegakernelPathTracer.vbuffer")      # Required by ray footprint.
    g.addEdge("GBufferRT.posW", "MegakernelPathTracer.posW")
    g.addEdge("GBufferRT.normW", "MegakernelPathTracer.normalW")
    g.addEdge("GBufferRT.tangentW", "MegakernelPathTracer.tangentW")
    g.addEdge("GBufferRT.faceNormalW", "MegakernelPathTracer.faceNormalW")
    g.addEdge("GBufferRT.viewW", "MegakernelPathTracer.viewW")
    g.addEdge("GBufferRT.diffuseOpacity", "MegakernelPathTracer.mtlDiffOpacity")
    g.addEdge("GBufferRT.specRough", "MegakernelPathTracer.mtlSpecRough")
    g.addEdge("GBufferRT.emissive", "MegakernelPathTracer.mtlEmissive")
    g.addEdge("GBufferRT.matlExtra", "MegakernelPathTracer.mtlParams")
    g.addEdge("MegakernelPathTracer.color", "ToneMappingPass.src")
#    g.addEdge("MegakernelPathTracer.color", "AccumulatePass.input")
#    g.addEdge("AccumulatePass.output", "ToneMappingPass.src")
    g.addEdge("ToneMappingPass.dst", "Denoiser.color")
    g.addEdge("GBufferRT.diffuseOpacity", "Denoiser.albedo")
    g.addEdge("GBufferRT.normW",  "Denoiser.normal")
    g.markOutput("ToneMappingPass.dst")
    #g.markOutput("Denoiser.output")  # TODO : Adding the denoiser output creates a segmentation fault during renderFrame() !!!!!!!!
    return g

PathTracerGraph = render_graph_PathTracerGraph()
try: m.addGraph(PathTracerGraph)
except NameError: None

# MODIFY THESE PATHS
scenepath = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/bathroom.pyscene"
m.frameCapture.outputDir = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/BathroomOutput"
csv_path = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/path_bathroom.csv"
csv_path_rough = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/door_roughness.csv"
csv_path_light = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/emitter_position.csv"

campos = []
camtar = []
rough = []
camup = [0,1,0]
m.loadScene(scenepath, buildFlags = SceneBuilderFlags.DontMergeMaterials, info = 0.0) 
renderFrame() # rendering hack to be able to activate the denoiser

with open(csv_path) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        campos.append(row[0:3])
        camtar.append(row[3:6])
        line_count +=1
    print(f'Total of {line_count} camera configurations.')

with open(csv_path_rough) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        rough.append(row[0])
        line_count +=1
    print(f'Total of {line_count} roughness configurations.')

light = []
with open(csv_path_light) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        light.append(row[0])
        line_count +=1
    print(f'Total of {line_count} light positions.')

m.clock.pause()
m.scene.camera.up = float3(camup[0], camup[1], camup[2])

start = time.time()
print("Start rendering")

renderFrame()
end = time.time()
print("Rendering time:  ", end - start) 
m.activeGraph.markOutput("Denoiser.output")

for i in range(line_count):

    print(light[i])
    lightX = float(light[i])
    m.loadScene(scenepath, buildFlags = SceneBuilderFlags.DontMergeMaterials, info = lightX) 

    # change camera position
    m.scene.camera.position = float3(float(campos[i][0]), float(campos[i][1]), float(campos[i][2]))
    m.scene.camera.target = float3(float(camtar[i][0]), float(camtar[i][1]), float(camtar[i][2]))

    m.scene.camera.focalLength = 28 # manually matched

    # # # To change material properties, invoke the following command, only modify the name of the material and the updated values
    roughness = 1.0 - math.sqrt(float(rough[i]))
    m.scene.getMaterial("var_BathroomDoor").specularParams = float4(1,1,1, roughness ) # The 4th value of the specular parameters is the roughness

    start = time.time()
    print("Start rendering")

    renderFrame()
    end = time.time()
    print("Rendering time:  ", end - start)

    m.frameCapture.baseFilename = f"bath-{i:04d}"
    m.frameCapture.capture()


exit()


