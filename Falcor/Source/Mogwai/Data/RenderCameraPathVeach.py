import csv

def render_graph_PathTracerGraph():
    g = RenderGraph("PathTracerGraph")
    loadRenderPassLibrary("AccumulatePass.dll")
    loadRenderPassLibrary("GBuffer.dll")
    loadRenderPassLibrary("ToneMapper.dll")
    loadRenderPassLibrary("MegakernelPathTracer.dll")
    AccumulatePass = createPass("AccumulatePass", {'enableAccumulation': True})
    loadRenderPassLibrary("OptixDenoiser.dll")
    g.addPass(createPass("OptixDenoiser", {}), "Denoiser")
    g.addPass(AccumulatePass, "AccumulatePass")
    ToneMappingPass = createPass("ToneMapper", {'autoExposure': False, 'exposureCompensation': 0.0})
    g.addPass(ToneMappingPass, "ToneMappingPass")
    GBufferRT = createPass("GBufferRT", {'forceCullMode': False, 'cull': CullMode.CullBack, 'samplePattern': SamplePattern.Stratified, 'sampleCount': 16})
    g.addPass(GBufferRT, "GBufferRT")
# ref x #num frames
#    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=1024, maxBounces=7)})
    MegakernelPathTracer = createPass("MegakernelPathTracer", {'mSharedParams': PathTracerParams(useVBuffer=0, samplesPerPixel=15, maxBounces=7)})
    g.addPass(MegakernelPathTracer, "MegakernelPathTracer")
    g.addEdge("GBufferRT.vbuffer", "MegakernelPathTracer.vbuffer")      # Required by ray footprint.
    g.addEdge("GBufferRT.posW", "MegakernelPathTracer.posW")
    g.addEdge("GBufferRT.normW", "MegakernelPathTracer.normalW")
    g.addEdge("GBufferRT.tangentW", "MegakernelPathTracer.tangentW")
    g.addEdge("GBufferRT.faceNormalW", "MegakernelPathTracer.faceNormalW")
    g.addEdge("GBufferRT.viewW", "MegakernelPathTracer.viewW")
    g.addEdge("GBufferRT.diffuseOpacity", "MegakernelPathTracer.mtlDiffOpacity")
    g.addEdge("GBufferRT.specRough", "MegakernelPathTracer.mtlSpecRough")
    g.addEdge("GBufferRT.emissive", "MegakernelPathTracer.mtlEmissive")
    g.addEdge("GBufferRT.matlExtra", "MegakernelPathTracer.mtlParams")
    g.addEdge("MegakernelPathTracer.color", "AccumulatePass.input")
    g.addEdge("AccumulatePass.output", "ToneMappingPass.src")
    g.addEdge("ToneMappingPass.dst", "Denoiser.color")
    g.addEdge("GBufferRT.diffuseOpacity", "Denoiser.albedo")
    g.addEdge("GBufferRT.normW",  "Denoiser.normal")
    g.markOutput("ToneMappingPass.dst")
    #g.markOutput("Denoiser.output")  # TODO : Adding the denoiser output creates a segmentation fault during renderFrame() !!!!!!!!
    return g

PathTracerGraph = render_graph_PathTracerGraph()
try: m.addGraph(PathTracerGraph)
except NameError: None

# MODIFY THESE PATHS
scenepath = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/veach-ajar.pyscene"
m.frameCapture.outputDir = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/veachrotate"
csv_path = "C:/Users/grainer.AD/Desktop/projects/renderers/Falcor/stavrostests/veach_path.csv"


campos = []
camtar = []
camup = [0,1,0]



with open(csv_path) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        campos.append(row[0:3])
        camtar.append(row[3:6])
        line_count +=1
    print(f'Total of {line_count} camera configurations.')

m.clock.pause()

m.loadScene(scenepath, buildFlags = SceneBuilderFlags.DontMergeMaterials, info = 0.0) 
renderFrame() # rendering hack to be able to activate the denoiser
m.frameCapture.baseFilename = f"veach-anim-{0:04d}"
m.frameCapture.capture()

m.activeGraph.markOutput("Denoiser.output")

for i in range(line_count):

    # for door the rotation is from -15 to 80 around the y axis
    rotation_angle_deg = float( 15.0 + (-80.0) * i / line_count )

    m.loadScene(scenepath, buildFlags = SceneBuilderFlags.DontMergeMaterials, info =  rotation_angle_deg) 

    # change camera position
    m.scene.camera.position = float3(float(campos[i][0]), float(campos[i][1]), float(campos[i][2]))
    m.scene.camera.target = float3(float(camtar[i][0]), float(camtar[i][1]), float(camtar[i][2]))
    m.scene.camera.up = float3(camup[0], camup[1], camup[2])

    #m.scene.camera.focalLength = 50 #Mitsuba default
    m.scene.camera.focalLength = 28 # Approximate match for 75deg FOV for 35mm lens 

    # # # To change material properties, invoke the following command, only modify the name of the material and the updated values
    # m.scene.getMaterial("Wood").specularParams = float4(1,1,1,i/10) # The 4th value of the specular parameters is the roughness

    renderFrame()
    m.frameCapture.baseFilename = f"veach-anim-{i:04d}"
    m.frameCapture.capture()


exit()
