# Disclaimer: This file contains many small helper functions that were used in different phases of the project, only a small fraction are used in the final results

import os
import numpy as np
import numpy as np
import cv2
import torch
import torchvision
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import matplotlib.pyplot as plt
from collections import OrderedDict
import nets


def rmse(im1, im2):
    return torch.sqrt( torch.mean( (im1 - im2)**2 ) )
def mape(im1, im2): # prediction, GT
    return 100.0 * torch.mean(torch.abs((im1 - im2) / (im2 + 0.00001)))

def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, net, num_freqs, withnormals, numSGs, alternate_parametrization, sg_centers, exponentiate,
                        timings=False, encode_normals=False, new_complex_sgnet=False, anisotropic=False, iteration=100000, withroughness=False):

    mask = (alpha > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    # flatten everything
    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)

    # isolate pixels that need shading
    mask = (alpha > 0.0)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]
    netin = pos3d
    if num_freqs > 0:
        netin = nets.positional_encoding(pos3d, num_encoding_functions=num_freqs)

        # Progressive freq
        maxf = min(int((iteration - iteration % 500) / 500), num_freqs)

        netin[int(maxf * 6) : int((maxf+1) * 6)] = (iteration % 500)/500 * netin[int(maxf * 2 * 3) : int((maxf+1) * 2 * 3)]
        # % set all other frequs to 0
        netin[int((maxf+1) * 6) : -1] = 0.0 * netin[int((maxf+1) * 6) : -1]


    if withnormals:
        nn = normals
        if encode_normals and num_freqs > 0:
            nn = nets.positional_encoding(normals, num_encoding_functions=3)
        netin = torch.cat((netin, nn), dim=1)

    if withroughness:
        netin = torch.cat((netin, alpha.unsqueeze(1)), dim=1)


    # print('netin ', netin.shape)
    assert (envmap != envmap).any() == False, "Damn! envmap contains NaNs"

    if timings:
        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)
    envinp = envmap.unsqueeze(0).to(device=gpudevice)
    posinp = netin.to(device=gpudevice)

    if timings:
        start.record()

    if new_complex_sgnet:
        # self.lightSGs = ImageEncoder(cnndim, 6 * numLightSGs)
        #
        # # Interreflections SG generator
        # # self.sceneSGs = VisibilityNet(numSceneSGs * 6, inpsize, width, num_layers)
        # self.sceneSGs = CompleteNet(cnndim, 72, numSceneSGs, inpsize, width, num_layers)
        #
        # # SG multiplier prediction
        # self.SGmultipliers = VisibilityNet(numSceneSGs + numLightSGs, inpsize + 3, width, num_layers)
        #
        # self.numLightSGs = numLightSGs
        # self.numSceneSGs = numSceneSGs
        # self.width = width
        # self.num_layers = num_layers
        # self.activate = nn.ReLU()  # nn.LeakyReLU()#
        #
        # self.skipconnections = False

        if alternate_parametrization:
            # net_out  = net.lightSGs(envinp).view(net.numLightSGs, 7)
            alternate_parametrization = True
            # net_out = net(envinp, posinp)

        else:
            # nout = net.lightSGs(envinp).view(net.numLightSGs, 3)
            # # rgbmean = (torch.sum(nout[:, 0:3] * nout[:, 0:3], dim=1)).mean()
            # # filteredlights = []
            # # sgidxs = []
            # # for s in range(numSGs):
            # #     if nout[s, 0] * nout[s, 0] + nout[s, 1] * nout[s, 1] + nout[s, 2] * nout[s, 2] >= rgbmean:
            # #         filteredlights.append(nout[s:s + 1, :])
            # #         sgidxs.append(s)
            # # net_out = torch.cat(filteredlights, dim=0).unsqueeze(0).expand(posinp.shape[0], -1, -1)
            # net_out = torch.cat( (nout, torch.zeros(net.numLightSGs,4).cuda()), dim=1)


            # sg_centers = sg_centers[sgidxs,:]

            net_out = net(envinp, posinp)

            if timings:
                print(" filtered sgs ", net_out.shape[1])
            # scenesgs = net.sceneSGs(envmap, posinp).unsqueeze(2).view(-1, net.numLightSGs, 6)



        # v = wi.to(device=gpudevice)
        # # print("envinp.shape, posinp.shape, v.shape", envinp.shape, posinp.shape, v.shape)
        # net_out = net(envinp, posinp, v)
    else:
        net_out = net(envinp, posinp)



    # Undo the normalization of light intensities
    # if alternate_parametrization:
    #     net_out = net_out.unsqueeze(2).view(-1, numSGs, 7)
    # else:
    #     net_out = net_out.unsqueeze(2).view(-1, numSGs, 6)

    # assert (net_out != net_out).any() == False, "Damn! net_out contains NaNs"
    envmaploss = 0.0
    lightmap = []
    if alternate_parametrization:
        if new_complex_sgnet:
            lightsgs, factors = net(envinp, posinp)
            sglights0 = deparametrize_2(lightsgs)
            sglights = torch.cat((sglights0[:, :, 0:3] * factors, sglights0[:, :, 3:7]), dim=2)

            # sglights0 = deparametrize_2(net_out.unsqueeze(0).expand(posinp.shape[0], -1, -1))
            # lightmap = sglights0[0]
            #
            # # sgmult = net.SGmultipliers(posinp).unsqueeze(2).expand(-1, -1, 3)
            # sgmult = net.SGmultipliers(net_out[:, 3:7].reshape(-1), posinp).unsqueeze(2).expand(-1, -1, 3)
            # sglights = torch.cat((sglights0[:, :, 0:3] * torch.sigmoid(sgmult), sglights0[:, :, 3:7]), dim=2)
            #
            # if timings:
            #     print("multipliers minmax ", torch.sigmoid(sgmult).min(), torch.sigmoid(sgmult).mean(), torch.sigmoid(sgmult).max())
            #
            # posmlp = net.SGmultipliers(posinp)
            # # vizrgb = torch.exp(posmlp[:, 0:3])
            # viz = posmlp[:, 0:3]
            # vizsharp = torch.exp(posmlp[:, 3])
            # vizdir = viz / torch.clamp(torch.sqrt(torch.sum(viz * viz, dim=1, keepdim=True)), min=1e-6).expand(-1, 3)
            # sglights = []
            # for i in range(numSGs):
            #     # for j in range(numSGs):
            #     #     # evaluate visibility in SG direction for all SG outputs of positional net
            #     cosangle = (sglights0[:, i, 4] * vizdir[:, 0] + sglights0[:, i, 5] * vizdir[:, 1]
            #                 + sglights0[:, i, 6] * vizdir[:, 2]) * torch.sigmoid(posmlp[:, 4])
            #
            #     sgmult = (torch.exp(vizsharp * (cosangle - 1.0))).unsqueeze(1).expand(-1, 3)
            #     sglights.append(torch.cat((sglights0[:, i, 0:3] * sgmult, sglights0[:, i, 3:7]), dim=1).unsqueeze(1))
            # sglights = torch.cat(sglights, dim=1)
            #
            # if timings:
            #     print('posnet: minmax RGB prediction: ', torch.sigmoid(posmlp[:, 4]).min(), torch.sigmoid(posmlp[:, 4]).mean(), torch.sigmoid(posmlp[:, 4]).max())
            #     print('posnet: minmax sharpness: ', vizsharp.min(), vizsharp.mean(), vizsharp.max())

            # net_out = net_out.unsqueeze(0).expand(posinp.shape[0], -1, -1)
        else:
            # sglights = deparametrize_2(net_out)
            sglights = deparametrize_2(net_out.unsqueeze(2).view(-1,numSGs, 7))

    else:
        # print("problem sizes: ", net_out.shape, sg_centers.shape)
        if new_complex_sgnet:
            sglights = deparametrize_torch(net_out, sg_centers, exponentiate)
            lightmap = sglights[0]

            # sglights0 = deparametrize_torch(net_out, sg_centers, exponentiate)
            #
            # sgmult = net.SGmultipliers(posinp).unsqueeze(2).expand(-1, -1, 3)
            # sglights = torch.cat((sglights0[:, :, 0:3] * torch.sigmoid(sgmult), sglights0[:, :, 3:7]), dim=2)
            #
            # lightmap = sglights0[0]
            # # TODO : new idea. Predict visibility as SG or simply as a direction+sharpness
            # posmlp = net.SGmultipliers(posinp)
            # # vizrgb = torch.exp(posmlp[:,0:3])
            # viz = posmlp[:, 0:3]
            # vizsharp = torch.exp(posmlp[:, 3])
            # vizdir = viz / torch.clamp(torch.sqrt(torch.sum(viz*viz,dim=1,keepdim=True)), min=1e-6).expand(-1,3)
            # sglights = []
            # for i in range(numSGs):
            #
            #     # for j in range(numSGs):
            #     #     # evaluate visibility in SG direction for all SG outputs of positional net
            #     cosangle = sglights0[:,i,4] * vizdir[:,0] + sglights0[:,i,5] * vizdir[:,1] + sglights0[:,i,6] * vizdir[:,2]
            #     sgmult = (torch.exp(vizsharp * (cosangle - 1.0))).unsqueeze(1).expand(-1,3)
            #     sglights.append(torch.cat((sglights0[:, i, 0:3] * sgmult, sglights0[:, i, 3:7]), dim=1).unsqueeze(1))
            # sglights = torch.cat(sglights, dim=1)

        else:
            sglights = deparametrize_torch(net_out.unsqueeze(2).view(-1,numSGs, 6), sg_centers, exponentiate)

    if alternate_parametrization:
        # sglights[:, :, 0:3] = sglights[:, :, 0:3] / (numSGs * numSGs)
        # sglights[:, :, 3] = sglights[:, :, 3] + numSGs / np.pi # * numSGs + np.pi # * numSGs
        sglights[:, :, 0:3] = sglights[:, :, 0:3] / (2.0 * np.pi)
        sglights[:, :, 3] = sglights[:, :, 3] * numSGs / np.pi
    else:
        # sglights[:, :, 0:3] = sglights[:, :, 0:3] / (numSGs * numSGs)
        # sglights[:, :, 3] = sglights[:, :, 3] * numSGs / np.pi # * numSGs / np.pi

        sglights[:, :, 3] = sglights[:, :, 3] * numSGs / np.pi # + numSGs / np.pi #


    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))

    if timings:
        print('minmax RGB prediction: ', sglights[:, :, 0:3].min(), sglights[:, :, 0:3].max())
        print('minmax sharpness: ', sglights[:, :, 3].min(), sglights[:, :, 3].max())
        # print('minmax thetaphi: ', net_out[:, :, 3:5].min(), net_out[:, :, 3:5].max())

    assert (sglights[:, :, 0:3] != sglights[:, :, 0:3]).any() == False, "Damn! Amplitudes contains NaNs after denormalizing"
    assert (sglights[:, :, 3] != sglights[:, :, 3]).any() == False, "Damn! Sharpness contains NaNs after denormalizing"

    envmaploss = 0# computeEnvmapLoss(envmap.permute(1, 2, 0), lightmap, gpudevice)

    # diffuse shading
    mask_diffuse = (torch.sum(diffuse_bsdf_val * diffuse_bsdf_val, dim=-1, keepdim=False) > 0.0)
    irr = torch.zeros(torch.sum(mask_diffuse), 3).to(device=gpudevice)
    for l in range(sglights.shape[1]):
        irr = irr + exact_cosSGinnerProduct(normals[mask_diffuse, :].to(device=gpudevice),
                                                sglights[mask_diffuse, l, 0:3],
                                                sglights[mask_diffuse, l, 3],
                                                sglights[mask_diffuse, l, 4:7])
    pred[mask_diffuse, :] = pred[mask_diffuse, :] + irr * diffuse_bsdf_val[mask_diffuse, :].to(device=gpudevice)
    assert (irr != irr).any() == False, "Damn! irr contains NaNs"

    # specular shading
    mask_specular = (torch.sum(specular_bsdf_val * specular_bsdf_val, dim=-1, keepdim=False) > 0.0)
    r2 = alpha[mask_specular] ** 2
    ndf_amp = 1.0 / (r2 * np.pi)
    ndf_amp = ndf_amp.unsqueeze(1).expand(-1, 3)
    ndf_sharp = 2.0 / r2
    ndf_sharp = ndf_sharp.unsqueeze(1).expand(-1, 3)
    ndf_val = torch.zeros(ndf_sharp.shape, device=gpudevice)
    if anisotropic:
        amp, warpX, warpY, warpZ, sharpX, sharpY = warpBRDF_ASG(ndf_amp.to(gpudevice),
                                                                    ndf_sharp.to(gpudevice),
                                                                    normals[mask_specular, :].to(gpudevice),
                                                                    wi[mask_specular, :].to(gpudevice))

        for l in range(sglights.shape[1]):
            ndf_val = ndf_val + convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY,
                                                   sglights[mask_specular, l, 0:3],
                                                   sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                                   sglights[mask_specular, l, 4:7])
    else:
        sg_amp, sg_sharp, sg_axi = warpBRDF_SG(ndf_amp.to(gpudevice), ndf_sharp.to(gpudevice), normals[mask_specular, :].to(gpudevice),
                                                                    wi[mask_specular, :].to(gpudevice))
        for l in range(sglights.size()[1]):
            ndf_val = ndf_val + SGinnerProduct(sglights[mask_specular, l, 0:3], sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                               sglights[mask_specular, l, 4:7], sg_amp, sg_sharp, sg_axi)

    assert (ndf_val != ndf_val).any() == False, "Damn! ndf_val contains NaNs"
    pred[mask_specular, :] = pred[mask_specular, :] + torch.nn.functional.relu(
        ndf_val * specular_bsdf_val[mask_specular, :].to(gpudevice))

    return pred, gtcpu.view(-1, 3)[mask, :], sglights[:, :, 3], envmaploss  #alpha.unsqueeze(1).expand(-1,3)


def computeEnvmapLoss(envmap, SGs, gpudevice):
    xyzgrid = create_envmap_grid(envmap.shape[0], envmap.shape[1]).to(device=gpudevice)
    # val = torch.zeros(xyzgrid.size()).to(device=gpudevice)
    # mask = torch.zeros(envmap.shape).to(device=gpudevice)
    mask, val = [], []

    for l in range(SGs.size()[0]):
        cosangle = (xyzgrid[:, :, 0] * SGs[l, 4] + xyzgrid[:, :, 1] * SGs[l, 5] + xyzgrid[:, :, 2] * SGs[
            l, 6]).unsqueeze(2).expand(xyzgrid.size())
        col = SGs[l, 0:3].unsqueeze(0).unsqueeze(0).expand(xyzgrid.size())
        # mask += torch.exp(SGs[l, 3] * (cosangle - 1.0))
        # val += col * mask

        m = torch.exp( (SGs[l, 3]+ SGs.shape[0] / np.pi) * (cosangle - 1.0))
        # mask.append( m * 2*np.pi / (SGs[l, 3]+ SGs.shape[0] / np.pi) * torch.sqrt(1.0- xyzgrid[:, :, 1:2]*xyzgrid[:, :, 1:2]) ) # cosine weighted by elevation

        mask.append( m ) #/ torch.sum(m) ) #
        val.append( col/ (SGs.shape[0] * SGs.shape[0]) * m )

    mask_ = torch.sum(torch.stack(mask), dim=0) / torch.mean(torch.stack(mask)) # / torch.sum(torch.stack(mask))

    val_ = torch.sum(torch.stack(val), dim=0)
    envgpu = envmap.to(device=gpudevice)
    # envgpu = (torch.exp(envgpu.squeeze()) -1.0)

    # Losses
    # envloss = (torch.abs( torch.exp(envgpu) -1.0 - val_ )) * mask_ / torch.clamp(torch.exp(envgpu) -1.0, min=1e-4)

    # TODO Try another method: Simply only evaluate the loss where there are SGs. No cosine term
    # If I put an SG here, it better be correct. Where I dont put anything, doesnt matter. Implicitly handles distortion in sampling
    # envloss = ( (torch.exp(envgpu) -1.0 - val_) **2 ) * val_ / torch.sum(val_)

    # envgpu = (torch.exp(envgpu) -1.0) * mask_ / torch.sum(mask_)
    # envloss = ( ( envgpu/envgpu.mean() - val_/envgpu.mean() * mask_ / torch.sum(mask_) )**2 ) / (envgpu +1.0)

    # envgpu = envgpu * mask_
    envloss = torch.abs( envgpu/envgpu.mean() - torch.log(1.0+val_) /envgpu.mean()  ) * mask_ #/ torch.exp(envgpu)
    # envloss = (torch.abs(envgpu - torch.log(1.0+val_) )) * val_ / torch.sum(val_)


    # envloss = (torch.abs(envgpu - torch.log(1.0+val_) )) * mask_ # * torch.exp(envgpu)

    # print("val_ ", val_.min(), val_.mean(), val_.max())
    # print("envgpu ", envgpu.min(), envgpu.mean(), envgpu.max())
    # print("envloss ", envloss.min(), envloss.mean(), envloss.max())
    return envloss.mean()


def render_gt_and_sg(scene_gt, randomsensor, campos, net, envmap, envmap_m, envmap_std,
                     num_freqs, numSGs, withnormals, spp,
                     sg_centers, exponentiate, imsize, gpudevice, test=False, alternate_parametrization=False, writer=None, iteration=None, filepath =None):

    try:
        scene_gt.integrator().render(scene_gt, randomsensor)
    except RuntimeError:
        print('We had a RunTime error Cuda')
        ek.cuda_malloc_trim()
        scene_gt = load_file(filepath)
        randomsensor, campos = generate_completely_random_sensor(spp, imsize, imsize)
        scene_gt.integrator().render(scene_gt, randomsensor)



    # GENERATE THE BUFFERS
    components = randomsensor.film().bitmap(raw=False).split()
    for i in range(len(components)):
        if 'position' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
            pos3d = torch.from_numpy(buffer.data_np())
        elif 'normals' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
            normals = torch.from_numpy(buffer.data_np())
        # elif 'bsdf' in components[i][0]:
        #     buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
        #     bsdf_val = torch.from_numpy(buffer.data_np())
        elif 'diffuse' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
            diffuse_bsdf_val = torch.from_numpy(buffer.data_np())
        elif 'specular' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.XYZ, Struct.Type.Float32, srgb_gamma=False)
            specular_bsdf_val = torch.from_numpy(buffer.data_np())
        elif 'gt' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)
            gtcpu = torch.from_numpy(buffer.data_np())
        elif 'alpha' in components[i][0]:
            buffer = components[i][1].convert(Bitmap.PixelFormat.Y, Struct.Type.Float32, srgb_gamma=False)
            alpha = torch.from_numpy(buffer.data_np())
            print('min alpha: ', alpha.min())


    # normalize position (for living-room) between -1,1
    pos3d[:,:,0] = 2.0 * pos3d[:,:,0]  / 5.122005  - 1.0
    pos3d[:,:,1] = 2.0 * pos3d[:,:,1]  / 2.879001  - 1.0
    pos3d[:,:,2] = 2.0 * pos3d[:,:,2]  / 3.632799  - 1.0


    assert (pos3d != pos3d).any() == False, "Damn! pos3d contains NaNs"
    assert (normals != normals).any() == False, "Damn! normals contains NaNs"
    assert (diffuse_bsdf_val != diffuse_bsdf_val).any() == False, "Damn! bsdf_val contains NaNs"
    assert (specular_bsdf_val != specular_bsdf_val).any() == False, "Damn! bsdf_val contains NaNs"
    assert (gtcpu != gtcpu).any() == False, "Damn! gtcpu contains NaNs"
    assert (alpha != alpha).any() == False, "Damn! alpha contains NaNs"

    # plt.imshow(alpha)
    # plt.show()
    # plt.imshow(bsdf_val)
    # plt.show()
    mask = (alpha > 0.0).expand(-1,-1,3)
    # to make sure, remove background
    gtcpu[~mask] = 0.0

    # flatten everything
    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha.view(-1)

    # isolate pixels that need shading
    mask = (alpha > 0.0)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    alpha = alpha[mask]
    netin = pos3d
    if num_freqs > 0:
        netin = nets.positional_encoding(pos3d, num_encoding_functions=num_freqs)
    if withnormals:
        netin = torch.cat((netin, normals), dim=1)

        # print('netin ', netin.shape)
    assert (envmap != envmap).any() == False, "Damn! envmap contains NaNs"
    net_out = net(envmap.unsqueeze(0).to(device=gpudevice), netin.to(device=gpudevice))

    # Undo the normalization of light intensities
    if alternate_parametrization:
        net_out = net_out.unsqueeze(2).view(-1, numSGs, 7)
    else:
        net_out = net_out.unsqueeze(2).view(-1, numSGs, 6)

    assert (net_out != net_out).any() == False, "Damn! net_out contains NaNs"

    net_out[:, :, 0:3] = net_out[:, :, 0:3] * envmap_std.to(device=gpudevice) + envmap_m.to(device=gpudevice)

    if alternate_parametrization:
        sglights = deparametrize_2(net_out)
    else:
        sglights = deparametrize_torch(net_out, sg_centers, exponentiate)
    # sglights[:, :, 0:3] = torch.exp(sglights[:, :, 0:3] * envmap_std.to(device=gpudevice) + envmap_m.to(device=gpudevice))

    # sglights[:, :, 0:3] = torch.exp(sglights[:, :, 0:3] * envmap_std.to(device=gpudevice) + envmap_m.to(device=gpudevice))
    # sglights[:, :, 3] = torch.exp(sglights[:, :, 3]) * numSGs

    # sglights[:, :, 0:3] = sglights[:, :, 0:3] * envmap_std.to(device=gpudevice) + envmap_m.to(device=gpudevice)

    if alternate_parametrization:
        sglights[:, :, 0:3] = sglights[:, :, 0:3] / numSGs / np.pi
        sglights[:, :, 3] = sglights[:, :, 3] * numSGs / np.pi / 4.0
    else:
        sglights[:, :, 0:3] = sglights[:, :, 0:3] / (numSGs * numSGs)
        sglights[:, :, 3] = sglights[:, :, 3] * numSGs * np.pi
    # sglights[:, :, 0:4] = sglights[:, :, 0:4] - 1.0

    print('minmax RGB prediction: ', sglights[:, :, 0:3].min(), sglights[:, :, 0:3].max())
    print('minmax sharpness: ', sglights[:, :, 3].min(), sglights[:, :, 3].max())
    print('minmax thetaphi: ', net_out[:, :, 3:4].min(), net_out[:, :, 3:4].max())
    assert (sglights != sglights).any() == False, "Damn! net_out contains NaNs after denormalizing"

    # diffuse shading
    mask_diffuse = (torch.sum(diffuse_bsdf_val * diffuse_bsdf_val, dim=-1, keepdim=False) > 0.0)
    irr = torch.zeros(torch.sum(mask_diffuse), 3).to(device=gpudevice)
    for l in range(numSGs):
        irr = irr + exact_cosSGinnerProduct(normals[mask_diffuse, :].to(device=gpudevice),
                                                sglights[mask_diffuse, l, 0:3], sglights[mask_diffuse, l, 3],
                                                sglights[mask_diffuse, l, 4:7])
    pred[mask_diffuse, :] = pred[mask_diffuse, :] + irr * diffuse_bsdf_val[mask_diffuse, :].to(device=gpudevice)
    assert (irr != irr).any() == False, "Damn! irr contains NaNs"

    # specular shading
    mask_specular = (torch.sum(specular_bsdf_val * specular_bsdf_val, dim=-1, keepdim=False) > 0.0)
    wi = normalize(campos.unsqueeze(0).expand(torch.sum(mask_specular), -1) - pos3d[mask_specular, :])
    r2 = alpha[mask_specular] ** 2
    ndf_amp = 1.0 / (r2 * np.pi)
    ndf_amp = ndf_amp.unsqueeze(1).expand(-1, 3)
    ndf_sharp = 2.0 / r2
    ndf_sharp = ndf_sharp.unsqueeze(1).expand(-1, 3)
    amp, warpX, warpY, warpZ, sharpX, sharpY = warpBRDF_ASG(ndf_amp.to(gpudevice), ndf_sharp.to(gpudevice),
                                                                normals[mask_specular, :].to(gpudevice),
                                                                wi.to(gpudevice))
    ndf_val = torch.zeros(wi.shape, device=gpudevice)
    for l in range(numSGs):
        ndf_val = ndf_val + convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY,
                                               sglights[mask_specular, l, 0:3],
                                               sglights[mask_specular, l, 3].unsqueeze(1).expand(-1, 3),
                                               sglights[mask_specular, l, 4:7])
    assert (ndf_val != ndf_val).any() == False, "Damn! ndf_val contains NaNs"
    pred[mask_specular, :] = pred[mask_specular, :] + torch.nn.functional.relu(
        ndf_val * specular_bsdf_val[mask_specular, :].to(gpudevice))


    if test:
        env = (torch.exp(envmap.squeeze() * envmap_std + envmap_m))
        light = sglights[int(sglights.shape[0] / 2), :, :].cpu().detach()
        env2 = displaySGEnvmap(light).permute(2, 0, 1)
        print('pred envmap max ', env2.max())
        img_grid2 = torchvision.utils.make_grid([0.5* env / env.mean(), 0.5* env2 / env.mean()])

        # display the static BSDF
        static = torch.zeros(mask.shape[0], 3)
        static[mask.cpu(),:] = static[mask.cpu(),:] + diffuse_bsdf_val.cpu() + specular_bsdf_val.cpu()
        static = static.unsqueeze(0).view(gtcpu.shape)
        print('debug ', static.shape, gtcpu.shape)
        img_grid = torchvision.utils.make_grid([static.permute(2, 0, 1), gtcpu.cpu().permute(2, 0, 1)])
        img_grid = img_grid / static.max()
        img_grid = img_grid ** (1.0 / 2.2)
        writer.add_image('static BSDF', img_grid, global_step=iteration)

        pr = torch.zeros(gtcpu.size())
        pr = pr.view(-1,3)
        pr[mask,:] = pred.cpu()
        return gtcpu, pr.view(gtcpu.shape), img_grid2
    else:
        return gtcpu.view(-1,3)[mask,:], pred

class EnvmapDataset(Dataset):
    def __init__(self, filenames, augment=True, transform=None, max=False):
        self.transform = transform
        self.files = filenames
        self.images = []
        self.means = []
        self.stds = []
        self.width = 0
        self.augment = augment
        self.maxmin = max

        self.minvalue = 0.00001
        c = 0
        thresh = torch.nn.Threshold(self.minvalue, self.minvalue, inplace=True)

        mmin = 1.0
        mmax = 0.0

        for f in self.files:
            if c%50 == 0:
                print('Read ', c, ' EXR files.')
            im = torch.clip(torch.from_numpy(
                cv2.cvtColor(cv2.imread(f, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                             cv2.COLOR_RGB2BGR)).permute(2, 0, 1), 0.0)
            if self.transform:
                im = self.transform(im)
            # if logtransf:

            im = im / im.mean() # (torch.sum(im) / im.numel())

            mmin = np.minimum(mmin, im.min().item())
            mmax = np.maximum(mmax, im.max().item())

            im = torch.log(thresh(im))
            im_m = 0
            im_std = 1

            if self.maxmin:
                im_m = im.min()
                im_std = im.max() - im_m
                im = (im - im_m) / im_std

            # im = torch.log(1.0 + im) #/im.max())
            # im = torch.log(thresh(im))
            # im_m = im.mean()# -torch.log(torch.tensor(0.000001))  #0#
            # im_std = im.std()# im.max() - im_m #  1.0# torch.sum(im) / torch.numel(im) #im.max()# im.std()
            # print(im.min(), im.mean(), im.max())

            # im = (im - im_m) / im_std
            # im = 2* im - 1.0

            # mmin = np.minimum(mmin, im.min().item())
            # mmax = np.maximum(mmax, im.max().item())

            self.images.append(im)
            self.means.append(im_m)
            self.stds.append(im_std)
            c += 1
            self.width = im.shape[2]
        print('minmax of envmaps, ', mmin, mmax)



    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        # print(idx)
        if self.augment:
            rota = int(self.width * np.random.rand(1))
            rgbperm = torch.randperm(3)
            t = self.images[idx]
            ims = torch.zeros(t.shape)
            ims[:,:, 0:self.width-rota] = t[rgbperm,:,rota:self.width]
            ims[:,:, self.width-rota:self.width] = t[rgbperm,:, 0:rota]

            return ims, self.means[idx], self.stds[idx]
        else:
            return self.images[idx], self.means[idx], self.stds[idx]
# 
# class EnvmapDatasetCNN(Dataset):
#     def __init__(self, filenames, logtransf=False, transform=None):
#         self.transform = transform
#         self.files = filenames
#         self.images = []
#         self.means = []
#         self.stds = []
#         c = 0
#         for f in self.files:
#             if c%50 == 0:
#                 print('Read ', c, ' EXR files.')
#             im = torch.clip(torch.from_numpy(
#                 cv2.cvtColor(cv2.imread(f, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
#                              cv2.COLOR_RGB2BGR)).permute(2, 0, 1), 0.0)
#             if self.transform:
#                 im = self.transform(im)
#             if logtransf:
#                 im = torch.log(1.0 + im)
#             im_m = im.mean()
#             im_std = im.std()
#             im = (im - im_m) / im_std
#             self.images.append(im)
#             self.means.append(im_m)
#             self.stds.append(im_std)
#             c += 1
# 
#     def __len__(self):
#         return len(self.files)
# 
#     def __getitem__(self, idx):
#         return self.images[idx], self.means[idx], self.stds[idx]

def my_loss(t1, t2, tonemap=False, clip=False, log=True, L2=False, weights=None):
    if tonemap:
        print('decide which tonemap')
    if clip:
        print('decide which clipping')

    if log:
        t1 = torch.log(1.0 + t1)
        t2 = torch.log(1.0 + t2)

    # m = torch.clamp(torch.abs(t1.detach()) + torch.abs(t2.detach()), min=0.000001)

    if L2:
        # l = torch.abs(t1 / t2 - 1.0)
        l = (t1 - t2) ** 2
        # l = ((t1 - t2) / (0.5 * m))**2
    else:
        l = torch.abs(t1 - t2)
        # l = torch.abs(t1 - t2) / (0.5 * m)

        # print('loss stats:  ', t1.min(), t1.mean(), t1.max(), t2.min(), t2.mean(), t2.max(), l.min(), l.mean(), l.max())

    if weights is not None:
        l = l * weights
    return l.mean()

# def render_SGs(numSGs, sglights, glossymask, normals, gpudevice):
#     # mask, pos3d, normals, bsdf_val = get_buffers(scene_train, randomsensor)
#     irr = torch.zeros(normals.shape[0], 3).to(device=gpudevice)
#     for l in range(numSGs):
#         irr = irr + exact_cosSGinnerProduct(normals, sglights[:, l, 0:3], sglights[:, l, 3],
#                                             sglights[:, l, 4:7])
#     pred_render = bsdf_val * irr
    # DEBUG FOR NaNs ===================================================================================================
    # assert (sglights != sglights).any() == False, "Damn! sglights contains NaNs"
    # assert (pos3d != pos3d).any() == False, "Damn! pos3d contains NaNs"
    # assert (irr != irr).any() == False, "Damn! ndf_val contains NaNs"
#     return pred_render


def write_intermediate_results(iteration, writer, scene_train, scene_gt, randomsensor, net, randomdir, withlight,
                               numSGs, sg_centers, groundtruthshape, withnormals, gpudevice, exponentiate, tensorboard_tag='predictions vs. GT', glossy=False):
    scene_gt.integrator().render(scene_gt, randomsensor)
    gtcpu = torch.from_numpy(np.array(
        randomsensor.film().bitmap(raw=True).convert(Bitmap.PixelFormat.RGB, Struct.Type.Float32, srgb_gamma=False)))
    groundtruthshape = gtcpu.shape

    if glossy:
        mask, pos3d, normals, bsdf_val, ndf_amp, ndf_sharp, wi = get_buffers_glossy(scene_train, randomsensor)
    else:
        mask, pos3d, normals, bsdf_val = get_buffers(scene_train, randomsensor)

    if glossy:
        lightdir = torch.from_numpy(randomdir).unsqueeze(0).expand(pos3d.shape[0], -1).to(device=gpudevice)
    else:
        lightdir = torch.from_numpy(-randomdir).unsqueeze(0).expand(pos3d.shape[0], -1).to(device=gpudevice)
    if withlight:
        pos3d = torch.cat((lightdir, pos3d), dim=1)
    if withnormals:
        pos3d = torch.cat((pos3d, normals), dim=1)
    net_out = net(pos3d)
    sglights = deparametrize_torch(net_out.unsqueeze(2).view(-1, numSGs, 6), sg_centers, exponentiate)

    if glossy:
        irr = shadeSGs(sglights, normals, wi, ndf_amp, ndf_sharp, hybridanis=False, anisowarp=True, sharpness_thresh=5)
    else:
        irr = torch.zeros(pos3d.shape[0], 3).to(device=gpudevice)
        for l in range(numSGs):
            irr = irr + exact_cosSGinnerProduct(normals, sglights[:, l, 0:3], sglights[:, l, 3],
                                                    sglights[:, l, 4:7])
    pred_render = bsdf_val * irr
    pred = torch.zeros(groundtruthshape[0] * groundtruthshape[1], 3)
    gtcpu = gtcpu.permute(2, 0, 1)
    pred[mask.cpu(), :] = pred_render.cpu()
    pred = pred.view(groundtruthshape)
    pred = pred.permute(2, 0, 1)
    diff = torch.abs(pred - gtcpu)

    img_grid = torchvision.utils.make_grid([pred, gtcpu, diff])
    img_grid = img_grid ** (1 / 2.2)
    img_grid = 1.0 * img_grid / img_grid.max()
    img_grid = torch.clip(img_grid, 0.0, 1.0)
    writer.add_image(tensorboard_tag, img_grid, global_step=iteration)

    originpos = torch.zeros(1, 3, device=gpudevice)
    if withlight:
        originpos = torch.cat((lightdir[0:1, :], originpos), dim=1)
    if withnormals:
        originpos = torch.cat((originpos, lightdir[0:1, :]), dim=1)
    net_output = net(originpos)
    sglights = deparametrize_torch(net_output.view(numSGs, 6).unsqueeze_(0), sg_centers,
                                       exponentiate).squeeze()
    envmap = displaySGEnvmap(sglights.cpu().detach())
    gtl = torch.zeros(1, 7)
    gtl[0, 0:3] = 1.0
    gtl[0, 4:7] = torch.from_numpy(randomdir)
    gtl[0, 3] = 1000
    gt_envmap = displaySGEnvmap(gtl)
    img_grid2 = torchvision.utils.make_grid([envmap.permute(2, 0, 1), gt_envmap.permute(2, 0, 1)])
    writer.add_image('envmap at origin', img_grid2, global_step=iteration)


def get_buffers(scene_train, randomsensor):
    # Custom rendering pipeline in Python (from Mitsuba2 doc)
    film = randomsensor.film()
    sampler = randomsensor.sampler()
    film_size = film.crop_size()
    total_sample_count = ek.hprod(film_size)
    if sampler.wavefront_size() != total_sample_count:
        sampler.seed(0, total_sample_count)  # deactivate all the random samples ???

    # Enumerate discrete sample & pixel indices, and uniformly sample positions within each pixel.
    pos = ek.arange(UInt32, total_sample_count)
    # pos //= spp
    scale = Vector2f(1.0 / film_size[0], 1.0 / film_size[1])
    pos = Vector2f(Float(pos % int(film_size[0])), Float(pos // int(film_size[0])))
    # pos += sampler.next_2d() # try deactivating this (no random position within the pixel but only center...)

    # Sample rays starting from the camera sensor
    rays, weights = randomsensor.sample_ray_differential(time=0, sample1=sampler.next_1d(), sample2=pos * scale,
                                                         sample3=0)
    # Intersect rays with the scene geometry
    si = scene_train.ray_intersect(rays)
    active_b = si.is_valid() & (Frame3f.cos_theta(si.wi) > 0.0)
    ctx = BSDFContext()
    bsdf = si.bsdf(rays)

    # print(bsdf.id())

    # world positions of intersections
    _mask = active_b.torch().bool()
    _normals = si.to_world(Vector3f(0, 0, 1)).torch()[_mask, :]
    _bsdf_val = BSDF.eval_vec(bsdf, ctx, si, Vector3f([0.0, 0.0, 1.0]), active_b).torch()[_mask, :]
    _pos3d = si.p.torch()[_mask, :]
    # im = ek.select(active_b, si.p, Vector3f(0)).torch().cpu().unsqueeze(0).view(256, 256, 3)
    # plt.imshow(im)
    # print('pos ', im.min(), im.max())
    # plt.show()
    #
    # im = ek.select(active_b, si.to_world(Vector3f(0, 0, 1)), Vector3f(0)).torch().cpu().unsqueeze(0).view(256, 256, 3)
    # plt.imshow(im)
    # print('normal ', im.min(), im.max())
    # plt.show()
    #
    # im = ek.select(active_b, BSDF.eval_vec(bsdf, ctx, si, Vector3f([0.0, 0.0, 1.0]), active_b), Vector3f(0)).torch().cpu().unsqueeze(0).view(256,256,3)
    # plt.imshow(im)
    # print('bsdf ', im.min(), im.max())
    # plt.show()
    return _mask, _pos3d, _normals, _bsdf_val

def get_buffers_glossy(scene_train, randomsensor):
    film = randomsensor.film()
    sampler = randomsensor.sampler()
    film_size = film.crop_size()
    total_sample_count = ek.hprod(film_size)
    # if sampler.wavefront_size() != total_sample_count:
    #     sampler.seed(0, total_sample_count)
    pos = ek.arange(UInt32, total_sample_count)
    scale = Vector2f(1.0 / film_size[0], 1.0 / film_size[1])
    pos = Vector2f(Float(pos % int(film_size[0])), Float(pos // int(film_size[0])))
    rays, weights = randomsensor.sample_ray_differential(time=0, sample1=sampler.next_1d(), sample2=pos * scale, sample3=0)
    si = scene_train.ray_intersect(rays)
    active_b = si.is_valid() & (Frame3f.cos_theta(si.wi) > 0.0)
    ctx = BSDFContext()
    bsdf = si.bsdf(rays)

    # print(bsdf.id())

    _mask = active_b.torch().bool()
    _roughness = BSDF.get_alpha_vec(bsdf, si).torch()
    _mask = _mask * ~((_roughness != _roughness).any())
    _mask = _mask * (_roughness > 0.0)
    _m2 = BSDF.get_alpha_vec(bsdf, si) ** 2
    _ndf_amp = ek.select(active_b, Vector3f(1.0) / (_m2 * np.pi), Vector3f(0)).torch()[_mask, :]
    _ndf_sharp = ek.select(active_b, Float(2.0) / _m2, Float(0)).torch()[_mask].unsqueeze_(1).expand(-1, 3)
    _wi = si.to_world(si.wi).torch()[_mask, :]
    _normals = si.to_world(Vector3f(0, 0, 1)).torch()[_mask, :]
    _bsdf_val = BSDF.eval_vec(bsdf, ctx, si, reflect(si.wi), active_b).torch()[_mask, :]
    _pos3d = si.p.torch()[_mask, :]
    return _mask, _pos3d, _normals, _bsdf_val, _ndf_amp, _ndf_sharp, _wi

def generate_random_sensor(spp, width, height):
    r = 5# 3.5
    angle = np.random.rand() * 2.0 * np.pi
    angle2 = np.random.rand() * np.pi
    camX = r * np.cos(angle) * np.cos(angle2)
    camY = np.abs(r * np.sin(angle2))
    camZ = r * np.sin(angle) * np.cos(angle2)
    origin_ = [camX, camY, camZ] # Vector3f(camX, camY, camZ)
    campos = torch.tensor(np.array(origin_).astype(np.float32))
    target_ = [0,0.8,0]  #hardcoded for now
    sensor = load_dict({
        "type": "perspective",
        "to_world": ScalarTransform4f.look_at(origin=origin_, target=target_, up=[0,1,0]),
        "near_clip": 0.1,
        "far_clip": 100.0,
        "myfilm": {
            "type": "hdrfilm",
            "rfilter": {"type": "box"},
            "width": width, "height": height,
        },
        "mysampler": {
            "type": "independent", "sample_count": spp,
        },
    })
    return sensor, campos

def generate_completely_random_sensor(spp, width, height):
    origin_ = 0.4 * np.random.rand(3) + 0.3 #rescale so it's between 10 and 90% of the bounding box
    target_ = np.random.rand(3) - 0.5

#     Living room:  min 0.994725 0.368096 -3.305306
# Mesh Bounding Box max 1.245299 0.695174 -2.925946
#     min 0.000000 0.000000  -3.632799
#     Mesh Bounding Box
#     max     5.122005     2.879001     0.000000

    origin_[0] =  5.122005  * origin_[0]
    origin_[1] =  2.879001  * origin_[1]
    origin_[2] = -3.632799 + 3.632799 * origin_[2]

    target_ = target_ + origin_

    print('Camera origin: ', origin_, ' looking at ',target_)
    campos = torch.tensor(np.array(origin_).astype(np.float32))
    sensor = load_dict({
        "type": "perspective",
        "to_world": ScalarTransform4f.look_at(origin=origin_, target=target_, up=[0,1,0]),
        "near_clip": 0.01,
        "far_clip": 100.0,
        "myfilm": {
            "type": "hdrfilm",
            "rfilter": {"type": "box"},
            "width": width, "height": height,
        },
        "mysampler": {
            "type": "independent", "sample_count": spp,
        },
    })
    return sensor, campos

def shadeSGs(sglights, n, wi, ndf_amp, ndf_sharp, hybridanis=True, anisowarp=True, sharpness_thresh=5.0, device='cuda:0'):
    if hybridanis:
        ndf_val = torch.zeros(wi.shape, device=device)

        for l in range(sglights.size()[1]):
            anismask = (sglights[:, l, 3] > sharpness_thresh).bool()
            anismask3 = anismask.unsqueeze(1).expand(-1, 3)

            # print(sg_amp.mean(), sg_sharp.mean(), sg_axi.mean())
            # print(amp.mean(), warpX.mean(), warpY.mean(), warpZ.mean(), sharpX.mean(), sharpY.mean())

            if torch.sum(anismask)>0:
                # anisotropic rotated SG NDF
                amp, warpX, warpY, warpZ, sharpX, sharpY = warpBRDF_ASG(ndf_amp[anismask, :], ndf_sharp[anismask, :],
                                                                        n[anismask, :], wi[anismask, :])
                ndf_val[anismask, :] = ndf_val[anismask, :] + convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY,
                                                                       sglights[anismask, l,0:3], sglights[anismask, l,3].unsqueeze(1).expand(-1,3), sglights[anismask, l,4:7])
                print('ndf_val iso ', ndf_val.mean())

            if torch.sum(~anismask)>0:
                # isotropic rotated SG NDF
                sg_amp, sg_sharp, sg_axi = warpBRDF_SG(ndf_amp[~anismask, :], ndf_sharp[~anismask, :], n[~anismask, :],
                                                       wi[~anismask, :])
                ndf_val[~anismask, :] = ndf_val[~anismask, :] + SGinnerProduct(sglights[~anismask, l, 0:3], sglights[~anismask, l, 3].unsqueeze(1).expand(-1,3), sglights[~anismask, l, 4:7],
                                         sg_amp, sg_sharp, sg_axi)
                print('ndf_val aniso ', ndf_val.mean())
    else:
        if anisowarp:
            # anisotropic rotated SG NDF
            amp, warpX, warpY, warpZ, sharpX, sharpY = warpBRDF_ASG(ndf_amp, ndf_sharp, n, wi)
            ndf_val = torch.zeros(wi.shape, device=device)

            for l in range(sglights.size()[1]):
                ndf_val = ndf_val + convolveASG_SG(amp, warpX, warpY, warpZ, sharpX, sharpY, sglights[:, l, 0:3],
                                                   sglights[:, l, 3].unsqueeze(1).expand(-1, 3), sglights[:, l, 4:7])
            print('ndf_val aniso ', ndf_val.mean())
        else:
            ndf_val = torch.zeros(wi.shape, device=device)
            sg_amp, sg_sharp, sg_axi = warpBRDF_SG(ndf_amp, ndf_sharp, n, wi)

            assert (sg_amp != sg_amp).any() == False, "Damn! sg_amp contains NaNs"
            assert (sg_sharp != sg_sharp).any() == False, "Damn! sg_sharp contains NaNs"
            assert (sg_axi != sg_axi).any() == False, "Damn! sg_axi contains NaNs"
            assert torch.isinf(sg_amp).any() == False, "Damn! sg_amp contains Infs"
            assert torch.isinf(sg_sharp).any() == False, "Damn! sg_sharp contains Infs"
            assert torch.isinf(sg_axi).any() == False, "Damn! sg_axi contains Infs"
            for l in range(sglights.size()[1]):
                ndf_val = ndf_val + SGinnerProduct(sglights[:, l, 0:3], sglights[:, l, 3].unsqueeze(1).expand(-1, 3),
                                                   sglights[:, l, 4:7], sg_amp, sg_sharp, sg_axi)
            # print('ndf_val simple ', ndf_val.mean())
        print('max sglight ', sglights[:, l, 3].max())
        assert torch.isinf(ndf_val).any() == False, "Damn! ndf_val contains Infs"
    return ndf_val

def stephen_hill_irradiance(normal, amp, sharp, axi, device='cuda:0'):
    muDotN = dot(normal, axi)
    lam = sharp.unsqueeze(1).expand(-1,3)
    c0 = 0.36
    eml = torch.exp(-lam)
    scale = 1.0 + 2.0 * eml * eml - 1.0 / lam
    bias = (eml - eml * eml) * 1.0 / lam - eml * eml
    x0 = c0 * muDotN
    x1 = torch.sqrt(1.0 - scale) / (4.0 * c0)
    n = x0 + x1

    y = torch.zeros(muDotN.size(), device=device)
    mask = (muDotN > 0.0).bool()
    y[mask] = y[mask] + muDotN[mask]
    mask = (torch.abs(x0) <= x1).bool()
    y[mask] = (n*n / torch.sqrt(1.0 - scale))[mask]

    result = scale * y + bias
    return result * 2.0 * np.pi * (1.0 - torch.exp(-2.0 * lam)) * amp / lam

def SGinnerProduct(amp1, sharp1, axi1, amp2, sharp2, axi2):
    # print(amp1.mean(), sharp1.mean(), axi1.mean(), amp2.mean(), sharp2.mean(), axi2.mean())
    um = sharp2 * normalize(axi2) + sharp1 * normalize(axi1)

    umLength = torch.clamp(torch.sqrt(torch.sum(um**2, dim=1, keepdim=True)), min=1e-6).expand(-1,3)
    # umLength = norm(um) + 0.0000001
    # print("umLength nill ", torch.sum(umLength==0.0))
    expo = torch.exp(umLength - sharp1 - sharp2)
    expo = expo * amp1 * amp2
    other = -torch.exp(-2.0 * umLength) + 1.0

    # print('sg stats')
    # print(sharp1.min(), sharp1.mean(), sharp1.max())
    # print(sharp2.min(), sharp2.mean(), sharp2.max())
    # print('interm')
    # print(expo.min(), other.min(), umLength.min())
    # print(expo.mean(), other.mean(), umLength.mean())
    # print(expo.max(), other.max(), umLength.max())

    # mask = (dot(axi1, axi2) > 0.0) # hack for SGs with wide support to not contribute to occluded normals
    # return torch.nn.functional.relu(mask * 2.0 * np.pi * expo * other) / umLength
    return 2.0 * np.pi * expo * other / umLength
    # return torch.nn.functional.relu(2.0 * np.pi * expo * other) / umLength

def exact_cosSGinnerProduct(normal, amp, sharp, axi):
    cos_sharpness = 2.133
    cos_amplitude = 1.17
    umLength = norm(cos_sharpness * normal + sharp.unsqueeze(1).expand(-1,3) * normalize(axi))
    lambdam = sharp.unsqueeze(1).expand(-1,3) + cos_sharpness
    sinh_term = torch.exp(umLength-lambdam) - torch.exp(-umLength-lambdam)
    return torch.nn.functional.relu(2.0 * np.pi * amp * cos_amplitude * sinh_term) / umLength

def dot(a,b):
    return torch.sum(a*b, dim=-1, keepdim=True).expand(-1,3)

def norm(v):
    return torch.sqrt(torch.sum(v**2, dim=-1, keepdim=True)).expand(-1,3)

def normalize(v):
    return v/norm(v)

def warpBRDF_SG(amp, sharp, axi, wi, device='cuda:0'):
    w_axi = normalize(-wi + 2.0*dot(wi,axi)*axi)
    thresh = torch.nn.Threshold(0.00001, 0.00001, inplace=True)
    # mask = (dot(wi, axi) > 0.00001)
    # w_sharp = torch.zeros(sharp.shape, device=device)
    w_sharp = sharp / (4.0 * thresh(dot(wi,axi)))
    return amp, w_sharp, w_axi

def warpBTDF_SG(amp, sharp, axi, wi, ior, device='cuda:0'):
    # coswi = torch.clamp(dot(wi,axi), 0.0)  #view cosine
    # sin_term = torch.sqrt(coswi**2 + ior**2 - 1.0)
    # # w_trans = normalize(-wi + (coswi - sin_term) * axi)
    # w_trans = normalize((-wi + (coswi - sin_term) * axi) / ior)
    #
    #
    # # changed the denominator to a plus instead of minus...
    # w_sharp = sharp * ior * sin_term / ((coswi / ior - sin_term)**2)
    # # w_sharp = sharp * ior * sin_term / ((coswi - sin_term)**2)

    cosv = torch.clamp(dot(wi, axi), 0.0)  # view cosine
    cosl = torch.sqrt(1.0 - (1.0 - cosv**2) / ior**2)   # + ior**2 - 1.0)
    w_trans = normalize(-wi + (cosv - ior * cosl) * axi)
    w_sharp = sharp * ior * cosl / ((cosv - ior * cosl) ** 2)

    assert (w_sharp != w_sharp).any() == False, "Damn! w_sharp contains NaNs"
    assert (cosl != cosl).any() == False, "Damn! sin_term contains NaNs"
    return amp, w_sharp, w_trans

def evaluate_ASG(amp, x, y, z, sharpX, sharpY, direction):
    sTerm = torch.nn.functional.relu(dot(z, direction))
    lambdaTerm = sharpX * dot(direction, x) * dot(direction, x)
    muTerm = sharpY * dot(direction, y) * dot(direction, y)
    sTerm = sTerm * torch.exp(-lambdaTerm - muTerm)
    return amp * sTerm

def convolveASG_SG(aniso_amp, aniso_x, aniso_y, aniso_z, aniso_sharpX, aniso_sharpY, amp, sharp, axi):
    nu = sharp * 0.5 #The ASG paper specifes an isotropic SG as exp(2 * nu * (dot(v, axis) - 1))
    out_sharpX = (nu * aniso_sharpX) / (nu + aniso_sharpX)
    out_sharpY = (nu * aniso_sharpY) / (nu + aniso_sharpY)
    out_amp = np.pi / torch.sqrt((nu + aniso_sharpX) * (nu + aniso_sharpY))
    return evaluate_ASG(amp * aniso_amp * out_amp, aniso_x, aniso_y, aniso_z, out_sharpX, out_sharpY, axi)


def warpBRDF_ASG(amp, sharp, axi, wi, device='cuda:0'):
    # // Generate any orthonormal basis with Z pointing in the direction of the reflected view vector
    warpZ = normalize(-wi + 2.0 * dot(wi, axi) * axi)
    warpX = normalize(torch.cross(axi, warpZ, dim=-1))
    warpY = normalize(torch.cross(warpZ, warpX, dim=-1))
    # print(ek.cross(warpZ, warpY)[0][1000000:1000009])
    # print(warpX[0][1000000:1000009])
    mask = (dot(wi, axi)>0.0001).bool()
    # // Second derivative of the sharpness with respect to how far we are from basis Axis direction
    sharpX = torch.zeros(mask.shape, device=device)
    sharpY = torch.zeros(mask.shape, device=device)
    # print(axi.shape, mask.shape, sharpX.shape, sharp.shape)
    sharpX[mask] = sharp[mask] / (8.0 * dot(wi, axi)[mask] * dot(wi, axi)[mask])
    sharpY[mask] = sharp[mask] / 8.0
    return amp, warpX, warpY, warpZ, sharpX, sharpY


def initialiseSGs(numSGs): # unused method
    params = torch.zeros([numSGs, 6])
    fiboangles = torch.zeros([numSGs, 3])
    phi = np.pi * (3. - np.sqrt(5.))  # golden angle in radians
    for i in range(numSGs):
        y = 1.0 - (i / float(numSGs - 1)) * 2  # y goes from 1 to -1
        radius = np.sqrt(1 - y * y)  # radius at y
        theta = phi * i  # golden angle increment
        x = np.cos(theta) * radius
        z = np.sin(theta) * radius
        params[i, 3] = torch.asin(z)
        params[i, 4] = torch.atan2(y, x)
        params[i, 5] = np.log(numSGs / np.pi)
    print(params)
    return params

def fibonacci_sphere(samples):
    points = torch.zeros([samples, 3])
    phi = np.pi * (3. - np.sqrt(5.))  # golden angle in radians
    for i in range(samples):
        y = 1.0 - (i / float(samples - 1)) * 2  # y goes from 1 to -1
        radius = np.sqrt(1 - y * y)  # radius at y
        theta = phi * i  # golden angle increment
        x = np.cos(theta) * radius
        z = np.sin(theta) * radius
        points[i, :] = torch.tensor([x, y, z])
    # print(points)
    return points

def initialiseSGcenters(numSGs):
    initial_angles = torch.zeros([numSGs, 2])
    fibop = fibonacci_sphere(numSGs)
    for i in range(numSGs):
        with torch.no_grad():
            initial_angles[i,0] = torch.asin(fibop[i,1])
            initial_angles[i,1] = torch.atan2(fibop[i,2],fibop[i,0])
    print(initial_angles)
    return initial_angles

def deparametrize_torch(sgcodes, sgcenter, exponentiate=True, device='cuda:0'):
    sgs = torch.zeros(sgcodes.size()[0], sgcodes.size()[1], 7, device=device)
    # sgs.requires_grad = True
    if exponentiate:
        sgs[:, :, 0:3] = torch.exp(sgcodes[:, :, 0:3]) #RGB amplitude
        # sgs[:, :, 3] = torch.exp(sgcodes[:, :, 5]) #sharpness
        sgs[:, :, 3] = torch.exp(sgcodes[:, :, 5])
        sgs[:, :, 4:7] = thetaphi_to_xyz(
            np.pi / 2 * torch.tanh(sgcodes[:, :, 3]) + sgcenter[:, 0],
            np.pi / 2 * torch.tanh(sgcodes[:, :, 4]) + sgcenter[:, 1])
    else:
        m = torch.nn.Softplus()
        # sgs[:, :, 0:3] = torch.exp(sgcodes[:, :, 0:3]) #**2
        sgs[:, :, 0:3] = m(sgcodes[:, :, 0:3])
        # sgs[:, :, 3] = torch.sqrt( torch.nn.functional.relu(sgcodes[:, :, 5]) )  # sharpness
        # sgs[:, :, 3] = m(sgcodes[:, :, 5])
        sgs[:, :, 3] = torch.exp(sgcodes[:, :, 5])

        sgs[:, :, 4:7] = thetaphi_to_xyz(
            # np.pi / 2 / torch.sqrt(sgcodes.shape[1]) * torch.tanh(sgcodes[:, :, 3]) + sgcenter[:, 0],
            # np.pi / 2 / torch.sqrt(sgcodes.shape[1]) * torch.tanh(sgcodes[:, :, 4]) + sgcenter[:, 1])
            np.pi / 2 * torch.tanh(sgcodes[:, :, 3]) + sgcenter[:, 0],
            np.pi / 2 * torch.tanh(sgcodes[:, :, 4]) + sgcenter[:, 1])
        # (np.pi / 2 - torch.abs(sgcenter[:, 0])) * np.pi * torch.tanh(sgcodes[:, :, 4]) + sgcenter[:, 1])

    # (np.pi / 2 - torch.abs(sgcenter[:, 0])) *

    # print('norms ', norm(sgs[:,:,4:7].view(-1,3)).min(), norm(sgs[:,:,4:7].view(-1,3)).max())
    return sgs



def deparametrize_2(sgcodes, device='cuda:0'):
    sgs = torch.zeros(sgcodes.size()[0], sgcodes.size()[1], 7, device=device)
    sgs[:, :, 0:3] = torch.exp(sgcodes[:, :, 0:3]) #  - 1.0 + 1e-6
    sgs[:, :, 3] = torch.exp(sgcodes[:, :, 3])
    sgs[:, :, 4:7] = sgcodes[:, :, 4:7] / torch.clamp(torch.sqrt(torch.sum(sgcodes[:, :, 4:7] * sgcodes[:, :, 4:7],
                                         dim=2, keepdim=True)), min=1e-6).expand(-1,-1,3)

    assert (sgs[:, :, 0:3] != sgs[:, :, 0:3]).any() == False, "Damn! " \
                                      "predicted amplitudes contains NaNs"
    assert (sgs[:, :, 3:4] != sgs[:, :, 3:4]).any() == False, "Damn! " \
                                      "predicted amplitudes contains NaNs"
    assert (sgs[:, :, 4:7] != sgs[:, :, 4:7]).any() == False, "Damn! " \
                            "predicted directions contains NaNs"
    return sgs

def LSregress(pred, gt, origin):
    nb = pred.size(0)
    origSize = pred.size()
    pred = pred.reshape(nb, -1)
    gt = gt.reshape(nb, -1)

    coef = (torch.sum(pred*gt, dim=1) / torch.clamp(torch.sum(pred*pred, dim=1), min=1e-5) ).detach()
    coef = torch.clamp(coef, 0.001, 1000)
    for n in range(0, len(origSize) -1):
        coef = coef.unsqueeze(-1)
    pred = pred.view(origSize)

    predNew = origin * coef.expand(origSize)
    return predNew


def signedexp(t):
    return torch.sign(t) * (torch.exp(torch.abs(t)) - 1.0)

def deparametrize(sgcodes, sgcenter):
    sgs = []
    with torch.no_grad():
        for l in range(sgcodes.size()[0]):
            code = sgcodes[l,:]
            center = sgcenter[l,:]
            theta = np.pi / 2 * torch.tanh(code[3]) + center[0]
            phi = np.pi * torch.tanh(code[4]) + center[1]
            ax = thetaphi_to_xyz(theta, phi).numpy()
            code = torch.exp(code).numpy()
            sg = [code[0], code[1], code[2], code[5], ax[0], ax[1], ax[2]]
            sgs.append(sg)
    return sgs

# def readEXR_RGB(filepath, astorch=True):
#     file = OpenEXR.InputFile(filepath)
#     header = file.header()
#     dw = header['dataWindow']
#     imsize = (dw.max.y - dw.min.y + 1, dw.max.x - dw.min.x + 1)
#     (r, g, b) = file.channels("RGB")
#
#     if header['channels']['R'] == Imath.Channel(Imath.PixelType(OpenEXR.HALF)):
#         filetype = np.float16
#
#     if header['channels']['R'] == Imath.Channel(Imath.PixelType(OpenEXR.FLOAT)):
#         filetype = np.float32
#
#     im = np.zeros((imsize[0], imsize[1], 3), dtype=np.float32)
#     rnp = np.reshape(np.frombuffer(r, dtype=filetype), imsize)
#     gnp = np.reshape(np.frombuffer(g, dtype=filetype), imsize)
#     bnp = np.reshape(np.frombuffer(b, dtype=filetype), imsize)
#     im[:, :, 0] = rnp.astype(np.float32)
#     im[:, :, 1] = gnp.astype(np.float32)
#     im[:, :, 2] = bnp.astype(np.float32)
#
#     if astorch:
#         return torch.from_numpy(im)
#     else:
#         return im
#
def writeEXR_RGB(im, savepath):
    im32 = np.float32(im)
    (r, g, b) = [im32[:, :, c].tostring() for c in range(0, 3)]
    # print(im32.shape, im32.dtype)
    out = OpenEXR.OutputFile(savepath, OpenEXR.Header(im32.shape[1], im32.shape[0]))
    out.writePixels({'R': r, 'G': g, 'B': b})

def displayEXR(im):
    print(im.size())
    im2 = torch.clip(im, 0.0, 1.0)**(1.0/2.2)
    plt.imshow(im2)
    plt.show()
    # plt.draw()
    # plt.show(block=False)

def thetaphi_to_xyz(theta, phi, CHW=False): #angles in radians
    # theta = angles.select(-1, 0) #* np.pi / 180.0
    # phi = angles.select(-1, 1) #* np.pi / 180.0
    x = -torch.cos(theta) * torch.sin(phi)
    y = torch.sin(theta)
    z = torch.cos(theta) * torch.cos(phi)
    if CHW:
        return torch.stack([x, y, z], dim=0)
    else:
        return torch.stack([x, y, z], dim=-1)

def create_latlong_grid(imheight, imwidth):
    # return the environment grid as (teta,phi): elevation [-90,90], azimuth [-180,180]
    grid = torch.stack(torch.meshgrid([-torch.linspace(-89.5, 89.5, steps=imheight), torch.linspace(-179.5, 179.5, steps=imwidth)]), dim=-1)
    return grid.reshape(imheight, imwidth, 2) * np.pi / 180.0

def create_envmap_grid(imheight, imwidth):
    latlong = create_latlong_grid(imheight, imwidth)
    return thetaphi_to_xyz(latlong.select(-1, 0), latlong.select(-1, 1))

def create_neural_coord_grid(imheight, imwidth):
    latlong = create_latlong_grid(imheight, imwidth)
    return thetaphi_to_xyz(latlong.select(-1, 0), latlong.select(-1, 1), CHW=True)

def norm3d(v):
    return torch.sqrt(torch.sum(v**2, dim=-1, keepdim=False))

def normalize3d(v):
    n = torch.sqrt(torch.sum(v**2, dim=-1, keepdim=True))
    return v/n

# def evaluate_sg_code(code, dir):
#     std = norm3d(code[3:6])
#     ax = normalize3d(code[3:6])
#     n = code[0:3]
#     return evaluate_sg(n, ax, std, dir)

def evaluate_sg_envmap(code, sgcenter, imheight, imwidth, use_cuda=True):
    # code is a single SG parameter vector
    grid = create_envmap_grid(imheight, imwidth)
    if use_cuda:
        grid = grid.cuda()

    amp = code[0:3].unsqueeze(0).unsqueeze(0).expand(imheight, imwidth, 3)
    theta = np.pi / 2 * torch.tanh(code[3]) + sgcenter[0]
    phi = np.pi * torch.tanh(code[4]) + sgcenter[1]

    std = torch.exp(code[5])
    # std = code[5]**2
    amp = torch.exp(amp)
    axis = thetaphi_to_xyz(theta, phi) #[torch.cos(theta) * torch.cos(phi), torch.cos(theta) * torch.sin(phi), torch.sin(theta)]

    cosangle = grid[:,:,0]*axis[0] + grid[:,:,1]*axis[1] + grid[:,:,2]*axis[2]
    return amp * torch.exp( std * (cosangle.unsqueeze(2).expand(imheight, imwidth, 3) - 1.0) )

def displaySGEnvmap(lightSG):
    xyzgrid = create_envmap_grid(128,256)
    # g = create_latlong_grid(128,256)
    # f = thetaphi_to_xyz(g.select(-1,0), g.select(-1,1))
    val = torch.zeros(xyzgrid.size())
    for l in range(lightSG.size()[0]):
        cosangle = (xyzgrid[:,:,0]*lightSG[l,4] + xyzgrid[:,:,1]*lightSG[l,5] + xyzgrid[:,:,2]*lightSG[l,6]).unsqueeze(2).expand(xyzgrid.size())
        col = lightSG[l,0:3].unsqueeze(0).unsqueeze(0).expand(xyzgrid.size())
        sg = col * torch.exp(lightSG[l,3] * (cosangle - 1.0))
        val = val + sg
    return val
    # im = val / val.max()
    # return im**(1.0/2.2)