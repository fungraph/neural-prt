import os
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os
import cv2
import nets
from torchvision.utils import save_image
import renderhelpers as sgr
import siren_pytorch as siren
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters =========================================================================================== 
basepath = '/home/grainer/'

frames_path = basepath + 'nprt/data/scenes/atelier/realtime'
envmap_path = basepath +  'nprt/data/scenes/indoorenvs/rotate'
outputpath = basepath +  'nprt/paper/paths/atelier/prt'
multiplier = 0.6
bbxmin = torch.tensor([-147.0, -44.5625, -170.25 ])
bbxmax = torch.tensor([147.0, 85.8125, 104.5 ])

netpath = basepath + 'nprt/paper/training/atelier/0499-prtbaseline---bats-3-lat-64-layer-2-width-256-lr-0.0001-net.pth'


# # # Parameters for BEDROOM scene:
envmap_path = basepath +  'nprt/data/scenes/outdoorenvs/rotate'
frames_path = 'F:/gilles/Dropbox/INRIA/projects/nprt/data/scenes/bedroom/path-alias'
outputpath = 'C:/Users/grainer/Documents/ibr_supplemental_website-master/ibr_supplemental_website-master/supplementary_in/bedroom/prt-inspired'
multiplier = 5 # 0.5 for atelier, 5 for kitchen, 5 bedroom, 10 for sanmiguel
bbxmin = torch.tensor([-2.978515625, -5.960464477539063e-08, -1.306640625 ])
bbxmax = torch.tensor([4.0234375, 2.380859375, 3.357421875  ])
netpath = basepath + 'nprt/paper/training/bedroom/0499-prtbaseline---bats-3-lat-64-layer-2-width-256-lr-0.0001-net.pth'



# Training parameters ==================================================================================================
insize = 19
latentdim = 64 
netwidth = 256 
batch_size = 3 
num_layers = 2 
learning_rate = 0.0001 
logloss = True 
L2loss = False  
max_epochs = 500  
cnndim = 2048  
withnormals = True 
encode_normals = False  

gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)

# Network loading ======================================================================================================
class Dumbnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, inpsize=6): # Retry this with alternate parametrization
        super(Dumbnet, self).__init__()
        self.sg_decoder = nets.posNet(latentdim + latentdim, 3, width, 2)
        # self.pos_decoder = nets.posNet(inpsize, latentdim, width, 2)
        self.pos_decoder = siren.SirenNet(dim_in=inpsize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = nets.EnvmapSGencoder(4096, latentdim)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        sgs = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
        return sgs

net = Dumbnet(latentdim, netwidth, num_layers, inpsize=insize).to(device=gpudevice)
print(net)
print("number of parameters ", sum(p.numel() for p in net.parameters() if p.requires_grad))
#Load network from PTH
net.load_state_dict(torch.load(netpath))
net.eval()


# Render path===========================================================================================================
class BufferDataset(Dataset):
    def __init__(self, renderpath, envpath, normalizepos=False):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []

        self.envfiles = []

        self.bbxmin = torch.tensor([10000.0, 10000.0, 10000.0])
        self.bbxmax = torch.tensor([-10000.0, -10000.0, -10000.0])
        for r, d, f in os.walk(envpath):  # r=root, d=directories, f = files
            f.sort()
            for file in f:
                if '.exr' in file:
                    self.envfiles.append(file)
                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                                      cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)

                    im = torch.log(1.0 + im)

                    # self.envmap.append(im)
                    self.envmap.append(im.to(device=gpudevice))
                    # print(file)

        c = 0
        for r, d, f in os.walk(renderpath):  # r=root, d=directories, f = files
            f.sort()
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('.')
                    if int(fname[0]) == c:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)
                    # print(file)

                    if fname[2] == 'posW':
                        # self.position.append(im.to(device=gpudevice))
                        self.position.append(im)
                        for coord in range(3):
                            if im[:, :, coord].min() < self.bbxmin[coord]:
                                self.bbxmin[coord] = im[:, :, coord].min()
                            if im[:, :, coord].max() > self.bbxmax[coord]:
                                self.bbxmax[coord] = im[:, :, coord].max()

                    if fname[2] == 'alpha':
                        # self.alpha.append(im.to(device=gpudevice))
                        self.alpha.append(im)
                    if fname[2] == 'diffuseOpacity':
                        # self.diffuse.append((im/ np.pi).to(device=gpudevice) )
                        self.diffuse.append(im / np.pi)
                    if fname[2] == 'output':
                        # self.gt.append(im.to(device=gpudevice))
                        self.gt.append(im)
                    if fname[2] == 'normW':
                        # self.normals.append(im.to(device=gpudevice))
                        self.normals.append(im)
                    if fname[2] == 'specRough':
                        im = torch.clamp(im, min=0.0, max=10000.0)
                        # self.specular.append(im.to(device=gpudevice))
                        self.specular.append(im)
                    if fname[2] == 'viewW':
                        # self.viewdir.append(im.to(device=gpudevice))
                        self.viewdir.append(im)

        # self.bbxmin = self.bbxmin.to(device=gpudevice)
        # self.bbxmax = self.bbxmax.to(device=gpudevice)

    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        # env = torch.log(1.0 + self.envmap[idx % len(self.envmap)])
        env = self.envmap[idx % len(self.envmap)]
        # env = self.envmap[idx % len(self.envmap)]

        pos = self.position[idx]
        pos = (pos - self.bbxmin.expand(pos.shape[0], pos.shape[1], -1)) \
             / (self.bbxmax - self.bbxmin).expand(pos.shape[0], pos.shape[1], -1)
        pos = (pos - 0.5) * 2

        alpha = self.alpha[idx]
        # alpha = torch.sqrt(self.alpha[idx])

        diff = self.diffuse[idx]
        gt = self.gt[idx]
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]
        return alpha, pos, normals, viewdir, diff, specular, gt, env

    def getBB(self):
        return self.bbxmin, self.bbxmax
    def setBB(self, bbxmin, bbxmax):
        self.bbxmin = bbxmin
        self.bbxmax = bbxmax

def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap, gpudevice, net, timings=False):
    # flatten everything
    mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    mask = mask[:,:,0].view(-1)

    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)

    # isolate pixels that need shading
    # mask = (alpha > 0.0)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]
    netin = pos3d

    netin = torch.cat((netin, normals), dim=1)
    netin = torch.cat((netin, alpha.unsqueeze(1), specular_bsdf_val, diffuse_bsdf_val, wi), dim=1)
    #
    # # # ADD REFLECTED DIRECTION
    reflected_dir = sgr.normalize(-wi + 2.0 * sgr.dot(wi, normals) * normals)
    # netin = torch.cat((netin, reflected_dir), dim=1)
    netin = torch.cat((netin, reflected_dir), dim=1)

    assert (envmap != envmap).any() == False, "Damn! envmap contains NaNs"
    if timings:
        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)

    envinp = envmap.unsqueeze(0).to(device=gpudevice)
    netin = netin.to(device=gpudevice)
    # print(netin.shape)

    if timings:
        start.record()

    # diff, spec = net(envinp, netin)
    out = net(envinp, netin)


    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))

    # print(out.min(), out.mean(), out.max())

    # pred = diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(diff) + specular_bsdf_val.to(device=gpudevice) \
    #        * torch.nn.functional.relu(spec)
    # diffuseim = torch.nn.functional.relu(diff)
    # specim = torch.nn.functional.relu(spec)

    pred = torch.nn.functional.relu(out)
    #     # diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrdiff) - 1.0) + specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrspec) - 1.0)
    #
    diffuseim = torch.nn.functional.relu(out).cpu() #diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrdiff) - 1.0)
    specim = torch.nn.functional.relu(out).cpu() #specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrspec) - 1.0)

    return pred, gtcpu.view(-1, 3)[mask, :], diffuseim, specim  #alpha.unsqueeze(1).expand(-1,3)

dataset = BufferDataset(frames_path, envmap_path)
dataset.setBB(bbxmin, bbxmax)
dataloader = DataLoader(dataset, batch_size=1, shuffle=False)


imcount = 0
print("rendering highres")
with torch.no_grad():
    for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in dataloader:
        gtenv = gtcpu_batch[0].clone()  # * envmask  # .to(gpudevice)

        pred_render, groundtruth, sharpprior, _ = render_from_buffers(alpha_batch[0], pos3d_batch[0],
                                                                      normals_batch[0], wi_batch[0],
                                                                      diffuse_bsdf_val_batch[0],
                                                                      specular_bsdf_val_batch[0],
                                                                      gtcpu_batch[0],
                                                                      envmap_batch[0].permute(2, 0, 1),
                                                                      gpudevice, net,
                                                                      timings=False)
        alpha = alpha_batch[0]  # .to(gpudevice)
        pos3d = pos3d_batch[0]  # .to(gpudevice)
        normals = normals_batch[0]  # .to(gpudevice)
        wi = wi_batch[0]  # .to(gpudevice)
        diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
        specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)

        envmap = envmap_batch[0].permute(2, 0, 1)
        alpha = alpha[:, :, 0].view(-1)
        mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
        envmask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=True)) < 0.5).expand(-1, -1, 3)
        gtenv = gtenv * envmask  # .to(gpudevice)

        pr = torch.zeros(gtcpu_batch[0].size())
        pr = pr.view(-1, 3)
        pr[mask, :] = pred_render.cpu()
        pr = pr.view(gtcpu_batch[0].shape)
        # print(pr.shape, gtenv.shape)
        pr = pr + gtenv

        imageio.imwrite(outputpath + "/" + str(imcount).zfill(8) + ".exr",
                        pr.cpu().numpy())
        pr = pr.permute(2, 0, 1)
        pr = torch.clip(multiplier * pr, 0.0, 1.0)
        pr = pr ** (1.0 / 2.2)
        save_image(pr, outputpath + "/" + str(imcount).zfill(8) + ".png")
        imcount = imcount + 1

print('Finished Rendering')