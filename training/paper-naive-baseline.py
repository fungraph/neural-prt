import os
import numpy as np
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
import sys
import os
import cv2
import nets
from torchvision.utils import save_image
import renderhelpers as sgr
import siren_pytorch as siren
import imageio
# imageio.plugins.freeimage.download() #Uncomment if the plugin to save EXR is not available on the system

torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True

# Input paths and parameters =========================================================================================== 
basepath = '/home/grainer/'

train_path = basepath + 'nprt/data/scenes/atelier/train'
test_path = basepath +  'nprt/data/scenes/atelier/test'
highres_test_path = basepath + 'nprt/data/scenes/atelier/path'

envmap_trainpath = basepath +  'nprt/data/scenes/indoorenvs/train'
envmap_testpath = basepath +  'nprt/data/scenes/indoorenvs/test'
highres_envmap_testpath = basepath + 'nprt/data/scenes/indoorenvs/rotate'

outputpath = basepath +  'nprt/res/paper/atelier'
multiplier = 0.6

experiment_name = 'naivebaseline--' #


# Training parameters ==================================================================================================
insize = 19

latentdim = 64 

netwidth = 256 
batch_size = 3 
num_layers = 4 

learning_rate = 0.0001 
logloss = True 
L2loss = False  
cnndim = 2048  
withnormals = True  
encode_normals = False  

tensorboard_writes = 10000
max_epochs = 500  # 10000
experiment_name = experiment_name + '-bats-' + str(batch_size) + '-lat-' + str(latentdim) + '-layer-' + str(
                    num_layers) + '-width-' + str(netwidth) + '-lr-' + str(learning_rate)
gpudevice = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
if not os.path.exists(outputpath):
    os.makedirs(outputpath)

class BufferDataset(Dataset):
    def __init__(self, renderpath, envpath, normalizepos=False):
        self.alpha = []
        self.diffuse = []
        self.specular = []
        self.envmap = []
        self.gt = []
        self.normals = []
        self.position = []
        self.viewdir = []

        self.envfiles = []

        self.bbxmin = torch.tensor([10000.0, 10000.0, 10000.0])
        self.bbxmax = torch.tensor([-10000.0, -10000.0, -10000.0])
        for r, d, f in os.walk(envpath):  # r=root, d=directories, f = files
            f.sort()
            for file in f:
                if '.exr' in file:
                    self.envfiles.append(file)
                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH),
                                      cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)

                    im = torch.log(1.0 + im)

                    # self.envmap.append(im)
                    self.envmap.append(im.to(device=gpudevice))
                    # print(file)

        c = 0
        for r, d, f in os.walk(renderpath):  # r=root, d=directories, f = files
            f.sort()
            for file in f:
                if '.exr' in file:
                    if c % 50 == 0:
                        print('Read ', c, ' EXR buffers.')

                    filename = os.path.splitext(os.path.basename(file))[0]
                    fname = filename.split('.')
                    if int(fname[0]) == c:
                        c = c+1

                    im = cv2.cvtColor(cv2.imread(os.path.join(r, file), cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH), cv2.COLOR_RGB2BGR)
                    im = torch.from_numpy(im)
                    # print(file)

                    if fname[2] == 'posW':
                        # self.position.append(im.to(device=gpudevice))
                        self.position.append(im)
                        for coord in range(3):
                            if im[:, :, coord].min() < self.bbxmin[coord]:
                                self.bbxmin[coord] = im[:, :, coord].min()
                            if im[:, :, coord].max() > self.bbxmax[coord]:
                                self.bbxmax[coord] = im[:, :, coord].max()

                    if fname[2] == 'alpha':
                        # self.alpha.append(im.to(device=gpudevice))
                        self.alpha.append(im)
                    if fname[2] == 'diffuseOpacity':
                        # self.diffuse.append((im/ np.pi).to(device=gpudevice) )
                        self.diffuse.append(im / np.pi)
                    if fname[2] == 'output':
                        # self.gt.append(im.to(device=gpudevice))
                        self.gt.append(im)
                    if fname[2] == 'normW':
                        # self.normals.append(im.to(device=gpudevice))
                        self.normals.append(im)
                    if fname[2] == 'specRough':
                        im = torch.clamp(im, min=0.0, max=10000.0)
                        # self.specular.append(im.to(device=gpudevice))
                        self.specular.append(im)
                    if fname[2] == 'viewW':
                        # self.viewdir.append(im.to(device=gpudevice))
                        self.viewdir.append(im)

        # self.bbxmin = self.bbxmin.to(device=gpudevice)
        # self.bbxmax = self.bbxmax.to(device=gpudevice)

    def __len__(self):
        return len(self.gt)

    def __getitem__(self, idx):
        # env = torch.log(1.0 + self.envmap[idx % len(self.envmap)])
        env = self.envmap[idx % len(self.envmap)]
        # env = self.envmap[idx % len(self.envmap)]

        pos = self.position[idx]
        pos = (pos - self.bbxmin.expand(pos.shape[0], pos.shape[1], -1)) \
             / (self.bbxmax - self.bbxmin).expand(pos.shape[0], pos.shape[1], -1)
        pos = (pos - 0.5) * 2

        alpha = self.alpha[idx]
        # alpha = torch.sqrt(self.alpha[idx])

        diff = self.diffuse[idx]
        gt = self.gt[idx]
        normals = self.normals[idx]
        specular = self.specular[idx]
        viewdir = self.viewdir[idx]
        return alpha, pos, normals, viewdir, diff, specular, gt, env

    def getBB(self):
        return self.bbxmin, self.bbxmax
    def setBB(self, bbxmin, bbxmax):
        self.bbxmin = bbxmin
        self.bbxmax = bbxmax

print('Load training data.')
train_dataset = BufferDataset(train_path, envmap_trainpath, normalizepos=True)
bbxmin, bbxmax = train_dataset.getBB()
print('Load validation data.')
val_dataset = BufferDataset(test_path, envmap_testpath)
val_dataset.setBB(bbxmin, bbxmax)
print('Training data loaded.')
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True) # , num_workers=10) # , pin_memory=True, drop_last=True)
val_dataloader = DataLoader(val_dataset, batch_size=1, shuffle=True) # , pin_memory=True, drop_last=True)
dataset = BufferDataset(highres_test_path, highres_envmap_testpath)
dataset.setBB(bbxmin, bbxmax)
dataloader = DataLoader(dataset, batch_size=1, shuffle=False)  # , pin_memory=True, drop_last=True)

# Setup NN, optimizer, tensorboard etc. ===================================================================================
class DumbDumbnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, inpsize=6): # Retry this with alternate parametrization
        super(DumbDumbnet, self).__init__()
        self.sg_decoder = nets.posNet(inpsize + latentdim, 3, width, num_layers)
        # self.sg_decoder = siren.SirenNet(dim_in=inpsize + 64, dim_hidden=width, dim_out=3, num_layers=num_layers,
        #                                  w0_initial=30.)

        self.encoder = nets.EnvmapSGencoder(4096, latentdim)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        sgs = self.sg_decoder(torch.cat((lightcode, pos), dim=1))
        return sgs

net = DumbDumbnet(latentdim, netwidth, num_layers, inpsize=insize).to(device=gpudevice)
print(net)
print("number of parameters ", sum(p.numel() for p in net.parameters() if p.requires_grad))

optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
writer = SummaryWriter(outputpath + '/runs/' + experiment_name)  # logloss-l1-adam-5e-3-testFaceNormals')
test_example = iter(val_dataloader)
iteration = 0

txtlog = open(outputpath + "/" + experiment_name + "-log.txt", "w")
txtlog.write("training log \n")
txtlog.write("Normalizing coordinates bounding box: " +"  \n")
txtlog.write("bbxmin : " + str(bbxmin[0].item()) +", " + str(bbxmin[1].item()) +", "+ str(bbxmin[2].item()) +"  \n")
txtlog.write("bbxmax : " + str(bbxmax[0].item()) +", " + str(bbxmax[1].item()) +", "+ str(bbxmax[2].item()) +"  \n")
txtlog.write("================\n")
txtlog.write("number of parameters " + str(sum(p.numel() for p in net.parameters() if p.requires_grad)) +"  \n")
txtlog.write(str(net))

def render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap, gpudevice, net, timings=False):
    # flatten everything
    mask = (torch.sqrt(torch.sum(normals**2, dim=-1, keepdim=True)).expand(-1,-1,3) > 0.0)  # to make sure, remove background
    gtcpu[~mask] = 0.0
    mask = mask[:,:,0].view(-1)

    pos3d = pos3d.view(-1, 3)
    normals = normals.view(-1, 3)
    diffuse_bsdf_val = diffuse_bsdf_val.view(-1, 3)
    specular_bsdf_val = specular_bsdf_val.view(-1, 3)
    alpha = alpha[:, :, 0].view(-1)
    wi = wi.view(-1, 3)

    # isolate pixels that need shading
    # mask = (alpha > 0.0)
    pos3d = pos3d[mask, :]
    pred = torch.zeros(pos3d.shape).to(device=gpudevice)
    normals = normals[mask, :]
    diffuse_bsdf_val = diffuse_bsdf_val[mask, :]
    specular_bsdf_val = specular_bsdf_val[mask, :]
    wi = wi[mask, :]
    alpha = alpha[mask]
    netin = pos3d

    netin = torch.cat((netin, normals), dim=1)
    netin = torch.cat((netin, alpha.unsqueeze(1), specular_bsdf_val, diffuse_bsdf_val, wi), dim=1)
    #
    # # # ADD REFLECTED DIRECTION
    reflected_dir = sgr.normalize(-wi + 2.0 * sgr.dot(wi, normals) * normals)
    # netin = torch.cat((netin, reflected_dir), dim=1)
    netin = torch.cat((netin, reflected_dir), dim=1)

    assert (envmap != envmap).any() == False, "Damn! envmap contains NaNs"
    if timings:
        start = torch.cuda.Event(enable_timing=True)
        end = torch.cuda.Event(enable_timing=True)

    envinp = envmap.unsqueeze(0).to(device=gpudevice)
    netin = netin.to(device=gpudevice)
    # print(netin.shape)

    if timings:
        start.record()

    # diff, spec = net(envinp, netin)
    out = net(envinp, netin)

    if timings:
        end.record()
        torch.cuda.synchronize()
        print("Time to evaluate network:  ", start.elapsed_time(end))

    pred = torch.nn.functional.relu(out) 
    diffuseim = torch.nn.functional.relu(out).cpu() #diffuse_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrdiff) - 1.0)
    specim = torch.nn.functional.relu(out).cpu() #specular_bsdf_val.to(device=gpudevice) * torch.nn.functional.relu(torch.exp(irrspec) - 1.0)

    return pred, gtcpu.view(-1, 3)[mask, :], diffuseim, specim  #alpha.unsqueeze(1).expand(-1,3)


for epoch in range(max_epochs):
    epoch_loss = 0.0

    for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in train_dataloader:
        iteration = iteration + 1
        optimizer.zero_grad()
        batch_loss = 0.0
        groundtruth, pred_render = [], []

        for b in range(alpha_batch.shape[0]):
            pred, gtcpu2, _, _ = render_from_buffers(alpha_batch[b], pos3d_batch[b], normals_batch[b], wi_batch[b],
                                                 diffuse_bsdf_val_batch[b], specular_bsdf_val_batch[b], gtcpu_batch[b],
                                                envmap_batch[b].permute(2,0,1),
                                                gpudevice, net, timings=False)

            groundtruth.append(gtcpu2)
            pred_render.append(pred)

        groundtruth = torch.cat(groundtruth, dim=0).to(gpudevice)
        pred_render = torch.cat(pred_render, dim=0)
        loss = sgr.my_loss(pred_render, groundtruth, tonemap=False, clip=False, log=logloss, L2=L2loss)

        batch_loss += loss.item()
        loss.backward()
        optimizer.step()
        writer.add_scalar('training loss', batch_loss, iteration)
        epoch_loss = epoch_loss + batch_loss / len(train_dataloader)

        with torch.no_grad():
            if iteration % tensorboard_writes == 1:
                try:
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()
                except StopIteration:
                    test_example = iter(val_dataloader)
                    alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch = test_example.next()

                alpha = alpha_batch[0]  # .to(gpudevice)
                pos3d = pos3d_batch[0]  # .to(gpudevice)
                normals = normals_batch[0]  # .to(gpudevice)
                wi = wi_batch[0]  # .to(gpudevice)
                diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
                specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)
                gtcpu = gtcpu_batch[0]  # .to(gpudevice)
                envmap = envmap_batch[0].permute(2, 0, 1)  # .to(gpudevice)

                pred, gtcpu2, diffim, specim = render_from_buffers(alpha, pos3d, normals, wi, diffuse_bsdf_val, specular_bsdf_val, gtcpu, envmap,
                        gpudevice, net, timings=True)

                alpha = alpha[:, :, 0].view(-1)
                mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
                pr = torch.zeros(gtcpu.size())
                pr = pr.view(-1, 3)
                pr[mask, :] = pred.cpu()
                pr = pr.view(gtcpu.shape)

                gtcpu = torch.zeros(normals.size())
                gtcpu = gtcpu.view(-1, 3)
                gtcpu[mask, :] = gtcpu2.cpu()
                gtcpu = gtcpu.view(normals.shape)

                diff = torch.zeros(normals.size())
                diff = diff.view(-1, 3)
                diff[mask, :] = diffim.cpu()
                diff = diff.view(normals.shape)
                spec = torch.zeros(normals.size())
                spec = spec.view(-1, 3)
                spec[mask, :] = specim.cpu()
                spec = spec.view(normals.shape)

                static_spec = torch.zeros(gtcpu.shape)
                static_spec = static_spec + specular_bsdf_val.cpu()
                static_spec = static_spec.permute(2, 0, 1)
                static_diff = torch.zeros(gtcpu.shape)
                static_diff = static_diff + diffuse_bsdf_val.cpu()
                static_diff = static_diff.permute(2, 0, 1)

                gtcpu = gtcpu.permute(2, 0, 1)
                pr = pr.permute(2, 0, 1)
                diff = diff.permute(2, 0, 1)
                spec = spec.permute(2, 0, 1)
                difference = torch.abs(pr - gtcpu)

                # print(gtcpu.shape, pr.shape, diff.shape)
                print('maxs and mins  ', gtcpu.max(), pr.max(), diff.max(), gtcpu.min(), pr.min(), diff.min())
                print('diffuse minmax : ', diff.min(), diff.mean(), diff.max())
                print('specular minmax : ', spec.min(), spec.mean(), spec.max())
                img_grid = torchvision.utils.make_grid([static_diff / static_diff.mean() * pr.mean(), static_spec / static_diff.mean() * pr.mean(), diff, spec, pr, gtcpu, difference])
                img_grid = img_grid / gtcpu.max()
                img_grid = img_grid ** (1.0 / 2.2)
                img_grid = torch.clip(1.2 * img_grid, 0.0, 1.0)

                writer.add_image('static vs predictions vs. GT', img_grid, global_step=iteration)

    writer.add_scalar('Epoch loss (training)', epoch_loss, epoch)

    with torch.no_grad():  # Loop over the test data:
        val_loss = 0.0

        mape_loss = 0.0
        rmse_loss = 0.0

        for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in val_dataloader:
            pred_render, groundtruth, sharpprior, _ = render_from_buffers(alpha_batch[0], pos3d_batch[0],
                                                                          normals_batch[0], wi_batch[0],
                                                                          diffuse_bsdf_val_batch[0],
                                                                          specular_bsdf_val_batch[0], gtcpu_batch[0],
                                                                          envmap_batch[0].permute(2, 0, 1),
                                                                          gpudevice, net,
                                                                          timings=False)
            loss = sgr.my_loss(pred_render, groundtruth.to(gpudevice), tonemap=False, clip=False, log=logloss,
                               L2=L2loss)
            val_loss += loss.item() / len(val_dataloader)

            mape_loss += sgr.mape(pred_render, groundtruth.to(gpudevice)) / len(val_dataloader)
            rmse_loss += sgr.rmse(pred_render, groundtruth.to(gpudevice)) / len(val_dataloader)

        writer.add_scalar('Epoch loss (validation)', val_loss, epoch)
        txtlog.write("Epoch loss : " + str(val_loss) + " for epoch " + str(epoch))
        txtlog.write("  ========\n")

        writer.add_scalar('MAPE ', mape_loss, epoch)
        txtlog.write("MAPE loss : " + str(mape_loss) + " for epoch " + str(epoch))
        txtlog.write("  ========\n")

        writer.add_scalar('RMSE ', rmse_loss, epoch)
        txtlog.write("RMSE loss : " + str(rmse_loss) + " for epoch " + str(epoch))
        txtlog.write("  ========\n")
        print('epoch: ', epoch)
        # TODO : save network and run it on the high-res testing images

        if (epoch == 1) or (epoch == 2) or (epoch == 4) or (epoch == 8) or (epoch == 16) or (epoch == 32):
            torch.save(net.state_dict(), outputpath + "/" + str(epoch).zfill(4) + "-" + experiment_name + "-net.pth")

            if not os.path.exists(outputpath + "/" + experiment_name):
                os.makedirs(outputpath + "/" + experiment_name)

        if (epoch+1) % 50 == 0:
            torch.save(net.state_dict(), outputpath + "/" + str(epoch).zfill(4) + "-" + experiment_name + "-net.pth")

            imcount = 0
            print("rendering highres")
            for alpha_batch, pos3d_batch, normals_batch, wi_batch, diffuse_bsdf_val_batch, specular_bsdf_val_batch, gtcpu_batch, envmap_batch in dataloader:
                # plt.imshow(gtcpu_batch[0])
                # plt.show()
                gtenv = gtcpu_batch[0].clone()  # * envmask  # .to(gpudevice)

                pred_render, groundtruth, sharpprior, _ = render_from_buffers(alpha_batch[0], pos3d_batch[0],
                                                                              normals_batch[0], wi_batch[0],
                                                                              diffuse_bsdf_val_batch[0],
                                                                              specular_bsdf_val_batch[0],
                                                                              gtcpu_batch[0],
                                                                              envmap_batch[0].permute(2, 0, 1),
                                                                              gpudevice, net,
                                                                              timings=False)
                alpha = alpha_batch[0]  # .to(gpudevice)
                pos3d = pos3d_batch[0]  # .to(gpudevice)
                normals = normals_batch[0]  # .to(gpudevice)
                wi = wi_batch[0]  # .to(gpudevice)
                diffuse_bsdf_val = diffuse_bsdf_val_batch[0]  # .to(gpudevice)
                specular_bsdf_val = specular_bsdf_val_batch[0]  # .to(gpudevice)

                envmap = envmap_batch[0].permute(2, 0, 1)
                alpha = alpha[:, :, 0].view(-1)
                mask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=False)) > 0.0).view(-1)
                envmask = (torch.sqrt(torch.sum(normals ** 2, dim=-1, keepdim=True)) < 0.5).expand(-1, -1, 3)
                gtenv = gtenv * envmask  # .to(gpudevice)

                pr = torch.zeros(gtcpu_batch[0].size())
                pr = pr.view(-1, 3)
                pr[mask, :] = pred_render.cpu()
                pr = pr.view(gtcpu_batch[0].shape)
                # print(pr.shape, gtenv.shape)
                pr = pr + gtenv

                imageio.imwrite(outputpath + "/" + experiment_name + "/" + str(imcount).zfill(8) + ".exr",
                                pr.cpu().numpy())
                pr = pr.permute(2, 0, 1)
                pr = torch.clip(multiplier * pr, 0.0, 1.0)
                pr = pr ** (1.0 / 2.2)
                save_image(pr, outputpath + "/" + experiment_name + "/" + str(imcount).zfill(8) + ".png")
                imcount = imcount + 1

txtlog.close()
print('Finished Training')