# Disclaimer: This file contains a lot of architectures that were tried, only a small fraction are actually used in the final results

import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import numpy as np
# import OpenEXR
# import Imath
import torch
import matplotlib.pyplot as plt
from collections import OrderedDict
import torch.nn.functional as F
import torchvision.models as models
import einops
import siren_pytorch as siren
import renderhelpers as sgr

class noNet(nn.Module):
    def __init__(self, numSGs):
        super(noNet, self).__init__()
        self.sgcodes = torch.zeros([numSGs, 6]).cuda().requires_grad_(True)

    def forward(self):
        return self.sgcodes


class posNet(nn.Module):
    def __init__(self, insize, outsize, width, num_layers):
        super(posNet, self).__init__()
        self.width = width
        self.num_layers = num_layers

        layers = [nn.Linear(insize, width)]  # , nn.ReLU()]
        for i in range(num_layers - 1):
            layers.append(nn.Linear(width, width))
        self.lastlayer = nn.Linear(width, outsize, bias=True)
        self.module_list = nn.ModuleList(layers)
        self.activate = nn.ReLU()

    def forward(self, x):
        for f in self.module_list:
            x = self.activate(f(x))
        return self.lastlayer(x)


class skipNet(nn.Module):
    def __init__(self, numSGs, width, num_layers):
        super(posNet, self).__init__()
        self.numSGs = numSGs
        self.width = width
        self.num_layers = num_layers

        layers = [nn.Linear(3, width)]  # , nn.ReLU()]
        for i in range(num_layers - 1):
            layers.append(nn.Linear(width + 3, width))
            # layers.append(nn.ReLU())
        # layers.append(nn.Linear(width, 6 * self.numSGs))
        self.lastlayer = nn.Linear(width + 3, 6 * self.numSGs)
        self.module_list = nn.ModuleList(layers)
        self.activate = nn.ReLU()

    def forward(self, x):
        encx = x.clone()
        for f in self.module_list:
            encx = torch.cat([x, self.activate(f(encx))], 1)
        # return encx
        return self.lastlayer(encx)

        # for f in self.module_list:
        #     x = f(x)
        # return x


# From TinyNERF colab notebook
def positional_encoding(tensor, num_encoding_functions=6, include_input=True, log_sampling=True) -> torch.Tensor:
    r"""Apply positional encoding to the input.
    Args:
      tensor (torch.Tensor): Input tensor to be positionally encoded.
      num_encoding_functions (optional, int): Number of encoding functions used to
          compute a positional encoding (default: 6).
      include_input (optional, bool): Whether or not to include the input in the
          computed positional encoding (default: True).
      log_sampling (optional, bool): Sample logarithmically in frequency space, as
          opposed to linearly (default: True).
    Returns:
      (torch.Tensor): Positional encoding of the input tensor.
    """
    # TESTED
    # Trivially, the input tensor is added to the positional encoding.
    encoding = [tensor] if include_input else []
    # Now, encode the input using a set of high-frequency functions and append the
    # resulting values to the encoding.
    frequency_bands = None
    if log_sampling:
        frequency_bands = 2.0 ** torch.linspace(
            0.0,
            num_encoding_functions - 1,
            num_encoding_functions,
            dtype=tensor.dtype,
            device=tensor.device,
        )
    else:
        frequency_bands = torch.linspace(
            2.0 ** 0.0,
            2.0 ** (num_encoding_functions - 1),
            num_encoding_functions,
            dtype=tensor.dtype,
            device=tensor.device,
        )

    for freq in frequency_bands:
        for func in [torch.sin, torch.cos]:
            encoding.append(func(tensor * freq))

    # Special case, for no positional encoding
    if len(encoding) == 1:
        return encoding[0]
    else:
        return torch.cat(encoding, dim=-1)


class DenseLayer(nn.Linear):
    def __init__(self, in_dim: int, out_dim: int, activation: str = "relu", *args, **kwargs) -> None:
        self.activation = activation
        super().__init__(in_dim, out_dim, *args, **kwargs)

    def reset_parameters(self) -> None:
        torch.nn.init.xavier_uniform_(self.weight, gain=torch.nn.init.calculate_gain(self.activation))
        # torch.nn.init.kaiming_uniform_(self.weight, a=math.sqrt(5))
        if self.bias is not None:
            torch.nn.init.zeros_(self.bias)


class posEncodingNet(nn.Module):
    def __init__(self, numSGs, width, num_layers, num_freqs=6):
        super(posEncodingNet, self).__init__()
        self.numSGs = numSGs
        self.width = width
        self.num_layers = num_layers
        self.num_freqs = num_freqs

        inpsize = 3 + 3 * 2 * self.num_freqs
        layers = [DenseLayer(inpsize, self.width, activation="relu")]
        # layers = [DenseLayer(3 + 3 * 2 * self.num_freqs, self.width, activation="relu"), nn.ReLU()]
        for i in range(num_layers - 1):
            layers.append(DenseLayer(self.width, self.width, activation="relu"))
            # layers.append(nn.ReLU())
            # layers.append(nn.LeakyReLU())

        self.lastlayer = DenseLayer(self.width, 6 * self.numSGs, activation="linear")
        # layers.append(DenseLayer(self.width + 3 * 2 * self.num_freqs, 6 * self.numSGs, activation="linear"))
        self.module_list = nn.ModuleList(layers)

        self.activate = nn.LeakyReLU()  # nn.ReLU() #

    def forward(self, x):
        encx = positional_encoding(x, num_encoding_functions=self.num_freqs)
        # encx = encx0
        for f in self.module_list:
            encx = self.activate(f(encx))
            # encx = torch.cat([positional_encoding(x, num_encoding_functions=self.num_freqs), self.activate(f(encx))], 1)
        # return encx
        return self.lastlayer(encx)


class SineLayer(nn.Module):
    # See paper sec. 3.2, final paragraph, and supplement Sec. 1.5 for discussion of omega_0.

    # If is_first=True, omega_0 is a frequency factor which simply multiplies the activations before the
    # nonlinearity. Different signals may require different omega_0 in the first layer - this is a
    # hyperparameter.

    # If is_first=False, then the weights will be divided by omega_0 so as to keep the magnitude of
    # activations constant, but boost gradients to the weight matrix (see supplement Sec. 1.5)

    def __init__(self, in_features, out_features, bias=True,
                 is_first=False, omega_0=30):
        super().__init__()
        self.omega_0 = omega_0
        self.is_first = is_first

        self.in_features = in_features
        self.linear = nn.Linear(in_features, out_features, bias=bias)

        self.init_weights()

    def init_weights(self):
        with torch.no_grad():
            if self.is_first:
                self.linear.weight.uniform_(-1 / self.in_features,
                                            1 / self.in_features)
            else:
                self.linear.weight.uniform_(-np.sqrt(6 / self.in_features) / self.omega_0,
                                            np.sqrt(6 / self.in_features) / self.omega_0)

    def forward(self, input):
        return torch.sin(self.omega_0 * self.linear(input))


class Siren(nn.Module):
    def __init__(self, in_features, hidden_features, hidden_layers, out_features, outermost_linear=False,
                 first_omega_0=30, hidden_omega_0=30.):
        super().__init__()

        self.net = []
        self.net.append(SineLayer(in_features, hidden_features,
                                  is_first=True, omega_0=first_omega_0))

        for i in range(hidden_layers):
            self.net.append(SineLayer(hidden_features, hidden_features,
                                      is_first=False, omega_0=hidden_omega_0))

        if outermost_linear:
            final_linear = nn.Linear(hidden_features, out_features)

            with torch.no_grad():
                final_linear.weight.uniform_(-np.sqrt(6 / hidden_features) / hidden_omega_0,
                                             np.sqrt(6 / hidden_features) / hidden_omega_0)

            self.net.append(final_linear)
        else:
            self.net.append(SineLayer(hidden_features, out_features,
                                      is_first=False, omega_0=hidden_omega_0))

        self.net = nn.Sequential(*self.net)

    def forward(self, coords):
        # coords = coords.clone().detach().requires_grad_(True)  # allows to take derivative w.r.t. input
        # output = self.net(coords)
        # return output, coords
        return self.net(coords)


class BasicConv(nn.Module):
    def __init__(self, in_channels, out_channels, kernel, stride, batchnorm=False, skipconnect=False,
                 nonlinearity=None):
        super(BasicConv, self).__init__()
        self.norm = batchnorm
        if self.norm:
            self.bn = nn.BatchNorm2d(out_channels)
        self.skip = skipconnect
        self.nonlin = True
        if nonlinearity is None:
            self.nonlin = False
        if nonlinearity == 'leaky':
            self.activate = nn.LeakyReLU()
        if nonlinearity == 'relu':
            self.activate = nn.ReLU()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel, stride=stride, padding=1)

    def forward(self, x):
        out = self.conv(x)
        if self.norm:
            out = self.bn(out)
        if self.nonlin:
            out = self.activate(out)
        if self.skip:
            return out + x
        return out


class BasicDeconv(nn.Module):
    def __init__(self, in_channels, out_channels, kernel, stride, batchnorm=False, skipconnect=False, nonlinearity=None,
                 coordconv=False):
        super(BasicDeconv, self).__init__()
        self.norm = batchnorm
        if self.norm:
            self.bn = nn.BatchNorm2d(out_channels)
        self.skip = skipconnect
        self.upsample = nn.UpsamplingBilinear2d(scale_factor=2)
        self.nonlin = True
        if nonlinearity is None:
            self.nonlin = False
        if nonlinearity == 'leaky':
            self.activate = nn.LeakyReLU()
        if nonlinearity == 'relu':
            self.activate = nn.ReLU()

        if coordconv:
            self.conv = CoordConvTranspose(in_channels, out_channels, kernel, stride=stride, padding=1, with_r=True)
        else:
            self.conv = nn.ConvTranspose2d(in_channels, out_channels, kernel, stride=stride, padding=1)

    def forward(self, x):
        out = self.conv(x)
        if self.norm:
            out = self.bn(out)
        if self.nonlin:
            out = self.activate(out)
        if self.skip:
            # print(out.shape, self.upsample(x).shape)
            return out + self.upsample(x)
        return out


class MyBasicResNetBottleneck(nn.Module):
    def __init__(self, in_channels, out_channels, nonlinearity='relu'):
        super(MyBasicResNetBottleneck, self).__init__()
        self.layer1 = BasicConv(in_channels, out_channels, 3, 1, batchnorm=True, skipconnect=False, nonlinearity='relu')
        self.layer2 = BasicConv(out_channels, out_channels, 3, 2, batchnorm=True)
        self.downsample = nn.MaxPool2d(2, 2)
        self.activate = None
        if nonlinearity == 'leaky':
            self.activate = nn.LeakyReLU()
        if nonlinearity == 'relu':
            self.activate = nn.ReLU()

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out) + self.downsample(x)
        if self.activate is not None:
            return self.activate(out)
        return out


class MyBasicResNetDebottleneck(nn.Module):
    def __init__(self, in_channels, out_channels, nonlinearity='relu'):
        super(MyBasicResNetDebottleneck, self).__init__()
        self.layer1 = BasicDeconv(in_channels, out_channels, 3, 1, batchnorm=True, skipconnect=False,
                                  nonlinearity='relu')
        self.layer2 = BasicDeconv(out_channels, out_channels, 4, 2, batchnorm=True)
        self.upsample = nn.UpsamplingBilinear2d(scale_factor=2)
        self.activate = None
        if nonlinearity == 'leaky':
            self.activate = nn.LeakyReLU()
        if nonlinearity == 'relu':
            self.activate = nn.ReLU()

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out) + self.upsample(x)
        if self.activate is not None:
            return self.activate(out)
        return out


class EnvmapAE(nn.Module):
    def __init__(self, cnndim, latentdim):
        super(EnvmapAE, self).__init__()
        self.latentdim = latentdim
        self.cnndim = cnndim
        chans = 16  # 64

        # self.encoder_cnn = nn.Sequential(
        #     BasicConv(3, chans, 3, 2, batchnorm=False, skipconnect=False, nonlinearity='relu'),
        #     BasicConv(chans, chans, 3, 2, batchnorm=False, skipconnect=False, nonlinearity='relu'),
        #     BasicConv(chans, chans, 3, 2, batchnorm=False, skipconnect=False, nonlinearity='relu'),
        #     BasicConv(chans, chans, 3, 2, batchnorm=False, skipconnect=False, nonlinearity='relu'),
        #     BasicConv(chans, chans, 3, 2, batchnorm=False, skipconnect=False, nonlinearity='relu'))
        # self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)

        self.encoder_cnn = nn.Sequential(
            nn.Conv2d(3, chans, 3, stride=1, padding=1),
            nn.LeakyReLU(), nn.MaxPool2d(2, 2),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='leaky', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='leaky', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='leaky', maxpool=True, coordconv=False))
        # self.encoder_cnn = nn.Sequential(
        #     BasicConv(3, chans, 3, 2, batchnorm=False, skipconnect=False, nonlinearity='relu'),
        #     BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'),
        #     BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'),
        #     # BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'),
        #     BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'))
        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)

        self.decoder_fc = nn.Linear(self.latentdim, self.cnndim)
        self.decoder_cnn = nn.Sequential(
            BasicDeconv(chans, chans, 4, 2, batchnorm=False, skipconnect=True, nonlinearity='leaky', coordconv=False),
            BasicDeconv(chans, chans, 4, 2, batchnorm=False, skipconnect=True, nonlinearity='leaky', coordconv=False),
            BasicDeconv(chans, chans, 4, 2, batchnorm=False, skipconnect=True, nonlinearity='leaky', coordconv=False),
            BasicDeconv(chans, chans, 4, 2, batchnorm=False, skipconnect=True, nonlinearity='leaky', coordconv=False),
            # BasicDeconv(chans, chans, 4, 2, batchnorm=False, skipconnect=True, nonlinearity='relu'),
            nn.ConvTranspose2d(chans, 3, 3, stride=1, padding=1))
        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
        dec_params = sum(p.numel() for p in self.decoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.decoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the decoder : %d" % dec_params)

    def forward(self, x):
        x = self.encoder_cnn(x)
        cnnshape = x.shape
        # print(x.shape)
        x = self.encoder_fc(x.view(-1, self.cnndim))
        # print(x.shape)
        x = self.decoder_fc(x).view(cnnshape)
        # print(x.shape)
        return self.decoder_cnn(x)

    def encode(self, x):
        return self.encoder_fc(self.encoder_cnn(x))

class BasicConvSkip(nn.Module):
    def __init__(self, in_channels, out_channels, kernel, batchnorm=False, nonlinearity=None, maxpool=False,
                 coordconv=False):
        super(BasicConvSkip, self).__init__()
        self.norm = batchnorm
        if self.norm:
            self.bn = nn.BatchNorm2d(out_channels)
        self.nonlin = True
        if nonlinearity is None:
            self.nonlin = False
        if nonlinearity == 'leaky':
            self.activate = nn.LeakyReLU()
        if nonlinearity == 'relu':
            self.activate = nn.ReLU()
        self.coordconv = coordconv
        if coordconv:
            # self.conv = CoordConv(in_channels, out_channels, kernel, stride=2, padding=1, with_r=True)
            self.conv = nn.Conv2d(in_channels+3, out_channels, kernel, stride=1, padding=1)
        else:
            self.conv = nn.Conv2d(in_channels, out_channels, kernel, stride=1, padding=1)
            # self.conv = nn.Conv2d(in_channels, out_channels, kernel, stride=2, padding=1)

        if maxpool:
            self.downsample = nn.MaxPool2d(2, 2)  # nn.AvgPool2d(2,2)#         #
        else:
            self.downsample = nn.AvgPool2d(2, 2)  # #
        self.avgpool = nn.AvgPool2d(2, 2)  # #

    def forward(self, x):
        xdown = self.downsample(x)
        if self.coordconv:
            # print(x.shape, 'x')
            coords = sgr.create_neural_coord_grid(x.shape[2], x.shape[3]).unsqueeze(0).repeat(x.shape[0], 1, 1, 1)
            # print(coords.shape, 'coord')
            x = torch.cat((coords.to(x.device), x), dim=1)
            # print(x.shape, 'x')
            out = self.downsample(self.conv(x))
        else:
            # out = self.conv(x)
            out = self.downsample(self.conv(x))
        if self.norm:
            out = self.bn(out)
        if self.nonlin:
            out = self.activate(out)
        return out + xdown


class BasicConvSkip2(nn.Module):
    def __init__(self, channels, kernel, maxpool=False, coordconv=False):
        super(BasicConvSkip2, self).__init__()
        self.nonlin = True
        self.activate = nn.LeakyReLU()

        if coordconv:
            self.conv = CoordConv(channels, channels, kernel, stride=1, padding=1, with_r=True)
        else:
            self.conv = nn.Conv2d(channels, channels, kernel, stride=1, padding=1)
        self.pool = nn.MaxPool2d(2, 2)

        if maxpool:
            self.downsample = nn.MaxPool2d(2, 2)  # nn.AvgPool2d(2,2)#         #
        else:
            self.downsample = nn.AvgPool2d(2, 2)  # #

    def forward(self, x):
        out = self.conv(x)
        if self.nonlin:
            out = self.activate(self.pool(out))
        return torch.cat((out, self.downsample(x)), dim=1)

class RenderingNet(nn.Module):
    def __init__(self, cnndim, latentdim, numSGs, inpsize, width, num_layers, num_freqs=6,
                 alternate_parametrization=False):
        super(RenderingNet, self).__init__()
        self.latentdim = latentdim
        self.cnndim = cnndim
        self.numSGs = numSGs
        self.width = width
        self.num_layers = num_layers
        self.num_freqs = num_freqs

        chans = 16  # 3# 16

        # self.encoder_cnn = nn.Sequential(
        #     BasicConv(3, chans, 3, 2, batchnorm=False, skipconnect=False, nonlinearity='relu'),
        #     BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'),
        #     BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'),
        #     # BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'),
        #     BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu'))
        self.encoder_cnn = nn.Sequential(
            nn.Conv2d(3, chans, 3, stride=1, padding=1),
            nn.ReLU(), nn.MaxPool2d(2, 2),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False))
        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)

        # inpsize = 3 + 3 * 2 * self.num_freqs
        print('positional net input size ', inpsize)
        self.activate = nn.ReLU()  # nn.LeakyReLU()#
        layers = [nn.Linear(inpsize + self.latentdim, self.width), self.activate]
        for i in range(num_layers - 1):
            layers.append(nn.Linear(self.width, self.width))
            layers.append(self.activate)

        for l in layers:
            if isinstance(l, nn.Linear):
                torch.nn.init.xavier_uniform_(l.weight, gain=torch.nn.init.calculate_gain('relu'))
                torch.nn.init.zeros_(l.bias)

        outdim = 6 * self.numSGs
        if alternate_parametrization:
            outdim = 7 * self.numSGs
        layers.append(nn.Linear(self.width, outdim, bias=False))

        # self.refinenet = nn.Sequential(nn.Linear(6 * self.numSGs+self.latentdim,6 * self.numSGs), nn.ReLU(), nn.Linear(6 * self.numSGs,6 * self.numSGs))

        self.posnet = nn.Sequential(*layers)
        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
        dec_params = sum(p.numel() for p in self.posnet.parameters() if p.requires_grad)
        print("Number of parameters in the decoder : %d" % dec_params)

    def forward(self, envmap, x):
        encx = x  # positional_encoding(x, num_encoding_functions=self.num_freqs)

        x1 = self.encoder_cnn(envmap)
        # print('x1 shape ', x1.shape)
        x1 = x1.view(-1, self.cnndim)
        # print('x1 shape2 ', x1.shape)
        x2 = self.encoder_fc(x1)
        # print(encx.shape, x2.shape)
        x2 = x2.expand(encx.shape[0], -1)
        # print(encx.shape, x2.shape)
        return self.posnet(torch.cat((encx, x2), dim=1))
        # x3 = torch.cat((self.posnet(encx), x2), dim=1)
        # return self.refinenet(x3)

class CoordEnvmapSGencoder(nn.Module):
    def __init__(self, cnndim, latentdim):
        super(CoordEnvmapSGencoder, self).__init__()
        self.latentdim = latentdim
        self.cnndim = cnndim
        chans = 32#32# 4  # 3# 16
        # self.encoder_cnn = models.resnet18()
        # self.encoder_fc = nn.Linear(1000, latentdim)
        self.encoder_cnn = nn.Sequential(
            nn.Conv2d(3, chans, 3, stride=1, padding=1), nn.ReLU(), nn.MaxPool2d(2, 2),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=True),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=True),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=True),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=True))
        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)

        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
        # torch.nn.init.xavier_uniform_(self.encoder_fc.weight, gain=torch.nn.init.calculate_gain('relu'))
        # torch.nn.init.zeros_(self.encoder_fc.bias)

    def forward(self, envmap):
        x1 = self.encoder_cnn(envmap)
        # print('x1 shape ', x1.shape)
        x1 = x1.view(-1, self.cnndim)
        # print('x1 shape2 ', x1.shape)
        return self.encoder_fc(x1)

class EnvmapSGencoder(nn.Module):
    def __init__(self, cnndim, latentdim):
        super(EnvmapSGencoder, self).__init__()
        self.latentdim = latentdim
        self.cnndim = cnndim
        chans = 32#32# 4  # 3# 16
        #
        # self.encoder_cnn = models.resnet18()
        # self.encoder_fc = nn.Linear(1000, latentdim)

        # self.encoder_cnn = nn.Sequential(
        #     nn.Conv2d(3, chans, 3, stride=1, padding=1),
        #     nn.ReLU(), nn.MaxPool2d(2, 2),
        #     BasicConvSkip2(chans, 3, maxpool=False, coordconv=False),
        #     BasicConvSkip2(chans *2, 3, maxpool=False, coordconv=False),
        #     BasicConvSkip2(chans *4, 3, maxpool=False, coordconv=False) )
        # self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)
        self.encoder_cnn = nn.Sequential(
            nn.Conv2d(3, chans, 3, stride=1, padding=1), nn.ReLU(), nn.MaxPool2d(2, 2),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            # BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False))
        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim)

        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
        # torch.nn.init.xavier_uniform_(self.encoder_fc.weight, gain=torch.nn.init.calculate_gain('relu'))
        # torch.nn.init.zeros_(self.encoder_fc.bias)

    def forward(self, envmap):
        x1 = self.encoder_cnn(envmap)
        # print('x1 shape ', x1.shape)
        x1 = x1.view(1, self.cnndim)
        # print('x1 shape2 ', x1.shape)
        return self.encoder_fc(x1)

        # return self.encoder_fc(self.encoder_cnn(envmap))


class DirectionalNet(nn.Module):
    def __init__(self, insize, numSGs, width, num_layers, num_freqs=6, skip=False):
        super(DirectionalNet, self).__init__()
        self.numSGs = numSGs
        self.width = width
        self.num_layers = num_layers
        self.num_freqs = num_freqs
        self.activate = nn.ReLU()  # nn.LeakyReLU()  #
        self.skip = skip

        inpsize = insize + insize * 2 * self.num_freqs
        layers = [nn.Linear(inpsize, self.width)]
        for i in range(num_layers - 1):
            if self.skip:
                layers.append(nn.Linear(self.width + insize, self.width))
            else:
                layers.append(nn.Linear(self.width, self.width))

        if self.skip:
            self.lastlayer = DenseLayer(self.width + insize, 6 * self.numSGs)
        else:
            self.lastlayer = DenseLayer(self.width, 6 * self.numSGs)
        self.module_list = nn.ModuleList(layers)

        # NeRF initialisation
        for l in self.module_list:
            torch.nn.init.xavier_uniform_(l.weight, gain=torch.nn.init.calculate_gain('relu'))
            torch.nn.init.zeros_(l.bias)
        torch.nn.init.xavier_uniform_(self.lastlayer.weight, gain=torch.nn.init.calculate_gain('linear'))
        torch.nn.init.zeros_(self.lastlayer.bias)

    def forward(self, x):
        if self.num_freqs > 0:
            encx = positional_encoding(x, num_encoding_functions=self.num_freqs)
        else:
            encx = x
        for f in self.module_list:
            encx = self.activate(f(encx))
            if self.skip:
                encx = torch.cat([encx, x], dim=1)
        return self.lastlayer(encx)


class ImageEncoder(nn.Module):
    def __init__(self, cnndim, latentdim):
        super(ImageEncoder, self).__init__()
        self.latentdim = latentdim
        self.cnndim = cnndim
        chans = 16  # 3# 16
        self.encoder_cnn = nn.Sequential(
            nn.Conv2d(3, chans, 3, stride=1, padding=1),
            nn.ReLU(), nn.MaxPool2d(2, 2),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False),
            BasicConvSkip(chans, chans, 3, batchnorm=False, nonlinearity='relu', maxpool=True, coordconv=False))

        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim, bias=False)

        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
    def forward(self, envmap):
        x1 = self.encoder_cnn(envmap) # print('x1 shape ', x1.shape)
        x1 = x1.view(-1, self.cnndim) # print('x1 shape2 ', x1.shape)
        x2 = self.encoder_fc(x1)
        return x2

class BiggerImageEncoder(nn.Module):
    def __init__(self, cnndim, latentdim):
        super(BiggerImageEncoder, self).__init__()
        self.latentdim = latentdim
        self.cnndim = cnndim
        self.cnnactiv = nn.ReLU()
        self.cnnpool = nn.MaxPool2d(2, 2)
        self.downsample = nn.AvgPool2d(2, 2)

        self.encoder_cnn = torch.nn.ModuleList(
            [ nn.Conv2d(3, 5, 3, stride=1, padding=1),
            nn.Conv2d(8, 13, 3, stride=1, padding=1),
            nn.Conv2d(16, 29, 3, stride=1, padding=1),
            nn.Conv2d(32, 45, 3, stride=1, padding=1),
            nn.Conv2d(48, 61, 3, stride=1, padding=1) ])
        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim, bias=False)

        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
    def forward(self, envmap):
        x = envmap
        xd = envmap
        for l in self.encoder_cnn:
            xd = self.downsample(xd)
            x = torch.cat( (self.cnnpool(self.cnnactiv( l(x) )), xd), dim=-3)
        x1 = x.view(-1, self.cnndim) # print('x1 shape2 ', x1.shape)
        x2 = self.encoder_fc(x1)
        return x2

class RGBEncoder(nn.Module):
    def __init__(self, latentdim):
        super(RGBEncoder, self).__init__()
        self.latentdim = latentdim
        self.cnndim = 8192
        self.cnnactiv = nn.ReLU()
        self.cnnpool = nn.MaxPool2d(2, 2)
        self.downsample = nn.AvgPool2d(2, 2)

        self.encoder_cnn = torch.nn.ModuleList(
            [ nn.Conv2d(3, 5, 3, stride=1, padding=1),
            nn.Conv2d(8, 13, 3, stride=1, padding=1),
            # nn.Conv2d(16, 13, 3, stride=1, padding=1),
            nn.Conv2d(16, 13, 3, stride=1, padding=1)])
        self.encoder_fc = nn.Linear(self.cnndim, self.latentdim, bias=False)

        # self.centers = nn.Parameter(torch.zeros( int(latentdim/3), 3, requires_grad=True).cuda())

        par = torch.tensor([0.0, 0.0, 2.0]) #theta, phi, sharpness
        # self.centers = nn.Parameter(torch.autograd.Variable(par.unsqueeze(0).expand( int(latentdim/3), -1), requires_grad=True).cuda())
        self.centers = par.unsqueeze(0).expand( int(latentdim/3), -1).cuda()

        enc_params = sum(p.numel() for p in self.encoder_fc.parameters() if p.requires_grad) + sum(
            p.numel() for p in self.encoder_cnn.parameters() if p.requires_grad)
        print("Number of parameters in the encoder : %d" % enc_params)
    def forward(self, envmap):
        x = envmap
        xd = envmap
        for l in self.encoder_cnn:
            xd = self.downsample(xd)
            x = torch.cat( (self.cnnpool(self.cnnactiv( l(x) )), xd), dim=-3)
        x1 = x.view(-1, self.cnndim) # print('x1 shape2 ', x1.shape)
        x2 = self.encoder_fc(x1).unsqueeze(2).view(-1, int(self.latentdim/3), 3)

        # sgdirs0 = torch.cat( (self.centers[:,0:1], 1000.0 * self.centers[:,1:3]), dim=1)
        # sgdirs = sgdirs0.unsqueeze(0).expand(x2.shape)
        sgdirs = self.centers.unsqueeze(0).expand(x2.shape) #* 100.0
        x3 = torch.cat( (x2, sgdirs), dim=2)
        return x3.view(-1, 2*self.latentdim)

class PositionSGDecoder(nn.Module):
    def __init__(self, latentdim, numSGs, inpsize, width, num_layers, lightinputdepth=0,
                 alternate_parametrization=False, outdim=None):
        super(PositionSGDecoder, self).__init__()
        self.latentdim = latentdim
        self.numSGs = numSGs
        self.width = width
        self.num_layers = num_layers
        self.lightinputdepth = lightinputdepth
        print('lightinputdepth = ', lightinputdepth)
        self.activate = nn.ReLU()  # nn.LeakyReLU()#

        self.skipconnections = False

        self.layers = torch.nn.ModuleList()
        if self.lightinputdepth == 0:
            self.layers.append(torch.nn.Linear(inpsize + self.latentdim, self.width))
        else:
            self.layers.append(torch.nn.Linear(inpsize, self.width))

        for i in range(num_layers - 1):
            if (i + 1) == self.lightinputdepth:
                self.layers.append(nn.Linear(self.width + self.latentdim + self.skipconnections * inpsize, self.width))
            else:
                self.layers.append(nn.Linear(self.width + self.skipconnections * inpsize, self.width))

        outdim = 6 * self.numSGs
        if alternate_parametrization:
            outdim = 7 * self.numSGs

        self.layers.append(nn.Linear(self.width + self.skipconnections * inpsize, outdim, bias=True))

        for ll in self.layers:
            if isinstance(ll, nn.Linear):
                torch.nn.init.xavier_uniform_(ll.weight, gain=torch.nn.init.calculate_gain('relu'))
                torch.nn.init.zeros_(ll.bias)
        # self.layers.append(nn.Linear(self.width + self.latentdim + self.skipconnections * inpsize, outdim, bias=False))

    def forward(self, lightcode, pos):
        lat = lightcode.expand(pos.shape[0], -1)
        x = pos
        for i, l in enumerate(self.layers):
            if i > 0:
                x = self.activate(x)
                if self.skipconnections:  # Try skips
                    x = torch.cat((pos, x), dim=1)

            if i == self.lightinputdepth:
                x = torch.cat((lat, x), dim=1)
            x = l(x)
        return x

class CompleteNet(nn.Module):
    def __init__(self, cnndim, latentdim, numSGs, inpsize, width, num_layers, num_freqs=6, lightinputdepth=0,
                 alternate_parametrization=False):
        super(CompleteNet, self).__init__()
        self.encoder = ImageEncoder(cnndim, latentdim)
        self.decoder = PositionSGDecoder(latentdim, numSGs, inpsize, width, num_layers, lightinputdepth, alternate_parametrization)
        self.sgdim = 6
        if alternate_parametrization:
            self.sgdim = 7
        self.numSGs = numSGs

    def forward(self, envmap, pos):
        return self.decoder(self.encoder(envmap), pos).unsqueeze(2).view(-1, self.numSGs, self.sgdim)

class StaticNet(nn.Module):
    def __init__(self, numSGs, inpsize, width, num_layers, alternate_parametrization=False):
        super(StaticNet, self).__init__()
        self.numSGs = numSGs
        self.width = width
        self.num_layers = num_layers
        self.activate = nn.ReLU()  # nn.LeakyReLU()#

        self.skipconnections = True

        self.layers = torch.nn.ModuleList()
        self.layers.append(torch.nn.Linear(inpsize, self.width))

        for i in range(num_layers - 1):
            self.layers.append(nn.Linear(self.width + self.skipconnections * inpsize, self.width))

        outdim = 6 * self.numSGs
        if alternate_parametrization:
            outdim = 7 * self.numSGs

        for ll in self.layers:
            if isinstance(ll, nn.Linear):
                torch.nn.init.xavier_uniform_(ll.weight, gain=torch.nn.init.calculate_gain('relu'))
                torch.nn.init.zeros_(ll.bias)

        self.layers.append(nn.Linear(self.width + self.skipconnections * inpsize, outdim, bias=False ))

    def forward(self, lightcode, pos):
        x = pos
        for i, l in enumerate(self.layers):
            if i > 0:
                # Try skips
                if self.skipconnections:
                    x = torch.cat((pos, x), dim=1)

                x = self.activate(x)

            x = l(x)
        return x

class VisibilityNet(nn.Module): # TODO: removed bias at the end
    def __init__(self, outdim, inpsize, width, num_layers, nerfinit=True):
        super(VisibilityNet, self).__init__()
        self.width = width
        self.num_layers = num_layers
        self.activate = nn.ReLU()  # nn.LeakyReLU()#
        self.outdim = outdim

        self.skipconnections = False

        self.layers = torch.nn.ModuleList()
        self.layers.append(torch.nn.Linear(inpsize, self.width))
        for i in range(num_layers - 1):
            self.layers.append(nn.Linear(self.width + self.skipconnections * inpsize, self.width))

        if nerfinit:
            self.layers.append(nn.Linear(self.width + self.skipconnections * inpsize, self.outdim, bias=True))
            for ll in self.layers:
                if isinstance(ll, nn.Linear):
                    torch.nn.init.xavier_uniform_(ll.weight, gain=torch.nn.init.calculate_gain('relu'))
                    torch.nn.init.zeros_(ll.bias)

        else:
            self.layers.append(nn.Linear(self.width + self.skipconnections * inpsize, self.outdim, bias=False))


    def forward(self, pos):
        x = pos
        for i, l in enumerate(self.layers):
            if i > 0:
                x = self.activate(x)
                if self.skipconnections:# Try skips
                    x = torch.cat((pos, x), dim=-1)
            x = l(x)
        return x

class SGTransportNet(nn.Module):
    def __init__(self, cnndim, numLightSGs, numSceneSGs, inpsize, width, num_layers, alternate_parametrization=False):
        super(SGTransportNet, self).__init__()
        self.alternate_parametrization = alternate_parametrization

        # // Envmap to SG encoder
        if alternate_parametrization:
            self.lightSGs = BiggerImageEncoder(cnndim, 7 * numLightSGs)
        else:
            # self.lightSGs = BiggerImageEncoder(cnndim, 6 * numLightSGs)
            self.lightSGs = BiggerImageEncoder(cnndim, 3 * numLightSGs)

        freezeencoder = True

        if freezeencoder:
            self.lightSGs = EnvmapSGencoder(2048, 7 * numLightSGs)
            self.lightSGs.load_state_dict(torch.load(
                'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/dict-outdoor-12SG-nobias-timesexpp1-long.pth'))
                # 'C:/Users/grainer.AD/Dropbox/INRIA/projects/learnedtransport/results/dict-envmapAE-72SG-nobias-timesexp.pth'))
            for param in self.lightSGs.parameters():
                param.requires_grad = False

        # Interreflections SG generator
        # self.sceneSGs = VisibilityNet(numSceneSGs * 6, inpsize, width, num_layers)
        # self.sceneSGs = CompleteNet(cnndim, 72, numSceneSGs, inpsize, width, num_layers)

        # SG multiplier prediction from SG directions:
        self.encoder = BiggerImageEncoder(cnndim, 32)
        self.SGmultipliers = PositionSGDecoder(32, numLightSGs, inpsize, width, num_layers, lightinputdepth=0,
                 alternate_parametrization=False, outdim=numLightSGs)
        # self.SGmultipliers = PositionSGDecoder(4 * numLightSGs, numLightSGs, inpsize, width, num_layers,
        #                                        lightinputdepth=0,
        #                                        alternate_parametrization=False, outdim=numLightSGs)
        # self.SGmultipliers = VisibilityNet(numLightSGs, inpsize, width, num_layers)

        # From view direction:
        # self.SGmultipliers = VisibilityNet(numSceneSGs + numLightSGs, inpsize + 3, width, num_layers)

        self.numLightSGs = numLightSGs
        self.numSceneSGs = numSceneSGs
        self.width = width
        self.num_layers = num_layers
        self.activate = nn.ReLU()  # nn.LeakyReLU()#

        self.skipconnections = False

    def forward(self, envmap, pos):
        lightsgs = self.lightSGs(envmap).view(self.numLightSGs, 7).unsqueeze(0).expand(pos.shape[0], -1, -1)
        # scenesgs = self.sceneSGs(pos).unsqueeze(2).view(-1, self.numLightSGs, 6)
        # scenesgs = self.sceneSGs(envmap, pos).unsqueeze(2).view(-1, self.numLightSGs, 6)


        lightcode = self.encoder(envmap)
        sgmult = self.SGmultipliers(lightcode, pos).unsqueeze(2).expand(-1,-1, 3)
        # sgmult = self.SGmultipliers(torch.cat((pos, wi), dim=1)).unsqueeze(2).expand(-1,-1, 3)
        # print("lightsgs.shape, scenesgs.shape, sgmult.shape", lightsgs.shape, scenesgs.shape, sgmult.shape)

        factors = torch.sigmoid(sgmult)

        # TODO : problem here is the multiplier never gets the light code so it cant do interreflections correctly cause it doesnt know that it should turn on or off the visibility for

        # out = torch.cat( (torch.cat((lightsgs[:,:,0:3], scenesgs[:,:,0:3]), dim=1) * factors,
        #                   torch.cat((lightsgs[:, :, 3:6], scenesgs[:, :, 3:6]), dim=1)), dim=2)

        # Test direct only
        # out = torch.cat( (lightsgs[:, :, 0:3] * factors[:,0:self.numLightSGs, :] , lightsgs[:, :, 3:7]), dim=2)
        # print("netout:  ", out.shape)
        # return out
        return lightsgs, factors

    def getEnvmap(self, envmap):
        if self.alternate_parametrization:
            return self.lightSGs(envmap).unsqueeze(2).view(-1, self.numLightSGs, 7)
        else:
            return self.lightSGs(envmap).unsqueeze(2).view(-1, self.numLightSGs, 6)




class DualNet(nn.Module):
    def __init__(self, cnndim, latentdim, numSGs, inpsize, width, num_layers): # Retry this with alternate parametrization
        super(DualNet, self).__init__()
        self.numSGs = numSGs

        self.encoder = ImageEncoder(cnndim, latentdim)
        self.decoder = PositionSGDecoder(latentdim, numSGs, 3, width, num_layers, alternate_parametrization=True)
        self.multipliers = siren.SirenNet(dim_in=6, dim_hidden=width, dim_out=numSGs, num_layers=num_layers, w0_initial=30.)


        # self.encoder_cnn = models.resnet18()
        # self.encoder_fc = nn.Linear(1000, latentdim)


    def forward(self, envmap, pos):
        sgs = self.decoder(self.encoder(envmap), pos[:,0:3])
        sgs = sgs.unsqueeze(2).view(-1, self.numSGs, 7)
        # sgs[:,:,0:3] = sgs[:,:,0:3] * torch.sigmoid(self.multipliers(pos)).unsqueeze(2).expand(-1,-1,3)
        # return sgs #self.decoder(self.encoder(envmap), pos)
        factors = torch.sigmoid(self.multipliers(pos)).unsqueeze(2).expand(-1, -1, 3)
        sg2 = torch.cat( ( sgs[:,:,0:3] * factors, sgs[:,:,3:]), dim=2)
        # print(sg2.shape)
        return sg2 #.view(-1, self.numSGs * 7)

class TripleNet(nn.Module):
    def __init__(self, latentdim, numSGs, width, num_layers, alternate=False): # Retry this with alternate parametrization
        super(TripleNet, self).__init__()
        self.numSGs = numSGs
        self.alternate = alternate
        if alternate:
            self.sg_decoder = posNet(2 * latentdim, numSGs * 7, 2, 512)
        else:
            self.sg_decoder = posNet(2 * latentdim, numSGs * 6, width, num_layers)

        self.pos_decoder = siren.SirenNet(dim_in=6, dim_hidden=512, dim_out=latentdim, num_layers=3, w0_initial=30.)

        # self.encoder_cnn = models.resnet18()
        # self.encoder_fc = nn.Linear(1000, latentdim)

        self.encoder = ImageEncoder(2048, latentdim)

    def forward(self, envmap, pos):
        # lightcode = self.encoder_fc(self.encoder_cnn(envmap)).expand(pos.shape[0], -1)
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)

        sgs = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))

        if self.alternate:
            return sgs.unsqueeze(2).view(-1, self.numSGs, 7)
        else:
            return sgs


class DualNetWeird(nn.Module):
    def __init__(self, cnndim, latentdim, numSGs, inpsize, width, num_layers): # Retry this with alternate parametrization
        super(DualNetWeird, self).__init__()
        self.numSGs = numSGs

        self.encoder = ImageEncoder(cnndim, latentdim)
        self.decoder = posNet(latentdim, numSGs * 6, width, num_layers)

        self.multipliers = siren.SirenNet(dim_in=6, dim_hidden=width, dim_out=latentdim, num_layers=num_layers, w0_initial=30.)

    def forward(self, envmap, pos):
        latentlight = self.encoder(envmap).expand(pos.shape[0], -1)
        latentmultipliers = torch.sigmoid(self.multipliers(pos))

        sgs = self.decoder(latentlight * latentmultipliers)
        return sgs# .unsqueeze(2).view(-1, self.numSGs, 7)


class PretrainedNet(nn.Module):
    def __init__(self, latentdim, numSGs, width, num_layers, pretrainpath=None, alternate=True, insize=6): # Retry this with alternate parametrization
        super(PretrainedNet, self).__init__()
        self.numSGs = numSGs
        self.sg_decoder = posNet(latentdim + 64, numSGs * 7, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

        if pretrainpath is not None:
            self.encoder.load_state_dict(torch.load(pretrainpath))
            print('Loading pretrained encoder ============================================================')
            # for param in self.encoder.parameters():
            #     param.requires_grad = False

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        # lightcode = self.encoder_fc(self.encoder_cnn(envmap)).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        sgs = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
        return sgs.unsqueeze(2).view(-1, self.numSGs, 7)

class Dumbnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(Dumbnet, self).__init__()
        self.sg_decoder = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        sgs = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
        return sgs


class DumbDumbnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(DumbDumbnet, self).__init__()
        self.sg_decoder = posNet(insize + 64, 3, width, 4)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        sgs = self.sg_decoder(torch.cat((lightcode, pos), dim=1))
        return sgs


class IntegralNet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):
        super(IntegralNet, self).__init__()
        # self.sg_decoder = posNet(latentdim, 3, width, num_layers)
        self.sg_decoder = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)
    def forward(self, envmap, pos):  # posdiff, posspec):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        # return self.sg_decoder(lightcode * poscode)
        return self.sg_decoder(torch.cat((lightcode, poscode), dim=1))

class IntegralNet3(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):
        super(IntegralNet3, self).__init__()
        self.latentdim = latentdim
        self.sg_decoder = posNet(latentdim, 3, width, num_layers)
        # self.sg_decoder = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=6, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.dirnet = siren.SirenNet(dim_in=9, dim_hidden=256, dim_out=3, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm, posview):  # posdiff, posspec):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode = self.pos_decoder(posnorm)
        diffuse = self.sg_decoder(lightcode * poscode)
        specular = self.dirnet(torch.cat((posview, diffuse), dim=1))
        return diffuse, specular

class IntegralNet4(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):
        super(IntegralNet4, self).__init__()
        self.latentdim = latentdim
        self.sg_decoder = posNet(latentdim, latentdim + 3, width, num_layers)
        # self.sg_decoder = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=6, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.dirnet = siren.SirenNet(dim_in=4, dim_hidden=256, dim_out=latentdim * 3, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm, viewrough):  # posdiff, posspec):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode = self.pos_decoder(posnorm)
        locallight = self.sg_decoder(lightcode * poscode)
        diffuse = locallight[:, 0:3]
        specular = torch.sum(self.dirnet(viewrough).view(-1, self.latentdim, 3) * locallight[:,3:].view(-1, self.latentdim, 1), dim=1)
        return diffuse, specular

class IntegralNet45(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):
        super(IntegralNet45, self).__init__()
        self.latentdim = latentdim
        self.pos_decoder_diff = siren.SirenNet(dim_in=6, dim_hidden=175, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.pos_decoder_spec = siren.SirenNet(dim_in=7, dim_hidden=175, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.radiance_decoder = posNet(latentdim, 3, 256, 2)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm, viewrough):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode_diff = self.pos_decoder_diff(posnorm)
        poscode_spec = self.pos_decoder_spec(viewrough)
        # diffuse = torch.sum(poscode_diff.view(-1, self.latentdim, 3) * lightcode.view(-1, self.latentdim, 1), dim=1)
        # specular = torch.sum(poscode_spec.view(-1, self.latentdim, 3) * lightcode.view(-1, self.latentdim, 1), dim=1)
        diffuse = self.radiance_decoder(poscode_diff * lightcode)
        specular = self.radiance_decoder(poscode_spec * poscode_diff * lightcode)
        return diffuse, specular

class IntegralNet5(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):
        super(IntegralNet5, self).__init__()
        self.latentdim = latentdim
        self.pos_decoder_diff = siren.SirenNet(dim_in=6, dim_hidden=175, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.pos_decoder_spec = siren.SirenNet(dim_in=7, dim_hidden=175, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.radiance_decoder = posNet(latentdim, 3, 256, 2)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm, posviewrough):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode_diff = self.pos_decoder_diff(posnorm)
        poscode_spec = self.pos_decoder_spec(posviewrough)
        # diffuse = torch.sum(poscode_diff.view(-1, self.latentdim, 3) * lightcode.view(-1, self.latentdim, 1), dim=1)
        # specular = torch.sum(poscode_spec.view(-1, self.latentdim, 3) * lightcode.view(-1, self.latentdim, 1), dim=1)
        diffuse = self.radiance_decoder(poscode_diff * lightcode)
        specular = self.radiance_decoder(poscode_spec * poscode_diff * lightcode)
        return diffuse, specular

class IntegralNet6(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):
        super(IntegralNet6, self).__init__()
        self.latentdim = latentdim
        self.sg_decoder = posNet(latentdim * 2, latentdim * 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=6, dim_hidden=172, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.view_decoder = siren.SirenNet(dim_in=4, dim_hidden=172, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm, diffuseviewrough, viewrough):  # posdiff, posspec):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode = self.pos_decoder(posnorm)
        locallight = self.sg_decoder(torch.cat((poscode,lightcode), dim=1)).view(-1, self.latentdim, 3)

        viewcode1 = self.view_decoder(diffuseviewrough).view(-1, self.latentdim, 1)
        diffuse = torch.sum(locallight * viewcode1, dim=1)
        viewcode = self.view_decoder(viewrough).view(-1, self.latentdim, 1)
        specular = torch.sum(locallight * viewcode, dim=1)
        return diffuse, specular


class IntegralNet2(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(IntegralNet2, self).__init__()
        self.sg_decoder = posNet(latentdim + latentdim, latentdim, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, latentdim)

        self.diffuse_decoder = siren.SirenNet(dim_in=3, dim_hidden=32, dim_out=latentdim*3, num_layers=2, w0_initial=30.)
        self.specular_decoder = siren.SirenNet(dim_in=7, dim_hidden=256, dim_out=latentdim*3, num_layers=2, w0_initial=30.)
        self.latentdim = latentdim

    def forward(self, envmap, pos_diffuse, pos_spec, normal, normalviewrough): #posdiff, posspec):
        envlat = self.encoder(envmap)
        poscode = self.pos_decoder(pos_spec)
        illumcode_spec = self.sg_decoder(torch.cat((envlat.expand(pos_spec.shape[0], -1), poscode), dim=1))

        poscode_diff = self.pos_decoder(pos_diffuse)
        illumcode_diff = self.sg_decoder(torch.cat((envlat.expand(pos_diffuse.shape[0], -1), poscode_diff), dim=1))

        diff = torch.sum(self.diffuse_decoder(normal).view(-1, self.latentdim, 3) * illumcode_diff.view(-1, self.latentdim,1), dim=1)
        spec = torch.sum(self.specular_decoder(normalviewrough).view(-1, self.latentdim, 3) * illumcode_spec.view(-1, self.latentdim,1), dim=1)
        return diff, spec
        # dotprod = lightcode * torch.sigmoid(poscode)
        # return self.sg_decoder(torch.cat((dotprod, lightcode, poscode, view), dim=1))
        # diff = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
        # poscodespec = self.pos_decoder(posspec)
        # spec = self.sg_decoder(torch.cat((lightcode, poscodespec), dim=1))
        # return diff, spec

class ResidualNet(nn.Module):
    def __init__(self, latentdim, numSGs, width, num_layers): # Retry this with alternate parametrization
        super(ResidualNet, self).__init__()
        self.numSGs = numSGs
        self.sg_decoder = posNet(latentdim + 128, numSGs * 7, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=6, dim_hidden=width, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 128)

        self.residual = siren.SirenNet(dim_in=9, dim_hidden=width, dim_out=3, num_layers=3, w0_initial=30.)

    def forward(self, envmap, pos, viewdir):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        sgs = self.sg_decoder(torch.cat((lightcode, poscode), dim=1))
        return sgs.unsqueeze(2).view(-1, self.numSGs, 7), self.residual(torch.cat((pos,viewdir), dim=1))

class SplitDumbnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(SplitDumbnet, self).__init__()
        self.diffusenet = posNet(latentdim + 64, 3, width, num_layers)
        self.specularnet = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(pos)
        diff = self.diffusenet(torch.cat((lightcode, poscode), dim=1))
        spec = self.specularnet(torch.cat((lightcode, poscode), dim=1))
        return diff, spec


class Splitnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(Splitnet, self).__init__()
        self.diffusenet = posNet(latentdim + 64, 3, width, num_layers)
        self.specularnet = posNet(latentdim + 64 + 3, 3, width, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, pos, normals, viewdir):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_decoder(torch.cat((pos, normals), dim=1))
        diff = self.diffusenet(torch.cat((lightcode, poscode), dim=1))
        spec = self.specularnet(torch.cat((lightcode, poscode, viewdir), dim=1))
        return diff, spec

class SplitEncodernet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):  # Retry this with alternate parametrization
        super(SplitEncodernet, self).__init__()
        self.decoder = posNet(latentdim, 3, 172, num_layers)
        self.decoder_specular = posNet(latentdim, 3, 172, num_layers)
        self.pos_decoder = siren.SirenNet(dim_in=6, dim_hidden=172, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.pos_decoder_specular = siren.SirenNet(dim_in=10, dim_hidden=172, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, posnorm, posnormview):
        lightcode = self.encoder(envmap).expand(posnorm.shape[0], -1)
        poscode_diff = self.pos_decoder(posnorm)
        poscode_spec = self.pos_decoder_specular(posnormview)
        diff = self.decoder(lightcode * poscode_diff)
        spec = self.decoder_specular(lightcode * poscode_spec)
        return diff, spec


class PixelenvNet(nn.Module):
    def __init__(self, width, num_layers, angles=128, useSiren=False, insize=6): # Retry this with alternate parametrization
        super(PixelenvNet, self).__init__()
        self.useSiren = useSiren
        self.angularsamples = 3 * angles
        self.latentgeodim = 64
        if useSiren:
            self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=32, dim_out=self.latentgeodim, num_layers=2,
                                              w0_initial=30.)
            self.decoder = posNet(self.angularsamples + self.latentgeodim, self.angularsamples, width, num_layers)
        else:
            self.decoder = posNet(self.angularsamples + insize, self.angularsamples, width, num_layers)
        # self.decoder = siren.SirenNet(dim_in=9, dim_hidden=256, dim_out=self.angularsamples**2, num_layers=2,w0_initial=30.)

    def forward(self, envmap, pos):
        poscode = pos
        if self.useSiren:
            poscode = self.pos_decoder(pos)
        return self.decoder(torch.cat((envmap, poscode), dim=1)) + envmap
        # return self.decoder(torch.cat((pos, normals, viewdir), dim=1))

class CompressedPixelenvNet(nn.Module):
    def __init__(self, width, num_layers, angles=128, useSiren=False, insize=6): # Retry this with alternate parametrization
        super(CompressedPixelenvNet, self).__init__()
        self.useSiren = useSiren
        self.angularsamples = 3 * angles
        self.latentgeodim = 64
        self.encoder = EnvmapSGencoder(4096, 64)
        if useSiren:
            self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=self.latentgeodim, num_layers=2,
                                              w0_initial=30.)
            self.decoder = posNet(64 + self.latentgeodim, self.angularsamples, width, num_layers)
        else:
            self.decoder = posNet(64 + insize, self.angularsamples, width, num_layers)
        # self.decoder = siren.SirenNet(dim_in=9, dim_hidden=256, dim_out=self.angularsamples**2, num_layers=2,w0_initial=30.)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        if self.useSiren:
            poscode = self.pos_decoder(pos)
            return self.decoder(torch.cat((lightcode, poscode), dim=1))
        return self.decoder(torch.cat((lightcode, pos), dim=1))

class NDFnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(NDFnet, self).__init__()
        self.decoder = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_encoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, pos):
        lightcode = self.encoder(envmap).expand(pos.shape[0], -1)
        poscode = self.pos_encoder(pos)
        return self.decoder(torch.cat((lightcode, poscode), dim=1))

class BRDFnet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(BRDFnet, self).__init__()
        self.decoder = posNet(latentdim + 64, 3, width, num_layers)
        self.pos_encoder = siren.SirenNet(dim_in=insize, dim_hidden=256, dim_out=latentdim, num_layers=2, w0_initial=30.)
        self.encoder = EnvmapSGencoder(4096, 64)

    def forward(self, envmap, pos_diffuse, pos_specular):
        lightcode = self.encoder(envmap)
        poscode_diffuse = self.pos_encoder(pos_diffuse)
        poscode_specular = self.pos_encoder(pos_specular)
        shade_diffuse = self.decoder(torch.cat((lightcode.expand(pos_diffuse.shape[0], -1), poscode_diffuse), dim=1))
        shade_specular = self.decoder(torch.cat((lightcode.expand(poscode_specular.shape[0], -1), poscode_specular), dim=1))
        return shade_diffuse, shade_specular


class SirenModulateNet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6):
        super(SirenModulateNet, self).__init__()
        self.encoder = EnvmapSGencoder(4096, 64)
        self.pos_decoder = siren.SirenNet(dim_in=insize, dim_hidden=width, dim_out=3, num_layers=num_layers,
                                  w0_initial=30.)
        self.pos_decoder_specular = siren.SirenNet(dim_in=insize+3, dim_hidden=width, dim_out=3, num_layers=num_layers,
                                  w0_initial=30.)
        self.modulator = siren.Modulator(dim_in=64, dim_hidden=width, num_layers=num_layers)
        self.modulator_specular = siren.Modulator(dim_in=64, dim_hidden=width, num_layers=num_layers)

    def forward(self, envmap, pos, normals, viewdir):
        latent = self.encoder(envmap)
        mods = self.modulator(latent.squeeze())
        mods2 = self.modulator_specular(latent.squeeze())
        diff = self.pos_decoder(torch.cat((pos, normals), dim=1), mods)
        spec = self.pos_decoder_specular(torch.cat((pos, normals, viewdir), dim=1), mods2)
        return diff, spec


class Crazynet(nn.Module):
    def __init__(self, latentdim, width, num_layers, insize=6): # Retry this with alternate parametrization
        super(Crazynet, self).__init__()
        self.numlats = 32

        self.transfer = siren.SirenNet(dim_in=insize, dim_hidden=width, dim_out=self.numlats*self.numlats, num_layers=2,
                                        w0_initial=30.)
        # self.specnet = siren.SirenNet(dim_in=insize + 3, dim_hidden=64, dim_out=self.numlats*3, num_layers=2, w0_initial=30.)

        self.decoder = siren.SirenNet(dim_in=4, dim_hidden=32, dim_out=self.numlats*3, num_layers=2, w0_initial=30.)
        self.decoder_diffuse = siren.SirenNet(dim_in=3, dim_hidden=32, dim_out=self.numlats*3, num_layers=2, w0_initial=30.)

        self.encoder = EnvmapSGencoder(4096, self.numlats)

    def forward(self, envmap, pos, normals, viewdir):
        light = self.encoder(envmap).expand(pos.shape[0], -1).view(pos.shape[0],self.numlats, 1)

        viz = self.transfer(torch.cat((pos, normals), dim=1)).unsqueeze(2).view(-1, self.numlats, self.numlats)
        locallight = torch.sum(viz * light, dim=-1)

        # diff = torch.sum(locallight, dim=-1).squeeze() / self.numlats
        diffvis = self.decoder_diffuse( normals ).unsqueeze(-1).view(-1, self.numlats, 3)
        diff = torch.sum(locallight.view(pos.shape[0], self.numlats, 1) * diffvis, dim=-2).squeeze()

        specvis = self.decoder(viewdir).unsqueeze(-1).view(-1,self.numlats, 3)
        spec = torch.sum(locallight.view(pos.shape[0], self.numlats, 1) * specvis, dim=-2).squeeze()

        return diff, spec 

class PretrainedMultiplierNet(nn.Module):
    def __init__(self, latentdim, numSGs, width, num_layers, pretrainpath=None, alternate=True): # Retry this with alternate parametrization
        super(PretrainedMultiplierNet, self).__init__()
        self.numSGs = numSGs
        self.encoder = EnvmapSGencoder(1024, 7*12)
        if pretrainpath is not None:
            self.encoder.load_state_dict(torch.load(pretrainpath))
            # for param in self.encoder.parameters():
            #     param.requires_grad = False

        self.multipliers = siren.SirenNet(dim_in=6, dim_hidden=width, dim_out=numSGs, num_layers=num_layers, w0_initial=30.)
        self.normalize = torch.nn.Softplus() #torch.sigmoid()

        # TODO : try a GI relu net that takes in Distant SGs, concats at every layer with position and modifies them
        # TODO : switch sigmoid to softplus so the multiplier network can adjust the global luminosity

    def forward(self, envmap, pos):
        sgs = self.encoder(envmap).expand(pos.shape[0], -1).unsqueeze(2).view(-1, self.numSGs, 7)
        factors = self.normalize(self.multipliers(pos)).unsqueeze(2).expand(-1, -1, 3)
        sg2 = torch.cat((sgs[:, :, 0:3] * factors, sgs[:, :, 3:]), dim=2)
        # print(sg2.shape)
        return sg2