# Neural Precomputed Radiance Transfer

*Gilles Rainer, Adrien Bousseau, Tobias Ritschel and George Drettakis*

Computer Graphics Forum (Proceedings of the Eurographics conference), April 2022. 

[**Project Website**](https://repo-sam.inria.fr/fungraph/neural-prt/)

![Teaser](https://repo-sam.inria.fr/fungraph/neural-prt/static/images/teaser.jpg?raw=true)

Recent advances in neural rendering indicate immense promise for architectures that learn light transport, allowing efficient rendering of global illumination effects once such methods are trained. The training phase of these methods can be seen as a form of pre-computation, which has a long standing history in Computer Graphics. In particular, Pre-computed Radiance Transfer (PRT) achieves real-time rendering by freezing some variables of the scene (geometry, materials) and encoding the distribution of others, allowing interactive rendering at runtime. We adopt the same configuration as PRT – global illumination of static scenes under dynamic environment lighting – and investigate different neural network architectures, inspired by the design principles and theoretical analysis of PRT. We introduce four different architectures, and show that those based on knowledge of light transport models and PRT-inspired principles improve the quality of global illumination predictions at equal training time and network size, without the need for high-end ray-tracing hardware.


# Requirements

Install the packages and activate the environment via:

```
conda create --name nprt --file requirements.txt
conda activate nprt
```

# Contents

*Disclaimer: This is unoptimized research code, full of redundancies :)*

## Rendering training data

This section is for Windows only as it depends on Falcor. The other sections can run on all platforms.

The training data was rendered using the real-time renderer [Falcor](https://github.com/NVIDIAGameWorks/Falcor). A fork of Falcor 4.3 with some minor amendments, rendering scripts and scene format conversion scripts is provided in this repository.

An minimal example (2 renderings) of a data layout that works with the provided scripts is given in the 'example-data' folder.
The renderings for training/validation/testing are in 'atelier' and the environment maps in 'indoorenvs'.
The naming convention for the Buffers is as they come out of the included Falcor implementation.

*Warning: The indices used in the respective rendering and environment map folder must be consistent! (i.e. in the 'atelier/train' folder the image '00000.AccumulatePass.output.0.exr' must have been rendered with the lighting in 'indoorenvs/train/env-00000.exr').*


## Training networks

Each architecture is in a separate python script in the 'training' folder. The four alternatives presented in the publication are all prefixed 'paper-', while 'deepshading.py' is a reimplementation of Nalbach et al. to match our trainable parameter count. 

All user-specific paths and parameters are hardcoded in the 'Input paths and parameters' section at the top. You only need to specify the rendering and environment map paths for training, validation, and testing (a higher-resolution dataset that is rendered out a couple of times during training to monitor the quality improvement).


The SIREN network code is taken from [this implementation](https://github.com/lucidrains/siren-pytorch).
We provide pre-trained networks from the publication (see below) for completeness, but the networks are scene- and data-specific and should be retrained from scratch for any new scene or application.

## Inference

We provide simple examples of inference scripts (all starting with 'read...').
These scripts will need paths like the G-Buffer frames, the per-frame environment maps, the pre-trained networks, and the bounding box values of the scene to be updated.

# Data

The [datasets and trained networks](https://repo-sam.inria.fr/fungraph/neural-prt/data/training-data.zip) used in the publication, as well as the [Falcor scene files](https://repo-sam.inria.fr/fungraph/neural-prt/data/3d-scenes.zip) used to render them, are available for download.

# Citation

Please consider citing the publication if you end up using any of this code in your research.

```
@Article{RBRD22,
  author       = "Rainer, Gilles and Bousseau, Adrien and Ritschel, Tobias and Drettakis, George",
  title        = "Neural Precomputed Radiance Transfer",
  journal      = "Computer Graphics Forum (Proceedings of the Eurographics conference)",
  number       = "2",
  volume       = "41",
  month        = "April",
  year         = "2022",
  url          = "http://www-sop.inria.fr/reves/Basilic/2022/RBRD22"
}
```

For any questions please contact gilles [dot] rainer [dot] enst [at] gmail [dot] com.
